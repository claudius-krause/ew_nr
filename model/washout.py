""" Class to compute baryon number washout"""

import numpy as np
import scipy.interpolate as interp

class SphaleronDecayRate:
    def __init__(self, model, v_array, T_array, ren_scale):
        """ sort v_array by temperature upon initialization"""
        assert len(v_array) == len(T_array)
        sorted_indices = np.argsort(T_array)
        self.T_array = T_array[sorted_indices] + 1e-16
        self.v_array = v_array[sorted_indices]
        v_eff = np.sqrt(self.v_array[..., 0]**2 + self.v_array[..., 1]**2)
        self.v_over_T = v_eff / self.T_array

        self.model = model
        self.param = SphaleronParameters()
        if len(ren_scale)>1:
            self.ren_scale = ren_scale[sorted_indices]
        else:
            self.ren_scale = ren_scale
        self.lbda_gsq = None

    def decay_rate(self):
        """ Gamma / V """
        ret = 4.* np.pi * self._omega_minus()
        ret *= self._N_rot_trans() * self.T_array**4
        ret *= self.model.cp_g(self.ren_scale)*self.v_over_T**7
        # add as log for numerical precision
        retlog = np.log(ret) + self._kappa() + (self._energy_prefactor() * self.v_over_T)
        return np.exp(retlog)

    def integrand(self):
        """ to be integrated over T """
        M_Planck = 1.22e19
        g_star = 106.75 + 4. + self.model.N
        ret = self.decay_rate() * (M_Planck/(self.T_array**6))
        ret *= np.sqrt(45./(4.*np.pi**3*g_star))
        return ret

    def washout_factor_discrete(self, T_low, T_high):
        """ returns washout factor based on temperature window with discrete T values"""
        n_f = 3.
        using_temps = np.logical_and(self.T_array >= T_low, self.T_array <= T_high)
        integrand_array = self.integrand()[using_temps]
        local_T_array = self.T_array[using_temps]

        #print("used T", local_T_array)
        #print("v/T", self.v_over_T[using_temps])
        #print("log kappa", self._kappa()[using_temps])
        #print("omega", self._omega_minus()[using_temps])
        #print("Nrottrans", self._N_rot_trans()[using_temps])
        #print("energy prefactor", self._energy_prefactor()[using_temps])
        #print("integrand", integrand_array)

        print("Basing washout integral on region between {} and {}".format(
            local_T_array[0], local_T_array[-1]))
        integral_array = (integrand_array[:-1] + integrand_array[1:])* 0.5 *\
            (local_T_array[1:]-local_T_array[:-1])
        #print("integrand cumsum", np.cumsum(integral_array))
        ret = np.exp((-13.*n_f/2.)*np.sum(integral_array))
        return ret, np.exp(np.log(ret)*1e-2), np.exp(np.log(ret)*1e2)

    def washout_factor_interp(self, T_low, T_high):
        """ returns washout factor based on temperature window, using interpolated T"""
        raise NotImplementedError

    def _omega_minus(self):
        """ returns omega_minus prefactor in units of [g v(T)]"""
        lbda_gsq = self._lambda_gsq()
        ret = np.sqrt(self.param.omega_minus_interp(lbda_gsq))
        return ret

    def _kappa(self):
        """ returns kappa prefactor """
        lbda_gsq = self._lambda_gsq()

        # use line:
        #ret = self.param.ln_kappa_interp_line(lbda_gsq)

        # use points:
        ret = self.param.ln_kappa_interp(lbda_gsq)

        return ret

    def _N_rot_trans(self):
        """ returns normalization prefactors """
        lbda_gsq = self._lambda_gsq()
        ret = self.param.N_tr_interp(lbda_gsq) * self.param.N_rot_interp(lbda_gsq)
        return ret

    def _energy_prefactor(self):
        """ = - E_sph / v  = - B * 4 pi / g"""
        prefactor = 4.*np.pi / self.model.cp_g(self.ren_scale)
        lbda_gsq = self._lambda_gsq()
        return - prefactor * self.param.B_func_interp(lbda_gsq)

    def _lambda_gsq(self):
        """ computes lambda/g^2 at the scale given by v_array,
            lambda is chosen to be b4 of lmbda, depending on type of minimum
        """
        if self.lbda_gsq is None:
            # use Higgs lambda as much as possible:
            #lbda = np.where(np.abs(self.v_array[..., 0]) > 5.,
            #                self.model.cp_lbda(self.ren_scale),
            #                self.model.cp_b4(self.ren_scale))

            # use Inert lambda as much as possible:
            lbda = np.where(np.abs(self.v_array[..., 1]) < 5.,
                            self.model.cp_lbda(self.ren_scale),
                            self.model.cp_b4(self.ren_scale))

            gsq = self.model.cp_g(self.ren_scale)**2
            self.lbda_gsq = lbda/gsq
        return self.lbda_gsq


class SphaleronParameters:
    """ Returns interpolated values for the prefactor of the sphaleron rate.

        Digitized data of figure 5 (N_tr and N_rot) and 6 (omega_minus)
        of https://journals.aps.org/prd/pdf/10.1103/PhysRevD.41.647, ref [1]
        using g3data
        [http://manpages.ubuntu.com/manpages/xenial/man1/g3data.1.html]
        and table 1 (kappa) of hep-th/9402078, ref [2]

        N_tr: normalization of translations
        N_rot: normalization of rotations
        omega_minus: frequency of unstable mode
        note that omega_minus of the decay rate actually is
        np.sqrt(returned_result) * g * v
    """

    def __init__(self, interpolation_level='linear'):
        self.interpolation_level = interpolation_level
        self.xi_grid = np.sort(np.array([10**j * i for i in np.arange(1, 10)\
                                         for j in [-2, -1, 0]]))
        self.xi_grid = np.append(self.xi_grid, 10.)

    def N_tr_disc(self):
        """ Fig 5 of ref [1] """
        ret = np.array([6.1212997737, 6.2485245748, 6.34292845646, 6.4667941869,
                        6.5293568779, 6.5914996850, 6.6838041475, 6.7152254542,
                        6.7769483776, 6.8080897618, 7.23847068978, 7.48452261541,
                        7.69915323427, 7.85262077492, 7.97536681518, 8.09797289417,
                        8.18999743404, 8.28202197392, 8.37376659124, 8.9253539854,
                        9.26231076069, 9.5072429961, 9.72131376986, 9.87436142667,
                        9.99696750566, 10.1192936621, 10.2416198185, 10.2727612027])
        return ret

    def N_tr_interp(self, xi):
        """ interpolation of digitized points"""
        ret = interp.interp1d(self.xi_grid, self.N_tr_disc(),
                              kind=self.interpolation_level,
                              bounds_error=True)
        return ret(xi)

    def N_rot_disc(self):
        """ Fig 5 of ref [1] """
        ret = np.array([15.1203217622, 14.686854751, 14.3051596535, 14.0109436565,
                        13.7455599855, 13.5085175788, 13.3315951315, 13.1536905611,
                        13.0056004396, 12.8575103181, 11.9391551404, 11.4064235522,
                        11.0819020449, 10.8160273124, 10.6395959265, 10.4929789896,
                        10.3453799296, 10.2280863801, 10.1401162179, 9.58591822657,
                        9.35575068106, 9.2130622362, 9.09871505571, 9.04399677302,
                        9.01811081621, 9.02253036982, 8.99566228998, 8.93848869974])
        return ret

    def N_rot_interp(self, xi):
        """ interpolation of digitized points"""
        ret = interp.interp1d(self.xi_grid, self.N_rot_disc(),
                              kind=self.interpolation_level,
                              bounds_error=True)
        return ret(xi)

    def omega_minus_disc(self):
        """ Taken from Fig. 6 of ref [1] """
        ret = np.array([0.445119580012, 0.462424654871, 0.479545012639, 0.4923779895,
                        0.505172078553, 0.517946723702, 0.526472875753, 0.534999027805,
                        0.543505735952, 0.552012444099, 0.619949445849, 0.666320125996,
                        0.699816902392, 0.733352470654, 0.762698517375, 0.787855042555,
                        0.804613128719, 0.829789049832, 0.842357614455, 0.989243015525,
                        1.09842272272, 1.19925248074, 1.29171289364, 1.379983785,
                        1.45564731987, 1.53973838765, 1.61122209895, 1.66587014035])
        return ret

    def omega_minus_interp(self, xi):
        """ interpolation of digitized points"""
        ret = interp.interp1d(self.xi_grid, self.omega_minus_disc(),
                              kind=self.interpolation_level,
                              bounds_error=True)
        return ret(xi)

    def ln_kappa_disc_line(self):
        # from figure 1 of ref [2]:
        ret = np.array([-45.1177158018, -29.9543286693, -22.3143931019, -18.0941852753,
                        -15.128265058, -12.7323836897, -11.3628754619, -9.99311455734,
                        -8.96573070971, -6.11023025173, -4.62398534475, -3.1357190234,
                        -2.33321752314, -1.98780834465, -1.64290451976, -1.4124632829,
                        -1.18227472285, -0.950822778813, -0.602128801996, -0.36941347399,
                        -0.248886642873, -0.244085783772, -0.126338397397,
                        -0.12330627586, -0.0045481823063, 0.112946527273])
        return ret

    def ln_kappa_interp_line(self, xi):
        """ interpolation of digitized points """
        # from figure 1 of ref [2]:
        kappa_grid_line = np.array([0.0200849631877, 0.0301326482141, 0.0398419983979,
                                    0.0498094963753, 0.0595884201245, 0.0702649740359,
                                    0.0792579776458, 0.0907818021332, 0.100897077916,
                                    0.149598812114, 0.199675748628, 0.301256364417,
                                    0.402520705549, 0.498522597117, 0.598794826199,
                                    0.697659639765, 0.800492823744, 0.991584819881,
                                    1.49864513868, 2.00414813551, 2.98399882767,
                                    3.99191480469, 5.02205205888, 6.03534095553,
                                    8.07251057462, 10.0013091578])
        ret = interp.interp1d(kappa_grid_line, self.ln_kappa_disc_line(),
                              kind=self.interpolation_level,
                              bounds_error=True)
        return ret(xi)

    def ln_kappa_disc(self):
        # from table 1 of ref [2]:
        ret = np.array([-46.80, -31.00, -22.30, -13.64, -9.64, -5.96, -5.14])
        return ret

    def ln_kappa_interp(self, xi):
        # from table 1 of ref [2]:
        kappa_grid = np.array([.02, .031, .045, .08, .125, .281, .5])

        fill_value = (self.ln_kappa_disc()[0], self.ln_kappa_disc()[-1])
        ret = interp.interp1d(kappa_grid, self.ln_kappa_disc(),
                              kind=self.interpolation_level,
                              fill_value=fill_value,
                              bounds_error=False)
        return ret(xi)

    def B_func_disc(self):
        """ Sphaleron Energy prefactor, taken from profile function integral """
        ret = np.array([1.637040511306324, 1.6742311510832688, 1.7001752002134296,
                        1.7205851379799653, 1.7376038852237092, 1.752295944747645,
                        1.7652766924882046, 1.776937702410832, 1.7875453023587844,
                        1.797289752455976, 1.8673278555264003, 1.9128506687034053,
                        1.9469920224629447, 1.9744174763841806, 1.9973712902813454,
                        2.0171184357290035, 2.0344467377009425, 2.049882023528444,
                        2.0637936371739447, 2.156755449092287, 2.211136392657265,
                        2.2491002788435135, 2.2779607050461568, 2.301052428917301,
                        2.3201754817683073, 2.336410440844552, 2.350455679859462,
                        2.3627878449020483])
        return ret

    def B_func_interp(self, xi):
        """ interpolation of points above """
        ret = interp.interp1d(self.xi_grid, self.B_func_disc(),
                              kind=self.interpolation_level,
                              bounds_error=True)
        return ret(xi)

if __name__ == '__main__':
    import matplotlib.pyplot as plt
    plt.rc('text', usetex=True)
    plt.rc('text.latex', preamble=r'\usepackage{amsmath,amssymb}')
    plt.rc('font', family='serif')
    plt.rc('font', size=16)

    param = SphaleronParameters()

    print('Plotting prefactors')

    xi_pts = np.logspace(-2., 1., 201)
    xi_pts2 = np.logspace(-1.65, 1., 201)

    # rotations and translations
    fig = plt.figure(dpi=150, figsize=[6., 6.])
    axis = fig.add_subplot(111)
    plt.xlim([1e-2, 1e1])
    plt.ylim([6., 16.])

    plt.xscale('log')

    plt.plot(xi_pts, param.N_rot_interp(xi_pts), label=r"$\mathcal{N}_{rot}$ based on Carson \& McLerran 90",
             color='k')
    #plt.plot(param.xi_grid, param.N_rot_disc(),
    #         label="Points used for digitization of $N_{rot}$",
    #         color='k', marker='x', linestyle="None")

    plt.plot(xi_pts, param.N_tr_interp(xi_pts), label=r"$\mathcal{N}_{tr}$ based on Carson \& McLerran 90",
             color='b')
    #plt.plot(param.xi_grid, param.N_tr_disc(),
    #         label=r"Points used for digitization of $N_{rot}$",
    #         color ='b', marker='x', linestyle="None")

    plt.title("Normalization Prefactors")
    plt.legend()
    plt.xlabel(r"$\lambda / g^2$")
    plt.savefig('prefactors_N.png')
    plt.show()
    plt.close()


    # omega minus
    fig = plt.figure(dpi=150, figsize=[6., 6.])
    axis = fig.add_subplot(111)
    plt.xlim([1e-2, 1e1])
    plt.ylim([.4, 1.8])

    plt.xscale('log')

    plt.plot(xi_pts, param.omega_minus_interp(xi_pts),
             label=r"$\omega_-^2$ based on Carson \& McLerran 90", color='k')

    #plt.plot(param.xi_grid, param.omega_minus_disc(),
    #         label=r"Points used for digitization of $\omega_-$",
    #         color='k', marker='x', linestyle="None")

    plt.title(r"Prefactor $\omega_-$")
    plt.legend()
    plt.xlabel(r"$\lambda / g^2$")
    plt.ylabel(r"$\omega_-^2 / (gv)^2$")
    plt.savefig('prefactor_omega.png')
    plt.show()
    plt.close()


    # kappa
    fig = plt.figure(dpi=150, figsize=[6., 6.])
    axis = fig.add_subplot(111)
    plt.xlim([1e-2, 1e1])
    plt.ylim([-50., 5])

    plt.xscale('log')

    #plt.plot(xi_pts2, param.ln_kappa_interp_line(xi_pts2),
    #         label="$\log \kappa$ based on EP as in Baacke \& Junker 94", color = 'k')
    #plt.plot(param.kappa_grid_line, param.ln_kappa_disc_line(),
    #         label="Points used for digitization of $\log\kappa$",
    #         color ='k', marker='x', linestyle="None")

    # taken from above
    kappa_grid = np.array([.02, .031, .045, .08, .125, .281, .5])

    plt.plot([xi_pts[0], xi_pts[-1]], [0., 0.], marker=None, color='k', lw=1.)

    plt.plot(kappa_grid, param.ln_kappa_disc(),
             label=r"$\ln \kappa$ based on Baacke \& Junker 94",
             color='b', marker='s', linestyle="None", markersize=4)
    #plt.plot(xi_pts, param.ln_kappa_interp(xi_pts),
    #         label = "Points used for digitization of $\log\kappa$ interpolation",
    #         color = 'b')

    axis.fill_between(xi_pts, param.ln_kappa_interp(xi_pts)-np.log(100.),
                      param.ln_kappa_interp(xi_pts)+np.log(100.), alpha=0.2, color='b',
                      label=r'Uncertainty on $\kappa: [0.01\kappa, 100 \kappa]$')

    #axis.fill_between([1e-2, 1e1], [np.log(1e-1),np.log(1e-1)], [np.log(1e-4),np.log(1e-4)],
    #                  alpha=0.2, color='green', label='$10^{-4} < \kappa < 10^{-1}$')

    plt.title(r"Prefactor $\kappa$")
    plt.legend(loc='lower right')
    plt.xlabel(r"$\lambda / g^2$")
    plt.ylabel(r"$\ln{\kappa}$")
    plt.savefig('prefactor_kappa.png')
    plt.show()
    plt.close()

    # B function (Energy prefactor)
    fig = plt.figure(dpi=150, figsize=[6., 6.])
    axis = fig.add_subplot(111)
    plt.xlim([1e-2, 1e1])
    plt.ylim([1.5, 2.5])

    plt.xscale('log')

    plt.plot(xi_pts, param.B_func_interp(xi_pts),
             label=r"Sphaleron Energy prefactor $B$", color='k')

    #plt.plot(param.xi_grid, param.B_func_disc(),
    #         label=r"Points used for digitization of $B$",
    #         color='k', marker='x', linestyle="None")

    plt.title(r"Sphaleron Energy Prefactor $B$")
    plt.legend()
    plt.xlabel(r"$\lambda / g^2$")
    plt.ylabel(r"$B$")
    plt.savefig('prefactor_B.png')
    plt.show()
    plt.close()
