""" class to handle the RGE evolution, optionally with wavefunction renormalization included"""
import numpy as np

class RGE:
    def __init__(self, N):
        self.N = N

    def rge(self, t, c):
        gs, g, gp, mu2, b2, lbda, b4, a2, c2, c4, ct, dt, at, c5, Yt, omega = c

        gsq = 3.*(g**2) + (gp**2)
        gqu = 3.*(g**4) + 2.*(g**2)*(gp**2) + (gp**4)
        gqu_m = 3.*(g**4) - 2.*(g**2)*(gp**2) + (gp**4)

        beta_gs = -7.* (gs**3)
        beta_g = -3.* (g**3)
        beta_gp = 7.* (gp**3)
        beta_mu2 = -(4.*a2*b2) - (at*b2) - (mu2*(-12.*lbda + 1.5*gsq - 6.*(Yt**2)))\
                   - (4. * self.N * c2 * dt)
        beta_b2 = -(4.*a2*mu2) - (at*mu2) - (b2*(-12.*b4 + 1.5*gsq)) - (self.N * c2 * ct)
        beta_lbda = (2.*(a2**2)) + (a2*at) + ((at**2)/4.) + (24.*lbda**2) - (3.*lbda*gsq)\
                    + ((3./8.)*gqu) + (12.*lbda*Yt**2) - (6.*Yt**4) + (8.*self.N*dt**2)
        beta_b4 = (2.*(a2**2)) + (a2*at) + ((at**2)/4.) + (24.*b4**2) - (3.*b4*gsq)\
                  + ((3./8.)*gqu)  + (0.5*self.N*ct**2)
        beta_a2 = ((3./4.)*gqu_m) + (4.*(a2**2)) + ((at**2)/2.) + a2*(12.*lbda + 12.* b4 - 3.*gsq)\
                  + (2.*at*(lbda+b4)) + (6.*a2*Yt**2)
        beta_c2 = (4.*b2*ct) + (6.*c4*c2) - (16. * mu2 * dt) + (2.*self.N*c2*c5) + (4.*c2*c5)
        beta_c4 = (18.*(c4**2)) + (24.*c4 * c5)
        beta_ct = ct*((-1.5*gsq) + (12.*b4) + (6.*c4) + (4.*ct) + (2.*self.N*c5)\
                      + (4.*c5)) + (16.*a2*dt) + (4.*at*dt)
        beta_dt = dt*((-1.5*gsq) + (12.*lbda) + (6.*c4) + (16.*dt) + (2.*self.N*c5)\
                      + (4.*c5) + (6.*Yt**2)) + (a2*ct) + (at*ct/4.)
        beta_at = (6.*g**2 * gp**2) - 3.*at*gsq + 6.*at*Yt**2 + 4.*at*(b4+lbda)\
                  + 8.*a2*at + 2.*at**2
        beta_c5 = (2.*ct**2) + (32*dt**2) + (16.*c5**2) + (12.*c4*c5) + (2.*self.N*c5**2)
        beta_Yt = -(8.*Yt*(gs**2)) - ((9./4.)*Yt*(g**2))\
                  - ((17./12.)*Yt*(gp**2)) + ((9./2.)*(Yt**3))
        beta_omega = 2. * (mu2**2 + b2**2 + 0.25*self.N*c2**2)

        pre = 1./(16.*np.pi**2)

        ret = pre * np.array([beta_gs, beta_g, beta_gp, beta_mu2, beta_b2, beta_lbda, beta_b4,\
                              beta_a2, beta_c2, beta_c4, beta_ct, beta_dt, beta_at, beta_c5,\
                              beta_Yt, beta_omega])
        #print("ret: ", ret)
        return ret

    def rge_wfr(self, t, c):
        """ Same as above, but with 2 more in- and outputs"""
        rge_ret = self.rge(t, c[:-2])
        _, g, gp, _, _, _, _, _, _, _, _, _, _, _, Yt, _, _, _ = c
        gsq = 3.*(g**2) + (gp**2)

        gamma_h = - gsq + 3.* Yt**2
        gamma_phi = -gsq

        pre = 1./(16.*np.pi**2)

        ret = np.array([*rge_ret, pre*gamma_h, pre*gamma_phi])

        return ret
