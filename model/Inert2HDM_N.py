""" Contains the model definition. """

import numpy as np
import numpy.linalg as LA
from scipy.integrate import solve_ivp
from scipy.optimize import fsolve
from scipy import optimize

from cosmoTransitions import generic_potential
from cosmoTransitions.finiteT import Jb_spline as Jb
from cosmoTransitions.finiteT import Jf_spline as Jf

import helper.helper as helper
from helper.helper import partial_derivative as part_der
import helper.highT as highT

from .RGE import RGE

v2 = 246.**2
mh = 125.

class model_2HDM_N(generic_potential.generic_potential): # pylint: disable=invalid-name

    def __init__(self, *, b2, b4, a2, c2, c4, ct, dt, at, c5, N, omega=0.,\
                 use_CW=0., use_Daisy=0., use_RGE=False, use_CW_in_delta_m=True,
                 use_delta_m_in_ren_scale=False):
        """ Class containing the model we stuy. All model parameters
            are required at initialization. The notation is as in the
            .pdf of our notes.

        b2 mass**2 of the Inert Doublet
        b4 quartic coupling of Inert Doublet
        a2 portal coupling of Inert to Higgs Doublet
        c2 mass of chis
        c4 coupling of chi_i**4
        ct portal coupling of Inert to chi
        dt portal coupling of Higgs to chi
        at second portal if Inert to Higgs
        c5 coupling of (sum chi_i**2)**2
        N number of chis
        omega vacuum energy term, default 0

        CW switch to use Coleman-Weinberg potential (0. OFF, 1. ON)
        Daisy switch to include thermal mass correction
        (0. OFF; 1. hard thermal; 2. improved 1-step; 3 improved 2-step 4 gap eq.)
        RGE switch to use RGE improved potential (boolean)
        The use of RGE is NOT fully vectorized: len(field.shape) <= 2 !!!
        """

        # This first line is absolutely essential in cosmoTransitions.
        # It specifies the number of field-dimensions in the theory.
        # = Higgs + real component of 2nd doublet + chi contribution
        # note that we consider all chi_i to be dynamically at the same value

        self.Ndim = 3 # should be 2+N, but that's too much for cosmotransitions
        self.num_boson_dof = None
        self.num_fermion_dof = None

        # number of chi_i fields
        self.N = N

        # setting switches
        self.use_CW = use_CW
        self.use_Daisy = use_Daisy
        self.use_RGE = use_RGE
        self.use_CW_in_delta_m = use_CW_in_delta_m
        self.use_delta_m_in_ren_scale = use_delta_m_in_ren_scale

        self.store_delta_m_2 = {}
        self.store_delta_m_3 = {}
        self.store_delta_m_4 = {}
        # minima dictionary with daisy i:
        self.minima_dict_wd0 = {}
        self.minima_dict_wd1 = {}
        self.minima_dict_wd2 = {}
        self.minima_dict_wd3 = {}
        self.minima_dict_wd4 = {}
        #self.store_delta_m['daisy_type'] = self.use_Daisy

        # RGE span
        self.scale_end_upper = 1e7
        self.scale_end_lower = 1.


        # These are the parameters of the model that are not inputs
        # (given at ew scales)
        # gs: alpha_s(mZ) = 0.1179 => gs(mZ) = 1.2171996941475736
        # gs(246 GeV) = 1.1448654178582776
        self.gs = 1.1448654178582776 # g_strong
        self.dict_gs = {}
        self.g = 0.65 # g weak
        self.dict_g = {}
        self.gp = 0.35 # g hypercharge
        self.dict_gp = {}
        self.yt = 1. # top yukawa
        self.dict_yt = {}

        self.b2 = b2
        self.dict_b2 = {}
        self.b4 = b4
        self.dict_b4 = {}
        self.a2 = a2
        self.dict_a2 = {}
        self.c2 = c2
        self.dict_c2 = {}
        self.c4 = c4
        self.dict_c4 = {}
        self.ct = ct
        self.dict_ct = {}
        self.dt = dt
        self.dict_dt = {}
        self.at = at
        self.dict_at = {}
        self.c5 = c5
        self.dict_c5 = {}
        self.omega = omega
        self.dict_omega = {}
        self.dict_wfr_h = {}
        self.dict_wfr_phi = {}
        self.lbdaSM = mh**2 / (2.*v2)
        self.mu2SM = (mh**2)/2.
        self.dict_lbda = {}
        self.dict_mu2 = {}

        self.couplings_str = ['gs', 'g', 'gp', 'mu2', 'b2', 'lbda', 'b4', 'a2', 'c2',\
                              'c4', 'ct', 'dt', 'at', 'c5', 'yt', 'omega']
        # doesn't contain mu and lbda (list needed to solve boundary):
        self.couplings_list = (self.gs, self.g, self.gp, self.b2, \
                              self.b4, self.a2, self.c2, self.c4,\
                              self.ct, self.dt, self.at, self.c5, self.yt, self.N)

        if self.use_CW == 1:
            print("Coleman-Weiberg ON")
            self.lbda, self.mu2 = fsolve(SolveCWBoundary, (0.1, 8.8), args=self.couplings_list)
            self.mu2 *= 1e3
            print("lambda: "+str(self.lbda))
            print("mu2: "+str(self.mu2))
            #print("Solution of boundary conditions (should be 0): "+\
            #    str(SolveCWBoundary((self.lbda,self.mu2), *self.couplings_list)))
            Vsol, Vsol2 = SolveCWBoundary((self.lbda, self.mu2*1e-3), *self.couplings_list)
            if Vsol > 1e-1 or Vsol2 > 1e-1:
                raise ValueError("Boundary conditions could not be implemented correctly!",\
                                 Vsol, Vsol2)

        elif self.use_CW == 0:
            print("Coleman-Weiberg OFF")
            self.lbda = self.lbdaSM
            self.mu2 = self.mu2SM
            print("lambda: "+str(self.lbda))
            print("mu2: "+str(self.mu2))
        else:
            raise ValueError("use_CW flag must be 1 or 0 !!!")

        if self.use_RGE:
            print("Solving RGE")
            class_RGE = RGE(self.N)
            current_RGE = class_RGE.rge_wfr
            self.scale_start = np.sqrt(v2)
            scale_delta = 1e-1
            scale_span_upper = [np.log(self.scale_start), np.log(self.scale_end_upper)]
            scale_span_lower = [np.log(self.scale_start), np.log(self.scale_end_lower)]

            c_0 = np.array([self.gs, self.g, self.gp, 1e-4*self.mu2, 1e-4*self.b2, self.lbda, self.b4,\
                            self.a2, 1e-4*self.c2, self.c4, self.ct, self.dt, self.at, self.c5,\
                            self.yt, 1e-8*self.omega, 1., 1.])
            scale_eval_upper = np.arange(np.log(self.scale_start)+scale_delta,\
                                         np.log(self.scale_end_upper), scale_delta)
            scale_eval_lower = np.arange(np.log(self.scale_start)-scale_delta,\
                                         np.log(self.scale_end_lower), -scale_delta)

            self.rge_sol_upper = solve_ivp(current_RGE, scale_span_upper, c_0, t_eval=\
                                           scale_eval_upper, dense_output=True, method='DOP853')
            print("RGE solver of upper region, between {} GeV and {:2.1e} GeV:".format(self.scale_start,\
                                                                            self.scale_end_upper))
            print(self.rge_sol_upper.message)
            self.rge_sol_lower = solve_ivp(current_RGE, scale_span_lower, c_0, t_eval=\
                                           scale_eval_lower, dense_output=True, method='DOP853')
            print("RGE solver of lower region, between {} GeV and {} GeV:".format(self.scale_start,\
                                                                            self.scale_end_lower))
            print(self.rge_sol_lower.message)
            if not self.rge_sol_upper.success or not self.rge_sol_lower.success:
                raise RuntimeError("The RGE could not be integrated successfully!")
            for i, cp in enumerate(self.couplings_str):
                scaling = 1.
                if cp in ['b2', 'mu2', 'c2']:
                    scaling = 1e4
                elif cp=='omega':
                    scaling = 1e8
                print("{} at scale {:1.2e} GeV is {}".format(cp, self.scale_end_upper, 
                                                          scaling*self.rge_sol_upper.sol(np.log(self.scale_end_upper))[i]))
        else:
            print("RGE solving OFF")
        print("-->  Model initialized successfully  <--")

    def current_scale(self, field, temp):
        """ Returns renormalization scale at given point X.
            It is defined as the largest eigenvalue of the
            mass matrix for deltaM(T) = 0.
            Since there is (currently) no vectorized root
            finder in scipy, I need a for-loop here.

            if use_delta_m_in_ren_scale is set, the thermal mass
            in high-T approximation is added. 

        """
        temp = np.array([temp])
        if len(temp.shape) == 2:
            temp = temp.squeeze()
            assert len(temp) == len(field)
        elif len(temp.shape) == 1:
            if len(field.shape) == 2:
                temp = np.tile(temp, len(field))
            elif len(field.shape) == 1:
                temp = temp
            else:
                raise ValueError("Unexpected field shape {}".format(field.shape))
        else:
            raise ValueError("Unexpected temperature shape {}".format(temp.shape))

        def mass(scale, field_value, local_temp):
            assert isinstance(local_temp, float)
            if self.use_delta_m_in_ren_scale:
                deltaM = self.HighT_Daisy(helper.convert_to_full(field_value), local_temp, scale=scale)
            else:
                deltaM = None
                local_temp = 0.
            masses, _, _ = self.boson_massSq(field_value, T=local_temp, scale=scale, deltaM=deltaM)
            mass = np.amax(np.sqrt(np.abs(masses)), axis=-1)
            #mass = np.amax(np.sqrt(masses + 0j).real, axis=-1)
            return mass - scale
        in_shape = field.shape
        out_shape = list(in_shape)
        out_shape = tuple(out_shape[:-1])
        flat_field = field.reshape(-1, in_shape[-1])
        ret = np.zeros(out_shape)
        if out_shape == ():
            ret = optimize.brentq(mass, self.scale_end_upper, self.scale_end_lower,
                                  args=(flat_field, temp[0]))
        else:
            for counter, value in enumerate(flat_field):
                ret[counter] = optimize.brentq(mass, self.scale_end_upper,
                                               self.scale_end_lower, args=(value, temp[counter]))
            ret = ret.reshape(out_shape)
        return np.maximum(ret, 246.)

    def check_coupling_dict(self, dictionary, scales, identifier):
        """ loops through scales and
        checks if dictionary[scale] exists,
        if yes, dictionary[scale] is added to return
        if not, func(scale)[identifier] is computed and added to dict and returned
        """
        scales = np.array(scales)
        scales_shape = scales.shape
        scales_flat = scales.flatten()
        compute_list = []
        compute_index = []
        existing_list = []
        existing_index = []

        # check if already stored:
        for scale_index, scale in enumerate(scales_flat):
            scale_hash = np.around(scale, 3)
            if scale_hash in dictionary:
                existing_list.append(dictionary[scale_hash])
                existing_index.append(scale_index)
            else:
                compute_index.append(scale_index)
                compute_list.append(scale)
        compute_list = np.array(compute_list)
        compute_index = np.array(compute_index).astype(int)
        existing_list = np.array(existing_list)
        existing_index = np.array(existing_index).astype(int)

        # compute new points
        if compute_list.size > 0:
            if compute_list.any() < self.scale_end_lower or\
               compute_list.any() > self.scale_end_upper:
                raise ValueError("Coupling evaluated out of pre-selected boundaries: "+\
                                 str(compute_list))

            computed_value = np.where(compute_list < self.scale_start,
                                      self.rge_sol_lower.sol(np.log(compute_list))[identifier],
                                      self.rge_sol_upper.sol(np.log(compute_list))[identifier])

            # add computed points to storage
            for index, value in enumerate(computed_value):
                scale_hash = np.around(compute_list[index], 3)
                dictionary[scale_hash] = value
        else:
            computed_value = np.array([])

        # combine them back together and reshape
        if existing_list.size == 0:
            ret = computed_value
        elif compute_list.size == 0:
            ret = existing_list
        else:
            ret = np.zeros_like(scales_flat)
            for index, value in enumerate(computed_value):
                ret[compute_index[index]] = value
            for index, value in enumerate(existing_list):
                ret[existing_index[index]] = value

        ret = ret.reshape(scales_shape)
        return ret

    def cp_gs(self, scale):
        """ Running value of gs. """
        if not self.use_RGE:
            return self.gs
        res = self.check_coupling_dict(self.dict_gs, scale, 0)
        return res

    def cp_g(self, scale):
        """ Running value of g. """
        if not self.use_RGE:
            return self.g
        res = self.check_coupling_dict(self.dict_g, scale, 1)
        return res

    def cp_gp(self, scale):
        """ Running value of gp. """
        if not self.use_RGE:
            return self.gp
        res = self.check_coupling_dict(self.dict_gp, scale, 2)
        return res

    def cp_mu2(self, scale):
        """ Running value of mu2. """
        if not self.use_RGE:
            return self.mu2
        res = 1e4*self.check_coupling_dict(self.dict_mu2, scale, 3)
        return res

    def cp_b2(self, scale):
        """ Running value of b2. """
        if not self.use_RGE:
            return self.b2
        res = 1e4*self.check_coupling_dict(self.dict_b2, scale, 4)
        return res

    def cp_lbda(self, scale):
        """ Running value of lambda. """
        if not self.use_RGE:
            return self.lbda
        res = self.check_coupling_dict(self.dict_lbda, scale, 5)
        return res

    def cp_b4(self, scale):
        """ Running value of b4. """
        if not self.use_RGE:
            return self.b4
        res = self.check_coupling_dict(self.dict_b4, scale, 6)
        return res

    def cp_a2(self, scale):
        """ Running value of a2. """
        if not self.use_RGE:
            return self.a2
        res = self.check_coupling_dict(self.dict_a2, scale, 7)
        return res

    def cp_c2(self, scale):
        """ Running value of c2. """
        if not self.use_RGE:
            return self.c2
        res = 1e4*self.check_coupling_dict(self.dict_c2, scale, 8)
        return res

    def cp_c4(self, scale):
        """ Running value of c4. """
        if not self.use_RGE:
            return self.c4
        res = self.check_coupling_dict(self.dict_c4, scale, 9)
        return res

    def cp_ct(self, scale):
        """ Running value of ct. """
        if not self.use_RGE:
            return self.ct
        res = self.check_coupling_dict(self.dict_ct, scale, 10)
        return res

    def cp_dt(self, scale):
        """ Running value of dt. """
        if not self.use_RGE:
            return self.dt
        res = self.check_coupling_dict(self.dict_dt, scale, 11)
        return res

    def cp_at(self, scale):
        """ Running value of at. """
        if not self.use_RGE:
            return self.at
        res = self.check_coupling_dict(self.dict_at, scale, 12)
        return res

    def cp_c5(self, scale):
        """ Running value of c5. """
        if not self.use_RGE:
            return self.c5
        res = self.check_coupling_dict(self.dict_c5, scale, 13)
        return res

    def cp_yt(self, scale):
        """ Running value of yt. """
        if not self.use_RGE:
            return self.yt
        res = self.check_coupling_dict(self.dict_yt, scale, 14)
        return res

    def cp_omega(self, scale):
        """ Running value of omega. """
        if not self.use_RGE:
            return self.omega
        res = 1e8*self.check_coupling_dict(self.dict_omega, scale, 15)
        return res

    def wfr_higgs(self, scale):
        """ Wave-function renormalization of Higgs. """
        if not self.use_RGE:
            return 1.
        res = self.check_coupling_dict(self.dict_wfr_h, scale, 16)
        return res

    def wfr_phi(self, scale):
        """ Wave-function renormalization of Inert. """
        if not self.use_RGE:
            return 1.
        res = self.check_coupling_dict(self.dict_wfr_phi, scale, 17)
        return res

    def forbidPhaseCrit(self, X):
        """ Method of CosmoTransition, allows only positive phases. """
        X = np.array(X)
        return ((X[..., 0] < -5.0) or (X[..., 1] < -5.0) or (X[..., 2] < -5.0)).any()

    def V0(self, X, scale=np.sqrt(v2)):
        """ Method of CosmoTransition. Returns tree-level potential """

        # X is the input field array. It is helpful to ensure that it is a
        # numpy array before splitting it into its components.
        X = np.asanyarray(X)
        assert X.shape[-1] == 2 + self.N

        # 1st component of X is h, 2nd is phi, 3rd is chi
        h = X[..., 0]
        phi = X[..., 1]
        chi_vec = X[..., 2:]

        #for only 3-dim input:
        #chi = X[..., 2]
        #chi_vec = np.array(self.N*[chi])
        #chi_vec = np.moveaxis(chi_vec, 0, -1)

        h2 = h**2
        phi2 = phi**2
        chi_vec2 = chi_vec**2

        # tree-level potential at T = 0.
        potential_h = -0.5 * self.cp_mu2(scale) * h2 + 0.25 * self.cp_lbda(scale) * h2**2
        potential_phi = 0.5 * self.cp_b2(scale) * phi2 +0.25 * self.cp_b4(scale) * phi2**2
        potential_chi = self.cp_c2(scale)*np.sum(0.5 * chi_vec2, axis=-1)
        potential_chi += self.cp_c4(scale)*np.sum(0.25 * chi_vec2**2, axis=-1)
        potential_chi += 0.25 * self.cp_c5(scale) * np.sum(chi_vec2, axis=-1)**2
        potential_cross = (0.25 * self.cp_a2(scale) + 0.125 * self.cp_at(scale)) * phi2 * h2\
                + 0.25 * self.cp_ct(scale) * np.sum(chi_vec2, axis=-1) * phi2\
                + self.cp_dt(scale) * np.sum(chi_vec2, axis=-1) * h2
        ret = potential_h + potential_phi + potential_chi + potential_cross - self.cp_omega(scale)
        return ret

    def Full_Matrix(self, full_X, scale=np.sqrt(v2)):
        """ Returns (8+N)x(8+N) mass matrix with full field dependence.
            Is properly vectorized if array of entries is given. input is full_X (8+N):
            h, hGB1, hGB2, hGB3, phi, phiGB1, phiGB2, phiGB3, chi_i
        """
        full_X = np.array(full_X)
        assert full_X.shape[-1] == 8 + self.N

        h = full_X[..., 0:4]
        phi = full_X[..., 4:8]
        chi = full_X[..., 8:]

        h2 = h**2
        h2_sum = np.sum(h2, axis=-1)

        phi2 = phi**2
        phi2_sum = np.sum(phi2, axis=-1)

        chi2 = chi**2
        chi2_sum = np.sum(chi2, axis=-1)

        zero = np.zeros((full_X[..., 0].shape))
        # one = np.ones((X[..., 0].shape))

        dim_mat = 8+self.N
        MassMatrix = np.multiply.outer(np.ones((dim_mat, dim_mat)), zero)

        # define couplings locally to save time:
        cp_lbda = self.cp_lbda(scale)
        cp_mu2 = self.cp_mu2(scale)
        cp_a2 = self.cp_a2(scale)
        cp_at = self.cp_at(scale)
        cp_dt = self.cp_dt(scale)
        cp_b4 = self.cp_b4(scale)
        cp_b2 = self.cp_b2(scale)
        cp_ct = self.cp_ct(scale)
        cp_c2 = self.cp_c2(scale)
        cp_c4 = self.cp_c4(scale)
        cp_c5 = self.cp_c5(scale)

        # Higgs:
        MassMatrix[0, 0] = cp_lbda*(h2_sum + 2.*h2[..., 0]) - cp_mu2 +\
                           0.5*cp_a2*phi2_sum + 0.25 * cp_at *\
                          (phi2[..., 0]+phi2[..., 3]) + 2.*cp_dt*chi2_sum

        MassMatrix[0, 1] = MassMatrix[1, 0] = 0.25*cp_at*(phi[..., 0]*phi[..., 1] +\
                                                                      phi[..., 2]*phi[..., 3]) +\
                                            2.*cp_lbda*h[..., 0]*h[..., 1]

        MassMatrix[0, 2] = MassMatrix[2, 0] = 0.25*cp_at*(phi[..., 0]*phi[..., 2]-\
                                                                      phi[..., 1]*phi[..., 3])+\
                                            2.*cp_lbda*h[..., 0]*h[..., 2]

        MassMatrix[0, 3] = MassMatrix[3, 0] = 2.*cp_lbda*h[..., 0] * h[..., 3]

        MassMatrix[1, 1] = cp_lbda*(h2_sum + 2.*h2[..., 1]) - cp_mu2 +\
                          0.5*cp_a2*phi2_sum + 0.25*cp_at*\
                          (phi2[..., 1] + phi2[..., 2]) + 2.*cp_dt*chi2_sum

        MassMatrix[1, 2] = MassMatrix[2, 1] = 2.*cp_lbda*h[..., 1]*h[..., 2]

        MassMatrix[1, 3] = MassMatrix[3, 1] = 0.25*cp_at*(phi[..., 1]*phi[..., 3]-\
                                                                      phi[..., 0]*phi[..., 2])+\
                                            2.*cp_lbda * h[..., 1] * h[..., 3]

        MassMatrix[2, 2] = cp_lbda*(h2_sum + 2.*h2[..., 2]) - cp_mu2 +\
                          0.5*cp_a2*phi2_sum + 0.25*cp_at*\
                          (phi2[..., 1] + phi2[..., 2]) + 2.*cp_dt*chi2_sum

        MassMatrix[2, 3] = MassMatrix[3, 2] = 0.25*cp_at*(phi[..., 2]*phi[..., 3] +\
                                                                      phi[..., 0]*phi[..., 1])+\
                                            2.*cp_lbda*h[..., 2]*h[..., 3]

        MassMatrix[3, 3] = cp_lbda*(h2_sum + 2.*h2[..., 3]) - cp_mu2 +\
                          0.5*cp_a2*phi2_sum + 0.25*cp_at*(phi2[..., 0]+\
                                                                                   phi2[..., 3]) +\
                        2.*cp_dt*chi2_sum

        # Phi
        MassMatrix[4, 4] = cp_b4*(phi2_sum + 2.*phi2[..., 0]) + cp_b2 +\
                          0.5*cp_a2*h2_sum + 0.25*cp_at*\
                          (h2[..., 0]+h2[..., 3]) + 0.5*cp_ct * chi2_sum

        MassMatrix[4, 5] = MassMatrix[5, 4] = 0.25*cp_at*(h[..., 0]*h[..., 1] +\
                                                                      h[..., 2]*h[..., 3])+\
                                            2.*cp_b4 * phi[..., 0]*phi[..., 1]

        MassMatrix[4, 6] = MassMatrix[6, 4] = 0.25*cp_at*(h[..., 0]*h[..., 2]-\
                                                                      h[..., 1]*h[..., 3])+\
                                            2.*cp_b4*phi[..., 0]*phi[..., 2]

        MassMatrix[4, 7] = MassMatrix[7, 4] = 2.*cp_b4*phi[..., 0]*phi[..., 3]

        MassMatrix[5, 5] = cp_b4*(phi2_sum + 2.*phi2[..., 1]) + cp_b2 +\
                          0.5*cp_a2*h2_sum + 0.25*cp_at*\
                          (h2[..., 1]+h2[..., 2]) + 0.5*cp_ct*chi2_sum

        MassMatrix[5, 6] = MassMatrix[6, 5] = 2.*cp_b4*phi[..., 1]*phi[..., 2]
        MassMatrix[5, 7] = MassMatrix[7, 5] = 0.25*cp_at *\
                           (h[..., 1]*h[..., 3]-h[..., 0]*h[..., 2])+\
                           2.*cp_b4*phi[..., 1]*phi[..., 3]

        MassMatrix[6, 6] = cp_b4*(phi2_sum + 2.*phi2[..., 2]) + cp_b2 +\
                          0.5*cp_a2*h2_sum + 0.25*cp_at*\
                          (h2[..., 1]+h2[..., 2]) + 0.5*cp_ct*chi2_sum

        MassMatrix[6, 7] = MassMatrix[7, 6] = 0.25*cp_at*(h[..., 0]*h[..., 1] +\
                                                                      h[..., 2]*h[..., 3])+\
                                            2.*cp_b4*phi[..., 2]*phi[..., 3]

        MassMatrix[7, 7] = cp_b4*(phi2_sum + 2.*phi2[..., 3]) + cp_b2 +\
                          0.5*cp_a2*h2_sum + 0.25*cp_at*\
                          (h2[..., 0]+h2[..., 3]) + 0.5*cp_ct*chi2_sum

        # Higgs-Phi
        MassMatrix[0, 4] = MassMatrix[4, 0] = cp_a2*h[..., 0]*phi[..., 0]+\
                        0.25*cp_at*(2.*h[..., 0]*phi[..., 0]+h[..., 1]*phi[..., 1]+\
                                                h[..., 2]*phi[..., 2])

        MassMatrix[1, 4] = MassMatrix[4, 1] = cp_a2*h[..., 1]*phi[..., 0]+\
                        0.25*cp_at*(h[..., 0]*phi[..., 1]-h[..., 3]*phi[..., 2])

        MassMatrix[2, 4] = MassMatrix[4, 2] = cp_a2*h[..., 2]*phi[..., 0]+\
                        0.25*cp_at*(h[..., 0]*phi[..., 2]+h[..., 3]*phi[..., 1])

        MassMatrix[3, 4] = MassMatrix[4, 3] = cp_a2*h[..., 3]*phi[..., 0]+\
                        0.25*cp_at*(2.*h[..., 3]*phi[..., 0]-h[..., 1]*phi[..., 2]+\
                                                h[..., 2]*phi[..., 1])

        MassMatrix[0, 5] = MassMatrix[5, 0] = cp_a2*h[..., 0]*phi[..., 1]+\
                        0.25*cp_at*(h[..., 1]*phi[..., 0]-h[..., 2]*phi[..., 3])

        MassMatrix[1, 5] = MassMatrix[5, 1] = cp_a2*h[..., 1]*phi[..., 1]+\
                        0.25*cp_at*(h[..., 0]*phi[..., 0]+2.*h[..., 1]*phi[..., 1]+\
                                                h[..., 3]*phi[..., 3])

        MassMatrix[2, 5] = MassMatrix[5, 2] = cp_a2*h[..., 2]*phi[..., 1]+\
                        0.25*cp_at*(-h[..., 0]*phi[..., 3]+2.*h[..., 2]*phi[..., 1]+\
                                                h[..., 3]*phi[..., 0])

        MassMatrix[3, 5] = MassMatrix[5, 3] = cp_a2*h[..., 3]*phi[..., 1]+\
                        0.25*cp_at*(h[..., 1]*phi[..., 3]+h[..., 2]*phi[..., 0])

        MassMatrix[0, 6] = MassMatrix[6, 0] = cp_a2*h[..., 0]*phi[..., 2]+\
                        0.25*cp_at*(h[..., 1]*phi[..., 3]+h[..., 2]*phi[..., 0])

        MassMatrix[1, 6] = MassMatrix[6, 1] = cp_a2*h[..., 1]*phi[..., 2]+\
                        0.25*cp_at*(h[..., 0]*phi[..., 3] +\
                                                2.*h[..., 1]*phi[..., 2]-h[..., 3]*phi[..., 0])

        MassMatrix[2, 6] = MassMatrix[6, 2] = cp_a2*h[..., 2]*phi[..., 2]+\
                        0.25*cp_at*(h[..., 0]*phi[..., 0] +\
                                                2.*h[..., 2]*phi[..., 2]+h[..., 3]*phi[..., 3])

        MassMatrix[3, 6] = MassMatrix[6, 3] = cp_a2*h[..., 3]*phi[..., 2]+\
                        0.25*cp_at*(-h[..., 1]*phi[..., 0]+h[..., 2]*phi[..., 3])

        MassMatrix[0, 7] = MassMatrix[7, 0] = cp_a2*h[..., 0]*phi[..., 3]+\
                        0.25*cp_at*(2.*h[..., 0]*phi[..., 3] +\
                                                h[..., 1]*phi[..., 2]-h[..., 2]*phi[..., 1])

        MassMatrix[1, 7] = MassMatrix[7, 1] = cp_a2*h[..., 1]*phi[..., 3]+\
                        0.25*cp_at*(h[..., 0]*phi[..., 2]+h[..., 3]*phi[..., 1])

        MassMatrix[2, 7] = MassMatrix[7, 2] = cp_a2*h[..., 2]*phi[..., 3]+\
                        0.25*cp_at*(-h[..., 0]*phi[..., 1]+h[..., 3]*phi[..., 2])

        MassMatrix[3, 7] = MassMatrix[7, 3] = cp_a2*h[..., 3]*phi[..., 3]+\
                        0.25*cp_at*(h[..., 0]*phi[..., 0] +\
                                                h[..., 2]*phi[..., 2]+2.*h[..., 3]*phi[..., 3])

        rolled_h = np.rollaxis(h, -1)
        rolled_phi = np.rollaxis(phi, -1)
        rolled_chi = np.rollaxis(chi, -1)

        higgs_chi_matrix = 4.* cp_dt * rolled_h[np.newaxis, ...] * rolled_chi[:, np.newaxis, ...]
        MassMatrix[8:, 0:4] += higgs_chi_matrix
        MassMatrix[0:4, 8:] += np.rollaxis(higgs_chi_matrix, 1)

        inert_chi_matrix = cp_ct * rolled_phi[np.newaxis, ...] * rolled_chi[:, np.newaxis, ...]
        MassMatrix[8:, 4:8] += inert_chi_matrix
        MassMatrix[4:8, 8:] += np.rollaxis(inert_chi_matrix, 1)

        chi_matrix = 2.*cp_c5* rolled_chi[np.newaxis, ...] * rolled_chi[:, np.newaxis, ...]
        MassMatrix[8:, 8:] += chi_matrix

        # timeit runs said this is faster:
        for i in range(self.N):
            MassMatrix[8+i, 8+i] += cp_c2 + 3.*cp_c4*chi2[..., i] +\
                cp_c5*chi2_sum + 0.5*cp_ct*phi2_sum +\
                2.*cp_dt*h2_sum

        # than this:
        #MassMatrix[8:, 8:] += cp_c2*np.multiply.outer(np.eye(N), one) +\
        #    2.*cp_dt*np.multiply.outer(np.eye(N), h2_sum) +\
        #    cp_c5*np.multiply.outer(np.eye(N),chi2_sum) +\
        #    0.5*cp_ct*np.multiply.outer(np.eye(N),phi2_sum) +\
        #    3. * cp_c4 * np.multiply.outer(np.eye(N), one) * rolled_chi2[np.newaxis, ...]

        return MassMatrix

    def V1_Gap(self, X, T, deltaM, scale=np.sqrt(v2)):
        """ Constructs V1 as needed for the Gap Equation at (8+N)-point X with given deltaM"""

        T = np.asanyarray(T, dtype=float)

        bosons = self.boson_massSq(X, T, scale=scale, full_input=X, deltaM=deltaM)
        fermions = self.fermion_massSq(X, full_input=X)

        V1c = self.V1T(bosons, fermions, T)
        if self.use_CW_in_delta_m:
            V1c += self.V1(bosons, fermions)
        return V1c

    def V1pp_Gap(self, X, T, deltaM, scale=np.sqrt(v2)):
        """ Returns second derivative of V1_Gap(), for thermal masses. """
        params = {'T': T, 'deltaM':deltaM, 'scale': scale}
        V1pp = np.zeros_like(X)
        rel_dx = np.maximum(np.maximum(np.max(X, axis=-1) * 2.5e-4 * np.sqrt(T),
                                       2.5e-4 * np.sqrt(T)), 2.5e-4)
        #rel_dx = np.maximum(np.max(X, axis=-1)*1e-2, 1e-2)
        for i in range(9): # 8+self.N
            # was 1e-1
            #rel_dx = np.maximum(1e-1 * scale, 1e-1)
            V1pp[..., i] = part_der(self.V1_Gap, var=i, x0=X, n=2, order=9, dx=rel_dx, **params)
        V1pp[..., 8:] = np.ones((1, self.N)) * V1pp[..., 8, np.newaxis]
        return V1pp

    def BFB(self, scale=np.sqrt(v2)):
        """ checks if tree-level potential is bounded from below
            at a given scale.
            Conditions should be checked for all n in [1,N],
            but strongest bounds come from these boundaries.
        """
        lbda, b4 = self.cp_lbda(scale), self.cp_b4(scale)
        c4, c5 = self.cp_c4(scale), self.cp_c5(scale)
        at, a2 = self.cp_at(scale), self.cp_a2(scale)
        ct, dt = self.cp_ct(scale), self.cp_dt(scale)
        if at <= 0:
            aterm = (a2 + at/2.)
        else:
            aterm = a2
        if lbda <= 0:
            return False
        elif b4 <= 0:
            return False
        elif (c4 + c5) <= 0:
            return False
        elif (c4 + self.N*c5) <= 0:
            return False
        elif aterm < -2.*np.sqrt(b4*lbda):
            return False
        elif ct < -2.*np.sqrt(b4*(c4+c5)):
            return False
        elif ct < -2.*np.sqrt(b4*((c4/self.N)+c5)):
            return False
        elif dt < -0.5*np.sqrt(lbda*(c4+c5)):
            return False
        elif dt < -0.5*np.sqrt(lbda*((c4/self.N)+c5)):
            return False
        last_cond = 2.*np.sqrt(lbda*b4*(c4+c5))
        last_cond += aterm * np.sqrt(c4+c5)
        last_cond += ct*np.sqrt(lbda)
        last_cond += 4.*dt*np.sqrt(b4)
        sqrt_term = aterm + np.sqrt(4.*lbda*b4)
        sqrt_term *= (ct + np.sqrt(4.*b4*(c4+c5)))
        sqrt_term *= ((4.*dt) + np.sqrt(4.*lbda*(c4+c5)))
        if (last_cond + np.sqrt(sqrt_term)) < 0:
            return False
        last_cond = 2.*np.sqrt(lbda*b4*((c4/self.N)+c5))
        last_cond += aterm * np.sqrt((c4/self.N)+c5)
        last_cond += ct*np.sqrt(lbda)
        last_cond += 4.*dt*np.sqrt(b4)
        sqrt_term = aterm + np.sqrt(4.*lbda*b4)
        sqrt_term *= ct + np.sqrt(4.*b4*((c4/self.N)+c5))
        sqrt_term *= (4.*dt) + np.sqrt(4.*lbda*((c4/self.N)+c5))
        if (last_cond + np.sqrt(sqrt_term)) < 0:
            return False
        return True

    def boson_massSq(self, X, T, scale=np.sqrt(v2), full_input=None, deltaM=None):
        """ Method of CosmoTransition. Eigenvalues of bosonic mass matrix, dof, and c
            needed for CW and V1T
            in addition, we add
            - the renormalization scale,
            - the option to have full field dependence,
            - the option for a deltaM (thermal) that is not just a function of T
        """

        X = np.array(X)
        T = np.asanyarray(T, dtype=float)
        T2=T*T

        if full_input is None:
            assert X.shape[-1] == 2 + self.N
            h = X[..., 0]
            phi = X[..., 1]
            chi = X[..., 2:]
            # assuming all chi are the same (effectively overwriting the last N-1 chis):
            #chi_single = X[..., 2]
            #chi = np.array(self.N*[chi_single])
            #chi = np.moveaxis(chi, 0, -1)

            h2 = h**2
            phi2 = phi**2

            field_full = np.multiply.outer(np.zeros(X[..., 0].shape), np.ones((self.N+8)))
            field_full[..., 0] = h
            field_full[..., 4] = phi
            for i in range(self.N):
                field_full[..., 8+i] = chi[..., i]

            MassMatrix = self.Full_Matrix(field_full, scale=scale)

        else:
            assert full_input.shape[-1] == 8 + self.N
            h = full_input[..., :4]
            phi = full_input[..., 4:8]
            h2 = np.sum(h**2, axis=-1)
            phi2 = np.sum(phi**2, axis=-1)

            chi = full_input[..., 8:]
            MassMatrix = self.Full_Matrix(full_input, scale=scale)

        #chi2 = np.sum(chi**2, axis=-1)

        if deltaM is not None:
            assert deltaM.shape[-1] == 8 + self.N
            if len(deltaM.shape) == 1:
                deltaM = deltaM[np.newaxis, ...]
            for i in range(self.N+8):
                MassMatrix[i, i] += deltaM[..., i]

            gauge_thermal = np.ones(deltaM.shape[0])
            for index, entry in enumerate(deltaM):
                if (entry == 0).all():
                    gauge_thermal[index] = 0.

        else:
            gauge_thermal = 0.
        #deltaM = np.asanyarray(deltaM, dtype=float)
        #print("deltaM", deltaM)
        #if deltaM.all() == 0:
        #    gauge_thermal = 0.
        #else:
        #    raise
        #    gauge_thermal = 1.
        #    for i in range(self.N+8):
        #        MassMatrix[i, i] += deltaM[..., i]

        MassMatrix = np.rollaxis(MassMatrix, 0, len(MassMatrix.shape))
        MassMatrix = np.rollaxis(MassMatrix, 0, len(MassMatrix.shape))

        # thermal masses from formula of 1802.10500 / specialized as in 1504.05949
        # We diagonalize the Mass Matrix and compute the field dependent masses of W and Z
        #(GB are part of Mass Matrix and therefore not needed again):

        M = LA.eigvalsh(MassMatrix)
        #M=LA.eig(MassMatrix)[0]

        # transverse masses of W/Z
        mW = 0.25*(self.cp_g(scale)**2)*(h2+phi2)
        mZ = 0.25*(self.cp_g(scale)**2 + self.cp_gp(scale)**2)*(h2+phi2)

        # longitudinal masses of W/Z/A
        mWL = (mW + 2.*(self.cp_g(scale)**2)*gauge_thermal*T2)
        mAZsqrt = np.sqrt(((0.5*mZ)+((self.cp_g(scale)**2 + self.cp_gp(scale)**2)*gauge_thermal*T2))**2 -\
                         ((self.cp_g(scale)**2)*(self.cp_gp(scale)**2)*gauge_thermal*T2*\
                          ((h2+phi2+(4.*gauge_thermal*T2)))))
        mZL = (0.5*mZ)+((self.cp_g(scale)**2 + self.cp_gp(scale)**2)*gauge_thermal*T2) + mAZsqrt
        mAL = (0.5*mZ)+((self.cp_g(scale)**2 + self.cp_gp(scale)**2)*gauge_thermal*T2) - mAZsqrt

        # so far works fine for vectorized field input
        # not for single tuple
        if mW.ndim == 0 and mWL.ndim != 0:
            mWL = mWL[0]
            mZL = mZL[0]
            mAL = mAL[0]

        # To add the masses of W and Z to the eigenvalues of the Matrix:
        # The eigenvalues of M are returned in the shape of the input array X (= h),
        # with each entry being an array of the eigenvalues
        # We then rotate M such that its entries are the eigenvalues in the shape of X
        # then we add mW and mZ (that are also in the shape of X)
        # then we rotate back such that M has the shape of X
        # with each entry an array of (now 3 more) eigenvalues
        M = np.rollaxis(M, len(M.shape)-1)
        M = np.append(M,np.array([mW, mWL, mZ, mZL, mAL]), axis=0)
        M = np.rollaxis(M, 0, len(M.shape))

        # The number of degrees of freedom for the masses.
        # Since np.linalg.eigvalsh orders the eigenvalues,
        # the three eigenvalues of the GB are separate and
        # dof has a 1 in all places
        # transverse gauge fields have 2, longitudinal 1 dof

        dof = np.concatenate([np.array((8+self.N)*[1]), np.array([4, 2, 2, 1, 1])])

        # c is a constant for each particle used in the Coleman-Weinberg
        # potential using MS-bar renormalization. See Quiros or 1605.08663
        # c_i = 1/2 for tranverse, 3/2 for longitudinal gauge fields
        c = np.concatenate([np.array((8+self.N)*[1.5]), np.array([0.5, 1.5, 0.5, 1.5, 1.5])])

        return M.real+1e-16, dof, c

    def fermion_massSq(self, X, scale=np.sqrt(v2), full_input=None):
        """ Method of CosmoTransition. Eigenvalues of fermionic mass matrix and dof
            needed for CW and V1T
            in addition, we add
            - the renormalization scale,
            - the option to have full field dependence,
        """
        X = np.array(X)
        if full_input is None:
            assert X.shape[-1] == 2 + self.N
            h = X[..., 0]
            h2 = h**2
        else:
            assert full_input.shape[-1] == 8 + self.N
            h = full_input[..., 0:4]
            h2 = np.sum(h**2, axis=-1)
        mt = 0.5*(self.cp_yt(scale))**2*h2
        Mf = np.array([mt])

        Mf = np.rollaxis(Mf, 0, len(Mf.shape))

        doff = np.array([12.])

        return Mf, doff

    def boson_name(self, index):
        """ returns string with name of boson, based on the mass matrix
            note that LA.eig() might have changed the order of of the scalar
            entries for large mixing
        """
        if index == 0:
            name = r'Higgs, $h$'
        elif index in (1, 2, 3):
            name = r'Higgs GB'
        elif index == 4:
            name = r'Inert Higgs, $\phi$'
        elif index in (5, 6, 7):
            name = r'Inert GB'
        elif index > 7 and index <= 7+self.N:
            name = r'Singlet $\chi$'
        elif index == 8+self.N:
            name = r'transverse $W$'
        elif index == 9+self.N:
            name = r'longitudinal $W$'
        elif index == 10+self.N:
            name = r'transverse $Z$'
        elif index == 11+self.N:
            name = r'longitudinal $Z$'
        elif index == 12+self.N:
            name = r'longitudinal $A$'
        else:
            raise ValueError('Index {} is not a valid index'.format(index))
        return name

    def approxZeroTMin(self, scale=np.sqrt(v2)):
        """ Method of CosmoTransitions. Returns list of possible minima at T=0"""
        # try all 8 solutions as approximation for V_CW
        c4c5_comb = self.cp_c4(scale) + self.N*self.cp_c5(scale)
        a2at_comb = 2.*self.cp_a2(scale) + self.cp_at(scale)

        # initialize, incl. one with all 0
        sol = np.zeros((8, self.N+2))
        # Higgs vacuum is known
        sol[1, 0] = np.sqrt(v2)
        # Inert direction only
        sol[2, 1] = np.sqrt(-self.cp_b2(scale)/self.cp_b4(scale) +0j).real
        # Chi direction only, all same is deepest
        sol[3, 2:] = np.sqrt(-self.cp_c2(scale)/c4c5_comb +0j).real * np.ones(self.N)
        # Higgs & Inert direction
        den = a2at_comb**2 - 16.*self.cp_b4(scale)*self.cp_lbda(scale)
        sol[4, 0] = np.sqrt((-4.*self.cp_b2(scale)*a2at_comb\
                             -16.*self.cp_b4(scale)*self.cp_mu2(scale))/den + 0j).real
        sol[4, 1] = np.sqrt((4.*self.cp_mu2(scale)*a2at_comb\
                             +16.*self.cp_b2(scale)*self.cp_lbda(scale))/den + 0j).real
        # Inert & Chi direction
        den = -4.*self.cp_b4(scale)*c4c5_comb + self.N*self.cp_ct(scale)**2
        sol[5, 1] = np.sqrt((4.*self.cp_b2(scale)*c4c5_comb\
                             -2.*self.cp_c2(scale)*self.cp_ct(scale)*self.N)/den + 0j).real
        sol[5, 2:] = np.sqrt((4.*self.cp_b4(scale)*self.cp_c2(scale)\
                             -2.*self.cp_ct(scale)*self.cp_b2(scale))/den + 0j).real\
                             * np.ones(self.N)
        # Higgs & Chi direction
        den = self.cp_lbda(scale)*c4c5_comb - 4.*self.cp_dt(scale)**2*self.N
        sol[6, 0] = np.sqrt((2.*self.cp_c2(scale)*self.cp_dt(scale)*self.N\
                             +self.cp_mu2(scale)*c4c5_comb)/den + 0j).real
        sol[6, 2:] = np.sqrt((self.cp_lbda(scale)*self.cp_c2(scale) + 2.*self.cp_mu2(scale)*\
                              self.cp_dt(scale))/den + 0j).real * np.ones(self.N)
        # All directions
        den = a2at_comb**2 * c4c5_comb - 8.*a2at_comb*self.cp_ct(scale)*self.cp_dt(scale)*self.N\
              -16.*self.cp_b4(scale)*self.cp_lbda(scale)*c4c5_comb\
              + 64.*self.N*self.cp_b4(scale)*self.cp_dt(scale)**2\
              + 4. * self.N * self.cp_ct(scale)**2 * self.cp_lbda(scale)
        num_h = -4.*self.cp_b2(scale)*c4c5_comb*a2at_comb + self.cp_ct(scale)*self.N*\
                (2.*a2at_comb*self.cp_c2(scale)+16.*self.cp_b2(scale)*self.cp_dt(scale)\
                  +4.*self.cp_ct(scale)*self.cp_mu2(scale))\
                -16.*self.cp_b4(scale)*(self.cp_mu2(scale)*c4c5_comb +\
                                        2.*self.cp_c2(scale)*self.N*self.cp_dt(scale))
        num_inert = 4.*self.cp_mu2(scale)*c4c5_comb*a2at_comb + self.cp_dt(scale)*self.N*\
                    (8.*a2at_comb*self.cp_c2(scale)-16.*self.cp_ct(scale)*self.cp_mu2(scale)\
                     -64.*self.cp_b2(scale)*self.cp_dt(scale))\
                    +8.*self.cp_lbda(scale)*(2.*self.cp_b2(scale)*c4c5_comb -\
                                        self.cp_c2(scale)*self.N*self.cp_ct(scale))
        num_chi = a2at_comb*(8.*self.cp_b2(scale)*self.cp_dt(scale) - 2.*self.cp_ct(scale)\
                             *self.cp_mu2(scale)-self.cp_c2(scale)*a2at_comb)\
                -8.*self.cp_b2(scale)*self.cp_ct(scale)*self.cp_lbda(scale)\
                +16.*self.cp_b4(scale)*(self.cp_c2(scale)*self.cp_lbda(scale)+\
                                        2.*self.cp_dt(scale)*self.cp_mu2(scale))
        sol[7, 0] = np.sqrt(num_h/den + 0j).real
        sol[7, 1] = np.sqrt(num_inert/den + 0j).real
        sol[7, 2:] = np.sqrt(num_chi/den + 0j).real * np.ones(self.N)
        return np.unique(sol, axis=0)

    def Vtot(self, X, T, include_radiation=True, run_scale=True):
        """ Method of CosmoTransitions.
        The total finite temperature effective potential.

        Parameters
        ----------
        X : array_like
            Field value(s).
            Either a single point (with length `Ndim`), or an array of points.
        T : float or array_like
            The temperature. The shapes of `X` and `T`
            should be such that ``X.shape[:-1]`` and ``T.shape`` are
            broadcastable (that is, ``X[...,0]*T`` is a valid operation).
        include_radiation : bool, optional
            If False, this will drop all field-independent radiation
            terms from the effective potential. Useful for calculating
            differences or derivatives.
        run_scale : bool, optional
            If true, the renormalization scale will be determined
            by the current field configuration. Otherwise 246 GeV is used

        TODO: include deltaM (thermal)
        """
        T = np.asanyarray(T, dtype=float)
        X = np.asanyarray(X, dtype=float)
        assert X.shape[-1] == 2 + self.N

        if run_scale:
            ren_scale = self.current_scale(X, T)
        else:
            ren_scale = np.sqrt(v2)

        #deltaM = self.Improved_Daisy(helper.convert_to_full(X), T, scale=ren_scale, improved=True)
        if T == 0:
            deltaM = np.zeros((8+self.N,))
        else:
            deltaM = self.delta_m_daisy(helper.convert_to_full(X), T, scale=ren_scale)
            #deltaM = self.daisy_with_dict(self.Improved_Daisy, self.store_delta_m_3,
            #                              helper.convert_to_full(X), T, scale=ren_scale,
            #                              improved=True)
        bosons = self.boson_massSq(X, T, scale=ren_scale, deltaM=deltaM)
        fermions = self.fermion_massSq(X, scale=ren_scale)
        Vtot = self.V0(X, scale=ren_scale)

        # Coleman-Weinberg contribution:
        Vtot += self.V1(bosons, fermions, scale=ren_scale) * self.use_CW

        # Temperature dependent contribution
        Vtot += self.V1T(bosons, fermions, T, include_radiation)

        return Vtot

    def V1T(self, bosons, fermions, T, include_radiation=True):
        """ Method of CosmoTransitions.
        The one-loop finite-temperature potential.

        This is generally not called directly, but is instead used by
        :func:`Vtot`.

        Note
        ----
        The `Jf` and `Jb` functions used here are
        aliases for :func:`finiteT.Jf_spline` and :func:`finiteT.Jb_spline`,
        each of which accept mass over temperature *squared* as inputs
        (this allows for negative mass-squared values, which I take to be the
        real part of the defining integrals.

        .. todo::
            Implement new versions of Jf and Jb that return zero when m=0, only
            adding in the field-independent piece later if
            ``include_radiation == True``. This should reduce floating point
            errors when taking derivatives at very high temperature, where
            the field-independent contribution is much larger than the
            field-dependent contribution.
        """
        # This does not need to be overridden.
        T2 = (T*T)[..., np.newaxis] + 1e-100
        # the 1e-100 is to avoid divide by zero errors
        T4 = T*T*T*T
        m2, nb, _ = bosons
        #print("bos: {}".format(m2/T2))
        y = np.sum(nb*Jb(m2/T2), axis=-1)
        m2, nf = fermions
        #print("ferm: {}".format(m2/T2))
        y += np.sum(nf*Jf(m2/T2), axis=-1)
        if include_radiation:
            if self.num_boson_dof is not None:
                nb = self.num_boson_dof - np.sum(nb)
                y -= nb * np.pi**4 / 45.
            if self.num_fermion_dof is not None:
                nf = self.num_fermion_dof - np.sum(nf)
                y -= nf * 7*np.pi**4 / 360.
        return y*T4/(2*np.pi*np.pi)

    def V1(self, bosons, fermions, scale=np.sqrt(v2)):
        """ Method of CosmoTransitions.
        The one-loop corrections to the zero-temperature potential
        using MS-bar renormalization.

        This is generally not called directly, but is instead used by
        :func:`Vtot`.
        """
        # This does not need to be overridden.
        # changed np.log(np.abs(x)+1e-100) to np.log(x+1e-100+0j).real

        scale2 = scale**2
        scale2 = scale2[..., np.newaxis]
        m2, n, c = bosons
        y = np.sum(n*m2*m2 * (np.log(m2/scale2 + 1e-100 + 0j)
                              - c), axis=-1)
        m2, n = fermions
        c = 1.5
        y -= np.sum(n*m2*m2 * (np.log(m2/scale2 + 1e-100 + 0j)
                               - c), axis=-1)
        return y.real/(64*np.pi*np.pi)

    def HighT_Daisy(self, X, T, scale=np.sqrt(v2)):
        """Computes high-T Daisy contribution in TFD"""
        T2 = T**2
        higgs = 0.5*self.cp_lbda(scale) + (1./6.)*self.cp_a2(scale) + 0.25*self.cp_yt(scale)**2+\
                (3.*self.cp_g(scale)**2 + self.cp_gp(scale)**2)/16. + (1./24.)*self.cp_at(scale)+\
                self.N/6. * self.cp_dt(scale)
        higgs *= T2
        inert = 0.5*self.cp_b4(scale) + (1./6.)*self.cp_a2(scale) +\
                (3.*self.cp_g(scale)**2 + self.cp_gp(scale)**2)/16. +\
                (self.N/24.)*self.cp_ct(scale) + (1./24.) * self.cp_at(scale)
        inert *= T2
        chi = 0.25*self.cp_c4(scale) + (1./6.) * self.cp_ct(scale) + (2./3.)*self.cp_dt(scale)+\
                +(self.N+2)*self.cp_c5(scale)/12.
        chi *= T2
        ret = np.concatenate([4*[higgs], 4*[inert], self.N*[chi]])
        ret = np.rollaxis(ret, 0, len(ret.shape)) * np.ones_like(X)
        return ret

    def Improved_Daisy(self, X, T, scale=np.sqrt(v2), improved=False, rel_dx_factor=1.):
        """ Computes Daisy terms as second derivative of V1T using splines for J.
            includes the CW potential into the 2nd derivative.
            Supports a 2-step approximation in which deltaM = Improved_Daisy(deltaM=0.)
            """
        T = np.asanyarray(T, dtype=float)
        X = np.asanyarray(X, dtype=float)

        if improved:
            deltaM = self.Improved_Daisy(X, T, scale=scale, improved=False)
        else:
            deltaM = np.zeros_like(X)

        def V1_Daisy(X):
            bosons = self.boson_massSq(X, T, scale=scale, full_input=X, deltaM=deltaM)
            fermions = self.fermion_massSq(X, scale=scale, full_input=X)

            scale_loc = np.asanyarray(scale)[..., np.newaxis]

            T2 = (T*T)[..., np.newaxis] + 1e-100
            T4 = T*T*T*T
            m2b, nb, c = bosons
            m2f, nf = fermions

            #print("field: {}".format(X))
            #print("bos: {}".format(m2b/T2))
            #print("ferm: {}".format(m2f/T2))
            #print(np.min(m2b/T2))
            #print("Jb: {}".format(nb*Jb(m2b/T2)))
            #print("Jf: {}".format(nf*Jf(m2f/T2)))
            #print("factor: {}".format(T4/(2*np.pi*np.pi)))

            y = np.sum(nb*Jb(m2b/T2), axis=-1)
            y += np.sum(nf*Jf(m2f/T2), axis=-1)
            #y = y*T4/(2*np.pi*np.pi)
            if self.use_CW_in_delta_m:
                yCW = np.sum(nb*m2b*m2b * (np.log(m2b/(scale_loc**2) + 1e-100 + 0j)
                                           - c), axis=-1)
                yCW -= np.sum(nf*m2f*m2f * (np.log(m2f/(scale_loc**2) + 1e-100 + 0j)
                                            - 1.5), axis=-1)
                yCW = yCW.real/(64*np.pi*np.pi)
                yCW = yCW*(2*np.pi*np.pi)/T4

                y += yCW
            return y

        ret = np.zeros_like(X)
        rel_dx = rel_dx_factor * np.maximum(
            np.maximum(np.max(X, axis=-1)*2.5e-4*np.sqrt(T),
                       2.5e-4*np.sqrt(T)), 2.5e-4)
        for i in range(9): #8+self.N
            ret[..., i] = part_der(V1_Daisy, var=i, x0=X, n=2, order=9, dx=rel_dx)
            #ret[..., i] = part_der(V1_Daisy, var=i, x0=X, n=2, order=5, dx=rel_dx) #1e-2
        ret[..., 8:] = np.ones((1, self.N)) * ret[..., 8, np.newaxis]
        return ret*T**4/(2*np.pi*np.pi)

    def Gap_Daisy(self, X, T, scale=np.sqrt(v2)):
        """ Computes Daisy terms as second derivative of V1T using splines for J.
            includes the CW potential into the 2nd derivative.
            Solves the gap equation deltaM = second_derivative V(deltaM)
        """
        short = True
        X = np.asanyarray(X)
        if np.asanyarray(scale).shape == ():
            scale *= np.ones(len(X))
        assert X.shape[:-1] == scale.shape
        solution = []
        len_X = len(X)
        for index, point in enumerate(X, 1):
            SOL_FAILED = False
            # make this external and T, point args
            def Gap_Eq_short(deltaM):
                delta_large = np.concatenate([deltaM, (self.N-1)*[deltaM[-1]]])
                return deltaM - self.V1pp_Gap(point, T, delta_large, scale=scale[index-1])[:9]
            def Gap_Eq_long(deltaM):
                return deltaM - self.V1pp_Gap(point, T, deltaM, scale=scale[index-1])

            #current_guess = self.Improved_Daisy(point, T, scale=scale[index-1], improved=True)
            current_guess = self.daisy_with_dict(self.Improved_Daisy, self.store_delta_m_2,
                                                 point, T, scale=scale[index-1], improved=False)

            if short:
                # solving the 9-dim system and adding N-1 chis at the end
                which = 'short'
                Gap_Eq = Gap_Eq_short
                current_guess = current_guess[:9]
            else:
                # solving full 8+N dim system
                which = 'long'
                Gap_Eq = Gap_Eq_long

            current_sol = optimize.root(Gap_Eq, current_guess, method='anderson',
                                        options={'maxiter':100, 'fatol':1e0,
                                                 'disp': True})
            if not current_sol.success:
                output_str = "'anderson' did not converge to target, current {} solution has norm {:.3e}"
                anderson_norm = np.linalg.norm(current_sol.fun)
                print(output_str.format(which, anderson_norm))
                print("Trying 'lm' now")
                current_lm = optimize.root(Gap_Eq, current_guess, method='lm',
                                           options={'xtol':1e-4, 'maxiter':1000000,
                                                    'ftol':1e-4})
                lm_norm = np.linalg.norm(current_lm.fun)
                if lm_norm < anderson_norm:
                    if current_lm.success:
                        print("'lm' norm is smaller ({:.3e}), therefore we use it".format(lm_norm))
                        current_sol = current_lm
                    else:
                        print("'lm' also failed, but norm is smaller ({:.3e}), we use this now".format(lm_norm))
                else:
                    print("'lm' norm is not smaller ({:.3e}), keeping 'anderson'".format(lm_norm))


            if short:
                sol = np.concatenate([current_sol.x, (self.N-1)*[current_sol.x[-1]]])
            else:
                sol = current_sol.x

            actual_sol = Gap_Eq_long(sol)
            norm = np.linalg.norm(actual_sol)
            rel_norm = np.linalg.norm(actual_sol/(sol+1e-16))
            max_entry = np.abs((actual_sol/(sol+1e-16))).max()

            output_str = "{} solver success {}/{}, abs norm of solution is {:1.3e},"+\
                " rel norm is {:1.3e}, max entry of rel is {:1.3e}"
            print(output_str.format(which, index, len_X, norm, rel_norm, max_entry))

            solution.append(sol)

        solution = np.array(solution)

        return solution

    def daisy_with_dict(self, daisy_func, storage, X, T, scale=np.sqrt(v2), **kwargs):
        """ checks if points already exist in storage, computes and adds them if neccessary """
        field_shape = X.shape
        field_flat = X.reshape(-1, field_shape[-1])
        compute_list = []
        compute_index = []
        existing_list = []
        existing_index = []
        scale_list = []

        if np.asanyarray(scale).shape == ():
            if X.ndim != 1:
                scale *= np.ones(len(X))
            else:
                scale = np.array([scale])
                X = X[np.newaxis, ...]
        assert X.shape[:-1] == scale.shape

        # check if already stored:
        for field_index, field in enumerate(field_flat):
            field_tuple = helper.make_tuple(field, T)
            if field_tuple in storage:
                print("Recycled a deltaM point: {}".format(field_tuple))
                existing_list.append(storage[field_tuple])
                existing_index.append(field_index)
            else:
                compute_index.append(field_index)
                compute_list.append(field)
                scale_list.append(scale[field_index])
        compute_list = np.array(compute_list)
        compute_index = np.array(compute_index).astype(int)
        existing_list = np.array(existing_list)
        existing_index = np.array(existing_index).astype(int)
        scale_list = np.array(scale_list)

        # compute new points
        #if compute_list != np.array([]):
        if compute_list.size > 0:
            print("computing deltaM(T) for {} point(s)".format(len(compute_list)))
            computed_delta = daisy_func(compute_list, T, scale=scale_list, **kwargs)
            print("done computing deltaM(T)")

            # add computed points to storage
            for index, delta in enumerate(computed_delta):
                field_tuple = helper.make_tuple(compute_list[index], T)
                storage[field_tuple] = delta
        else:
            computed_delta = np.array([])

        # combine them back together and reshape
        ret = np.zeros_like(field_flat)
        # there must be a faster way ...
        if len(field_shape) == 1:
            #if computed_delta != np.array([]):
            if computed_delta.size > 0:
                ret = computed_delta
            else:
                ret = existing_list
        for index, value in enumerate(computed_delta):
            ret[compute_index[index]] = value
        for index, value in enumerate(existing_list):
            ret[existing_index[index]] = value
        ret = ret.reshape(field_shape)
        return ret

    def delta_m_daisy(self, X, T, scale=np.sqrt(v2)):
        """ selects deltaM based on use_Daisy switch. """
        # (0. OFF; 1. hard thermal; 2. improved 1-step; 3 improved 2-step 4 gap eq.)
        if self.use_Daisy == 0:
            ret = np.zeros_like(X)
        elif self.use_Daisy == 1:
            ret = self.HighT_Daisy(X, T, scale=scale)
        elif self.use_Daisy == 2:
            ret = self.daisy_with_dict(self.Improved_Daisy, self.store_delta_m_2,
                                       X, T, scale=scale, improved=False)
        elif self.use_Daisy == 3:
            ret = self.daisy_with_dict(self.Improved_Daisy, self.store_delta_m_3,
                                       X, T, scale=scale, improved=True)
        elif self.use_Daisy == 4:
            ret = self.daisy_with_dict(self.Gap_Daisy, self.store_delta_m_4, X, T, scale=scale)
        else:
            raise ValueError("Do not recognize input for use_Daisy: {}\n".format(self.use_Daisy)+\
            "0 = Off, 1 = high-T, 2 = improved 1-step, 3 = improved 2-step, 4 = gap eq.")
        return ret


def SolveCW_Vprime(h, const, scale2):
    """ Helper function for SolveCWBoundary"""
    ret = (h+const) * (np.log((h+const)/scale2 +1e-100+0j)-1.)
    return ret.real

def SolveCW_V2prime(h, const, scale2):
    """ Helper function for SolveCWBoundary"""
    ret = 0.5* ((3.*h+const) * np.log((h+const)/scale2 +1e-100+0j) - (h+const))
    return ret.real
        
def SolveCWBoundary(inval, gs, g, gp, b2, b4, a2, c2, c4, ct, dt, at, c5, yt, N):
    """ Function used to solve for electroweak boundary conditions:
        first derivative vanishes at v and second derivative gives mh**2

        TODO: be more flexible in the scale (use current_scale()?)
    """

    del gs, b4, c4, ct, c5
    lbda, mu2 = inval
    mu2 *= 1e3
    g2 = g**2
    gp2 = gp**2
    ggp2 = g2 + gp2
    scale2 = 246.**2
    h_pos = 246.
    den = 64.*np.pi*np.pi
    gauge_combo = (3. * g**4) + (2. * g**2 * gp**2) + gp**4
    VprimeGauge = 0.25*h_pos**3 * (-gauge_combo + (3. * ggp2**2 * np.log(h_pos**2 * ggp2 / (4. * scale2))) +\
                                   (6. * g2**2 * np.log(g2*h_pos**2 / (4.*scale2))))
    V2primeGauge = 0.75*h_pos**2 * (gauge_combo + (3. * ggp2**2 * np.log(h_pos**2 * ggp2 / (4. * scale2))) +\
                                   (6. * g2**2 * np.log(g2*h_pos**2 / (4.*scale2))))
    VprimeFerm = -48. * yt**2 * SolveCW_Vprime(0.5*h_pos**2 * yt**2, 0., scale2)
    V2primeFerm = -12. * yt**4 * (h_pos**2) * (3.*np.log(h_pos**2 * yt**2/(2.*scale2))-1.)

    h_array = np.array([h_pos**2 * (0.5* a2 + 0.25*at), 2.*dt*h_pos**2, 0.5*a2*h_pos**2,\
                        lbda * h_pos**2, 3. * lbda * h_pos**2])
    const_array = np.array([b2, c2, b2, - mu2, - mu2])
    pre_array = np.array([4. * (2.*a2 + at), 16.*dt*N, 8.*a2, 24.*lbda, 24.*lbda])
    VprimeScalar = np.sum(pre_array * SolveCW_Vprime(h_array, const_array, scale2), axis=-1)
    V2primeScalar = np.sum(pre_array * SolveCW_V2prime(h_array, const_array, scale2), axis=-1)

    #Vprime = np.round((VprimeGauge + (VprimeFerm + VprimeScalar) * 0.5 * h_pos)/den + (h_pos**3 * lbda - h_pos*mu2), 5)
    #V2prime = np.round((V2primeGauge + V2primeFerm + V2primeScalar)/den + (3.*h_pos**2 * lbda - mu2), 5)
    Vprime = (VprimeGauge + (VprimeFerm + VprimeScalar) * 0.5 * h_pos)/den + (h_pos**3 * lbda - h_pos*mu2)
    V2prime = (V2primeGauge + V2primeFerm + V2primeScalar)/den + (3.*h_pos**2 * lbda - mu2)

    return [Vprime.real, V2prime.real-mh**2]
