""" Reads in the model benchmarks we scanned and plots all correlations """

import numpy as np
import matplotlib.pyplot as plt

import helper.helper as helper
from model.Inert2HDM_N import model_2HDM_N

num_chi = 100
model_parameters = {'b2': 100**2, 'b4': 0.126, 'c2': 10**2, 'c4': 1.,
                    'a2': -0.02, 'ct': -0.06, 'dt': 0., 'at': 0., 'c5': 0.,
                    'N': num_chi, 'use_Daisy': 3., 'use_CW_in_delta_m':False}

model_RGE0_CW0 = model_2HDM_N(**model_parameters, use_CW=0., use_RGE=False)
helper.load_minima_dic(model_RGE0_CW0)
model_RGE0_CW1 = model_2HDM_N(**model_parameters, use_CW=1., use_RGE=False)
helper.load_minima_dic(model_RGE0_CW1)
model_RGE1_CW0 = model_2HDM_N(**model_parameters, use_CW=0., use_RGE=True)
helper.load_minima_dic(model_RGE1_CW0)
model_RGE1_CW1 = model_2HDM_N(**model_parameters, use_CW=1., use_RGE=True)
helper.load_minima_dic(model_RGE1_CW1)

def create_T_array(model, which_daisy):
    """ transforms dictionaries to arrays """
    T_array = []
    minima_array = []
    minima_dict = model.__dict__[f'minima_dict_wd{which_daisy}']
    for entry in minima_dict:
        T_array.append(entry)
        minima_array.append(minima_dict[entry])
    T_array = np.array(T_array)
    minima_array = np.array(minima_array)

    sorted_indices = np.argsort(T_array)
    return T_array[sorted_indices], np.abs(minima_array)[sorted_indices]

def plot_prep():
    """ prepares plot """
    fig = plt.figure(dpi=150, figsize=[6., 6.])
    axis = fig.add_subplot(111)
    plt.xlim([0., 1e3])
    plt.ylim([-5., 800.])
    plt.xscale('symlog', linthreshx=500.)
    #ticks = np.concatenate([np.arange(0., 1000., 100), np.arange(1000., 6000., 1000.)])
    ticks = np.arange(0., 1000., 100)
    labels = list(ticks.copy())
    #labels[-2] = ''
    #labels[-3] = ''
    labels[6] = ''
    labels[7] = ''
    labels[8] = ''
    labels[9] = ''
    plt.xticks(ticks=ticks, labels=labels)

    plt.xlabel(r"$T$ [GeV]")
    plt.ylabel(r"Field value at Minimum")

def plot_field(T_array, minima_array, style, label, color):
    """ plots the three fields"""
    plt.plot(T_array, minima_array[..., 2], ls=style, label='Chi '+label, lw=1.5, color=color[0])
    plt.plot(T_array, minima_array[..., 0], ls=style, label='Higgs '+label, lw=1.5, color=color[1])
    plt.plot(T_array, minima_array[..., 1], ls=style, label='Inert '+label, lw=1.5, color=color[2])
    plt.legend(loc='upper left')

#for model in [model_RGE0_CW0, model_RGE0_CW1, model_RGE1_CW0, model_RGE1_CW1]:
#    for daisy in range(4):
#        print(create_T_array(model, daisy))

T_RGE0_CW0_D0, vev_RGE0_CW0_D0 = create_T_array(model_RGE0_CW0, 0)
T_RGE0_CW0_D1, vev_RGE0_CW0_D1 = create_T_array(model_RGE0_CW0, 1)
T_RGE0_CW0_D2, vev_RGE0_CW0_D2 = create_T_array(model_RGE0_CW0, 2)
T_RGE0_CW0_D3, vev_RGE0_CW0_D3 = create_T_array(model_RGE0_CW0, 3)
T_RGE0_CW0_D4, vev_RGE0_CW0_D4 = create_T_array(model_RGE0_CW0, 4)

T_RGE0_CW1_D0, vev_RGE0_CW1_D0 = create_T_array(model_RGE0_CW1, 0)
T_RGE0_CW1_D1, vev_RGE0_CW1_D1 = create_T_array(model_RGE0_CW1, 1)
T_RGE0_CW1_D2, vev_RGE0_CW1_D2 = create_T_array(model_RGE0_CW1, 2)
T_RGE0_CW1_D3, vev_RGE0_CW1_D3 = create_T_array(model_RGE0_CW1, 3)

T_RGE1_CW0_D0, vev_RGE1_CW0_D0 = create_T_array(model_RGE1_CW0, 0)
T_RGE1_CW0_D1, vev_RGE1_CW0_D1 = create_T_array(model_RGE1_CW0, 1)
T_RGE1_CW0_D2, vev_RGE1_CW0_D2 = create_T_array(model_RGE1_CW0, 2)
T_RGE1_CW0_D3, vev_RGE1_CW0_D3 = create_T_array(model_RGE1_CW0, 3)

T_RGE1_CW1_D0, vev_RGE1_CW1_D0 = create_T_array(model_RGE1_CW1, 0)
T_RGE1_CW1_D1, vev_RGE1_CW1_D1 = create_T_array(model_RGE1_CW1, 1)
T_RGE1_CW1_D2, vev_RGE1_CW1_D2 = create_T_array(model_RGE1_CW1, 2)
T_RGE1_CW1_D3, vev_RGE1_CW1_D3 = create_T_array(model_RGE1_CW1, 3)


# 4 plots with fixed RGE, CW
# scanning Daisy
plot_prep()
plot_field(T_RGE0_CW0_D0, vev_RGE0_CW0_D0, 'solid', 'Daisy OFF', ['lightgreen', 'lightcoral', 'cornflowerblue'])
plot_field(T_RGE0_CW0_D1, vev_RGE0_CW0_D1, 'dashed', 'high T', ['green', 'red', 'blue'])
plot_field(T_RGE0_CW0_D2, vev_RGE0_CW0_D2, 'dashdot', '1-step', ['green', 'red', 'blue'])
plot_field(T_RGE0_CW0_D3, vev_RGE0_CW0_D3, 'dashdot', '2-step', ['darkgreen', 'darkred', 'darkblue'])
plot_field(T_RGE0_CW0_D4, vev_RGE0_CW0_D4, 'dotted', 'gap eq.', ['darkgreen', 'darkred', 'darkblue'])
plt.title("RGE OFF, CW OFF, Daisy scan")
plt.savefig('plots/bm_study/RGE0_CW0_DaisyScan.png')
plt.close()

plot_prep()
plot_field(T_RGE0_CW1_D0, vev_RGE0_CW1_D0, 'solid', 'Daisy OFF', ['lightgreen', 'lightcoral', 'cornflowerblue'])
plot_field(T_RGE0_CW1_D1, vev_RGE0_CW1_D1, 'dashed', 'high T', ['green', 'red', 'blue'])
plot_field(T_RGE0_CW1_D2, vev_RGE0_CW1_D2, 'dashdot', '1-step', ['green', 'red', 'blue'])
plot_field(T_RGE0_CW1_D3, vev_RGE0_CW1_D3, 'dotted', '2-step', ['darkgreen', 'darkred', 'darkblue'])
plt.title("RGE OFF, CW ON, Daisy scan")
plt.savefig('plots/bm_study/RGE0_CW1_DaisyScan.png')
plt.close()

plot_prep()
plot_field(T_RGE1_CW0_D0, vev_RGE1_CW0_D0, 'solid', 'Daisy OFF', ['lightgreen', 'lightcoral', 'cornflowerblue'])
plot_field(T_RGE1_CW0_D1, vev_RGE1_CW0_D1, 'dashed', 'high T', ['green', 'red', 'blue'])
plot_field(T_RGE1_CW0_D2, vev_RGE1_CW0_D2, 'dashdot', '1-step', ['green', 'red', 'blue'])
plot_field(T_RGE1_CW0_D3, vev_RGE1_CW0_D3, 'dotted', '2-step', ['darkgreen', 'darkred', 'darkblue'])
plt.title("RGE ON, CW OFF, Daisy scan")
plt.savefig('plots/bm_study/RGE1_CW0_DaisyScan.png')
plt.close()

plot_prep()
plot_field(T_RGE1_CW1_D0, vev_RGE1_CW1_D0, 'solid', 'Daisy OFF', ['lightgreen', 'lightcoral', 'cornflowerblue'])
plot_field(T_RGE1_CW1_D1, vev_RGE1_CW1_D1, 'dashed', 'high T', ['green', 'red', 'blue'])
plot_field(T_RGE1_CW1_D2, vev_RGE1_CW1_D2, 'dashdot', '1-step', ['green', 'red', 'blue'])
plot_field(T_RGE1_CW1_D3, vev_RGE1_CW1_D3, 'dotted', '2-step', ['darkgreen', 'darkred', 'darkblue'])
plt.title("RGE ON, CW ON, Daisy scan")
plt.savefig('plots/bm_study/RGE1_CW1_DaisyScan.png')
plt.close()


# plots with fixed RGE, Daisy
# scanning CW
plot_prep()
plot_field(T_RGE0_CW0_D0, vev_RGE0_CW0_D0, 'solid', 'CW OFF', ['lightgreen', 'lightcoral', 'cornflowerblue'])
plot_field(T_RGE0_CW1_D0, vev_RGE0_CW1_D0, 'dashed', 'CW ON', ['green', 'red', 'blue'])
plt.title("RGE OFF, Daisy OFF, CW scan")
plt.savefig('plots/bm_study/RGE0_Daisy0_CWScan.png')
plt.close()

plot_prep()
plot_field(T_RGE0_CW0_D1, vev_RGE0_CW0_D1, 'solid', 'CW OFF', ['lightgreen', 'lightcoral', 'cornflowerblue'])
plot_field(T_RGE0_CW1_D1, vev_RGE0_CW1_D1, 'dashed', 'CW ON', ['green', 'red', 'blue'])
plt.title("RGE OFF, Daisy high-T, CW scan")
plt.savefig('plots/bm_study/RGE0_Daisy1_CWScan.png')
plt.close()

plot_prep()
plot_field(T_RGE0_CW0_D2, vev_RGE0_CW0_D2, 'solid', 'CW OFF', ['lightgreen', 'lightcoral', 'cornflowerblue'])
plot_field(T_RGE0_CW1_D2, vev_RGE0_CW1_D2, 'dashed', 'CW ON', ['green', 'red', 'blue'])
plt.title("RGE OFF, Daisy 1-step, CW scan")
plt.savefig('plots/bm_study/RGE0_Daisy2_CWScan.png')
plt.close()

plot_prep()
plot_field(T_RGE0_CW0_D3, vev_RGE0_CW0_D3, 'solid', 'CW OFF', ['lightgreen', 'lightcoral', 'cornflowerblue'])
plot_field(T_RGE0_CW1_D3, vev_RGE0_CW1_D3, 'dashed', 'CW ON', ['green', 'red', 'blue'])
plt.title("RGE OFF, Daisy 2-step, CW scan")
plt.savefig('plots/bm_study/RGE0_Daisy3_CWScan.png')
plt.close()

plot_prep()
plot_field(T_RGE0_CW0_D4, vev_RGE0_CW0_D4, 'solid', 'CW OFF', ['lightgreen', 'lightcoral', 'cornflowerblue'])
#plot_field(T_RGE0_CW1_D4, vev_RGE0_CW1_D4, 'dashed', 'CW ON', ['green', 'red', 'blue'])
plt.title("RGE OFF, Daisy gap eq., CW scan")
plt.savefig('plots/bm_study/RGE0_Daisy4_CWScan.png')
plt.close()

plot_prep()
plot_field(T_RGE1_CW0_D0, vev_RGE1_CW0_D0, 'solid', 'CW OFF', ['lightgreen', 'lightcoral', 'cornflowerblue'])
plot_field(T_RGE1_CW1_D0, vev_RGE1_CW1_D0, 'dashed', 'CW ON', ['green', 'red', 'blue'])
plt.title("RGE ON, Daisy OFF, CW scan")
plt.savefig('plots/bm_study/RGE1_Daisy0_CWScan.png')
plt.close()

plot_prep()
plot_field(T_RGE1_CW0_D1, vev_RGE1_CW0_D1, 'solid', 'CW OFF', ['lightgreen', 'lightcoral', 'cornflowerblue'])
plot_field(T_RGE1_CW1_D1, vev_RGE1_CW1_D1, 'dashed', 'CW ON', ['green', 'red', 'blue'])
plt.title("RGE ON, Daisy high-T, CW scan")
plt.savefig('plots/bm_study/RGE1_Daisy1_CWScan.png')
plt.close()

plot_prep()
plot_field(T_RGE1_CW0_D2, vev_RGE1_CW0_D2, 'solid', 'CW OFF', ['lightgreen', 'lightcoral', 'cornflowerblue'])
plot_field(T_RGE1_CW1_D2, vev_RGE1_CW1_D2, 'dashed', 'CW ON', ['green', 'red', 'blue'])
plt.title("RGE ON, Daisy 1-step, CW scan")
plt.savefig('plots/bm_study/RGE1_Daisy2_CWScan.png')
plt.close()

plot_prep()
plot_field(T_RGE1_CW0_D3, vev_RGE1_CW0_D3, 'solid', 'CW OFF', ['lightgreen', 'lightcoral', 'cornflowerblue'])
plot_field(T_RGE1_CW1_D3, vev_RGE1_CW1_D3, 'dashed', 'CW ON', ['green', 'red', 'blue'])
plt.title("RGE ON, Daisy 2-step, CW scan")
plt.savefig('plots/bm_study/RGE1_Daisy3_CWScan.png')
plt.close()


# plots with fixed CW, Daisy
# scanning RGE
plot_prep()
plot_field(T_RGE0_CW0_D0, vev_RGE0_CW0_D0, 'solid', 'RGE OFF', ['lightgreen', 'lightcoral', 'cornflowerblue'])
plot_field(T_RGE1_CW0_D0, vev_RGE1_CW0_D0, 'dashed', 'RGE ON', ['green', 'red', 'blue'])
plt.title("Daisy OFF, CW OFF, RGE scan")
plt.savefig('plots/bm_study/Daisy0_CW0_RGEScan.png')
plt.close()

plot_prep()
plot_field(T_RGE0_CW0_D1, vev_RGE0_CW0_D1, 'solid', 'RGE OFF', ['lightgreen', 'lightcoral', 'cornflowerblue'])
plot_field(T_RGE1_CW0_D1, vev_RGE1_CW0_D1, 'dashed', 'RGE ON', ['green', 'red', 'blue'])
plt.title("Daisy high-T, CW OFF, RGE scan")
plt.savefig('plots/bm_study/Daisy1_CW0_RGEScan.png')
plt.close()

plot_prep()
plot_field(T_RGE0_CW0_D2, vev_RGE0_CW0_D2, 'solid', 'RGE OFF', ['lightgreen', 'lightcoral', 'cornflowerblue'])
plot_field(T_RGE1_CW0_D2, vev_RGE1_CW0_D2, 'dashed', 'RGE ON', ['green', 'red', 'blue'])
plt.title("Daisy 1-step, CW OFF, RGE scan")
plt.savefig('plots/bm_study/Daisy2_CW0_RGEScan.png')
plt.close()

plot_prep()
plot_field(T_RGE0_CW0_D3, vev_RGE0_CW0_D3, 'solid', 'RGE OFF', ['lightgreen', 'lightcoral', 'cornflowerblue'])
plot_field(T_RGE1_CW0_D3, vev_RGE1_CW0_D3, 'dashed', 'RGE ON', ['green', 'red', 'blue'])
plt.title("Daisy 2-step, CW OFF, RGE scan")
plt.savefig('plots/bm_study/Daisy3_CW0_RGEScan.png')
plt.close()

plot_prep()
plot_field(T_RGE0_CW0_D4, vev_RGE0_CW0_D4, 'solid', 'RGE OFF', ['lightgreen', 'lightcoral', 'cornflowerblue'])
#plot_field(T_RGE1_CW0_D3, vev_RGE1_CW0_D3, 'dashed', 'RGE ON', ['green', 'red', 'blue'])
plt.title("Daisy gap eq., CW OFF, RGE scan")
plt.savefig('plots/bm_study/Daisy4_CW0_RGEScan.png')
plt.close()

plot_prep()
plot_field(T_RGE0_CW1_D0, vev_RGE0_CW1_D0, 'solid', 'RGE OFF', ['lightgreen', 'lightcoral', 'cornflowerblue'])
plot_field(T_RGE1_CW1_D0, vev_RGE1_CW1_D0, 'dashed', 'RGE ON', ['green', 'red', 'blue'])
plt.title("Daisy OFF, CW ON, RGE scan")
plt.savefig('plots/bm_study/Daisy0_CW1_RGEScan.png')
plt.close()

plot_prep()
plot_field(T_RGE0_CW1_D1, vev_RGE0_CW1_D1, 'solid', 'RGE OFF', ['lightgreen', 'lightcoral', 'cornflowerblue'])
plot_field(T_RGE1_CW1_D1, vev_RGE1_CW1_D1, 'dashed', 'RGE ON', ['green', 'red', 'blue'])
plt.title("Daisy high-T, CW ON, RGE scan")
plt.savefig('plots/bm_study/Daisy1_CW1_RGEScan.png')
plt.close()

plot_prep()
plot_field(T_RGE0_CW1_D2, vev_RGE0_CW1_D2, 'solid', 'RGE OFF', ['lightgreen', 'lightcoral', 'cornflowerblue'])
plot_field(T_RGE1_CW1_D2, vev_RGE1_CW1_D2, 'dashed', 'RGE ON', ['green', 'red', 'blue'])
plt.title("Daisy 1-step, CW ON, RGE scan")
plt.savefig('plots/bm_study/Daisy2_CW1_RGEScan.png')
plt.close()

plot_prep()
plot_field(T_RGE0_CW1_D3, vev_RGE0_CW1_D3, 'solid', 'RGE OFF', ['lightgreen', 'lightcoral', 'cornflowerblue'])
plot_field(T_RGE1_CW1_D3, vev_RGE1_CW1_D3, 'dashed', 'RGE ON', ['green', 'red', 'blue'])
plt.title("Daisy 2-step, CW ON, RGE scan")
plt.savefig('plots/bm_study/Daisy3_CW1_RGEScan.png')
plt.close()

# plots to compare 1-step with gap (i.e. Daisy with Superdaisy)
plot_prep()
plot_field(T_RGE0_CW0_D2, vev_RGE0_CW0_D2, 'solid', '1-step', ['green', 'red', 'blue'])
plot_field(T_RGE0_CW0_D4, vev_RGE0_CW0_D4, 'dashed', 'gap eq.', ['darkgreen', 'darkred', 'darkblue'])
plt.title("RGE OFF, CW OFF, Daisy comparison")
plt.savefig('plots/bm_study/RGE0_CW0_DaisyComparison.png')
plt.close()
