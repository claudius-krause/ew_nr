""" Uses pytest to test the internal consistency of the model file """

import numpy as np
from scipy import optimize
import pytest

from cosmoTransitions.finiteT import Jb_spline as Jb
from cosmoTransitions.finiteT import Jf_spline as Jf

import helper.helper as helper
from helper.helper import partial_derivative as part_der
import helper.highT as highT
from model.Inert2HDM_N import model_2HDM_N
from model.RGE import RGE

def generate_field_config(num_pts, num_chi, full_field=False, same_chis=False):
    """ generates num_pts random field configurations.
        num_chi: int, number of chi singlets
        full_field: bool, if True, GBs are generated, too
        same_chis: bool, if True, all chi will have same value
    """
    num_fields = 2 + num_chi
    if full_field:
        num_fields += 6
    field_config = 1e3 * np.random.rand(num_pts, num_fields)
    if same_chis:
        for i in range(num_chi-1):
            field_config[..., -i-1] = field_config[..., -num_chi]
    return field_config

def generate_model(use_CW=0., use_Daisy=0., use_RGE=False):
    """ generates a model with random parameters """
    param_b2 = 200**2 * np.random.rand()
    param_b4 = 0.5* np.random.rand()
    param_a2 = (np.random.rand() - (1./3.)) * 0.75
    param_c2 = 200**2 * np.random.rand()
    param_c4 = 1.* np.random.rand()
    param_ct = (np.random.rand() - (1./3.)) * 0.75
    param_dt = (np.random.rand() - (1./3.)) * 0.75
    param_at = (np.random.rand() - (1./3.)) * 0.75
    param_c5 = 0.1 * np.random.rand()
    param_N = np.random.randint(low=1, high=31)
    param_dic = {'b2':param_b2, 'b4':param_b4, 'a2':param_a2, 'c2':param_c2, 'c4':param_c4,
                 'ct':param_ct, 'dt':param_dt, 'at':param_at, 'c5':param_c5, 'N':param_N}
    model_ret = model_2HDM_N(**param_dic, use_CW=use_CW, use_Daisy=use_Daisy,
                             use_RGE=use_RGE, use_CW_in_delta_m=False)
    return model_ret

@pytest.fixture
def generate_param_dict():
    """ generates random model parameters """
    param_b2 = 200**2 * np.random.rand()
    param_b4 = 0.5* np.random.rand()
    param_a2 = (np.random.rand() - (1./3.)) * 0.75
    param_c2 = 200**2 * np.random.rand()
    param_c4 = 1.* np.random.rand()
    param_ct = (np.random.rand() - (1./3.)) * 0.75
    param_dt = (np.random.rand() - (1./3.)) * 0.75
    param_at = (np.random.rand() - (1./3.)) * 0.75
    param_c5 = 0.1 * np.random.rand()
    param_N = np.random.randint(low=1, high=31)
    param_dic = {'b2':param_b2, 'b4':param_b4, 'a2':param_a2, 'c2':param_c2, 'c4':param_c4,
                 'ct':param_ct, 'dt':param_dt, 'at':param_at, 'c5':param_c5, 'N':param_N}
    return param_dic

@pytest.fixture
def generate_random_model_with_CW_no_RGE():
    model = generate_model(use_CW=1., use_Daisy=0., use_RGE=False)
    return model

@pytest.fixture
def generate_random_model_with_CW_with_RGE():
    while True:
        try:
            model = generate_model(use_CW=1., use_Daisy=0., use_RGE=True)
            break
        except RuntimeError:
            continue
    return model

@pytest.fixture
def generate_random_model_no_CW_no_RGE():
    model = generate_model(use_CW=0., use_Daisy=0., use_RGE=False)
    return model

@pytest.fixture
def generate_random_model_no_CW_with_RGE():
    while True:
        try:
            model = generate_model(use_CW=0., use_Daisy=0., use_RGE=True)
            break
        except RuntimeError:
            continue
    return model

def test_partial_derivative():
    pts = np.random.rand(10, 2)
    def func(x):
        return np.sin(x[..., 0]**2 + x[..., 1]**2)
    def first_der_x(x):
        return np.cos(x[..., 0]**2 + x[..., 1]**2)*2*x[..., 0]
    def first_der_y(x):
        return np.cos(x[..., 0]**2 + x[..., 1]**2)*2*x[..., 1]
    def second_der_x(x):
        ret = -np.sin(x[..., 0]**2 + x[..., 1]**2)*4.*x[..., 0]**2
        ret += np.cos(x[..., 0]**2 + x[..., 1]**2)*2.
        return ret
    def second_der_y(x):
        ret = -np.sin(x[..., 0]**2 + x[..., 1]**2)*4.*x[..., 1]**2
        ret += np.cos(x[..., 0]**2 + x[..., 1]**2)*2.
        return ret
    assert np.allclose(first_der_x(pts), part_der(func, pts, 0, n=1), rtol=1e-3)
    assert np.allclose(first_der_y(pts), part_der(func, pts, 1, n=1), rtol=1e-3)
    assert np.allclose(second_der_x(pts), part_der(func, pts, 0, n=2), rtol=1e-3)
    assert np.allclose(second_der_y(pts), part_der(func, pts, 1, n=2), rtol=1e-3)

NUM_PTS = 5
NUM_CHI = 10

def test_gen_field_conf_full_false_same_chi_false():
    fields = generate_field_config(NUM_PTS, NUM_CHI,
                                   full_field=False, same_chis=False)
    assert fields.shape == (NUM_PTS, NUM_CHI+2)
    assert not all(all(chi == fields[..., 2].T) for chi in fields[..., 2:].T)

def test_gen_field_conf_full_false_same_chi_True():
    fields = generate_field_config(NUM_PTS, NUM_CHI,
                                   full_field=False, same_chis=True)
    assert fields.shape == (NUM_PTS, NUM_CHI+2)
    assert all(all(chi == fields[..., 2].T) for chi in fields[..., 2:].T)

def test_gen_field_conf_full_true_same_chi_false():
    fields = generate_field_config(NUM_PTS, NUM_CHI,
                                   full_field=True, same_chis=False)
    assert fields.shape == (NUM_PTS, NUM_CHI+8)
    assert not all(all(chi == fields[..., 8].T) for chi in fields[..., 8:].T)

def test_gen_field_conf_full_true_same_chi_True():
    fields = generate_field_config(NUM_PTS, NUM_CHI,
                                   full_field=True, same_chis=True)
    assert fields.shape == (NUM_PTS, NUM_CHI+8)
    assert all(all(chi == fields[..., 8].T) for chi in fields[..., 8:].T)

def test_convert_to_full_different_chi():
    short_fields = generate_field_config(NUM_PTS, NUM_CHI,
                                         full_field=False, same_chis=False)
    long_fields = helper.convert_to_full(short_fields)
    assert long_fields.shape == (NUM_PTS, NUM_CHI+8)
    assert all(long_fields[..., 1] == 0.)
    assert all(long_fields[..., 2] == 0.)
    assert all(long_fields[..., 3] == 0.)
    assert all(long_fields[..., 5] == 0.)
    assert all(long_fields[..., 6] == 0.)
    assert all(long_fields[..., 7] == 0.)
    assert not all(all(chi == long_fields[..., 8].T) for chi in long_fields[..., 8:].T)
    assert np.allclose(np.delete(long_fields, [1, 2, 3, 5, 6, 7], axis=-1),
                       short_fields)

def test_convert_to_full_same_chi():
    short_fields = generate_field_config(NUM_PTS, NUM_CHI,
                                         full_field=False, same_chis=True)
    long_fields = helper.convert_to_full(short_fields)
    assert long_fields.shape == (NUM_PTS, NUM_CHI+8)
    assert all(long_fields[..., 1] == 0.)
    assert all(long_fields[..., 2] == 0.)
    assert all(long_fields[..., 3] == 0.)
    assert all(long_fields[..., 5] == 0.)
    assert all(long_fields[..., 6] == 0.)
    assert all(long_fields[..., 7] == 0.)
    assert all(all(chi == long_fields[..., 8].T) for chi in long_fields[..., 8:].T)
    assert np.allclose(np.delete(long_fields, [1, 2, 3, 5, 6, 7], axis=-1),
                       short_fields)

def test_convert_to_short_different_chi():
    long_fields = generate_field_config(NUM_PTS, NUM_CHI,
                                        full_field=True, same_chis=False)
    short_fields = helper.convert_to_short(long_fields)
    assert short_fields.shape == (NUM_PTS, NUM_CHI+2)
    assert not all(all(chi == short_fields[..., 2].T) for chi in short_fields[..., 2:].T)
    assert np.allclose(np.delete(long_fields, [1, 2, 3, 5, 6, 7], axis=-1),
                       short_fields)

def test_convert_to_short_same_chi():
    long_fields = generate_field_config(NUM_PTS, NUM_CHI,
                                        full_field=True, same_chis=True)
    short_fields = helper.convert_to_short(long_fields)
    assert short_fields.shape == (NUM_PTS, NUM_CHI+2)
    assert all(all(chi == short_fields[..., 2].T) for chi in short_fields[..., 2:].T)
    assert np.allclose(np.delete(long_fields, [1, 2, 3, 5, 6, 7], axis=-1),
                       short_fields)

def test_CW_boundary_of_random_model_no_RGE(generate_random_model_with_CW_no_RGE):
    model = generate_random_model_with_CW_no_RGE
    vev_field = np.concatenate([[246., 0.], model.N*[0.]])

    first_der = part_der(model.Vtot, var=0, x0=vev_field, n=1, dx=1e-3, T=0.)
    second_der = part_der(model.Vtot, var=0, x0=vev_field, n=2, dx=1e-1, T=0.)
    assert np.allclose([0., 125.], [first_der, np.sqrt(second_der + 0j).real],
                       rtol=1e-3, atol=1e-3)

def test_CW_boundary_of_random_model_with_RGE(generate_random_model_with_CW_with_RGE):
    model = generate_random_model_with_CW_with_RGE
    vev_field = np.concatenate([[246., 0.], model.N*[0.]])

    first_der = part_der(model.Vtot, var=0, x0=vev_field, n=1, dx=1e-3, T=0.)
    second_der = part_der(model.Vtot, var=0, x0=vev_field, n=2, dx=1e-1, T=0.)
    assert np.allclose([0., 125.], [first_der, np.sqrt(second_der + 0j).real],
                       rtol=1e-3, atol=1e-3)

def test_ren_scale_with_CW(generate_random_model_with_CW_with_RGE):
    model = generate_random_model_with_CW_with_RGE
    fields = generate_field_config(NUM_PTS, model.N,
                                   full_field=False, same_chis=True)
    scales = model.current_scale(fields)
    boson_mass, _, _ = model.boson_massSq(fields, T=0, scale=scales)
    assert np.allclose(scales, np.maximum(np.max(np.sqrt(np.abs(boson_mass)), axis=-1), 246.))

def test_ren_scale_no_CW(generate_random_model_no_CW_with_RGE):
    model = generate_random_model_no_CW_with_RGE
    fields = generate_field_config(NUM_PTS, model.N,
                                   full_field=False, same_chis=True)
    scales = model.current_scale(fields)
    boson_mass, _, _ = model.boson_massSq(fields, T=0, scale=scales)
    assert np.allclose(scales, np.maximum(np.max(np.sqrt(np.abs(boson_mass)), axis=-1), 246.))

def test_daisy_1_at_t_0_no_RGE(generate_random_model_no_CW_no_RGE):
    model = generate_random_model_no_CW_no_RGE
    vev_field = helper.convert_to_full(np.concatenate([[246., 0.],
                                                       model.N*[0.]]))
    assert not np.any(model.HighT_Daisy(vev_field, 0.))

def test_daisy_1_at_t_0_with_RGE(generate_random_model_no_CW_with_RGE):
    model = generate_random_model_no_CW_with_RGE
    vev_field = np.concatenate([[246., 0.], model.N*[0.]])
    vev_scale = model.current_scale(vev_field)
    vev_field = helper.convert_to_full(vev_field)
    assert not np.any(model.HighT_Daisy(vev_field, 0., scale=vev_scale))

def test_daisy_2_at_t_0_no_RGE(generate_random_model_no_CW_no_RGE):
    model = generate_random_model_no_CW_no_RGE
    vev_field = helper.convert_to_full(np.concatenate([[246., 0.],
                                                       model.N*[0.]]))
    assert not np.any(model.Improved_Daisy(vev_field, 0., improved=False))

def test_daisy_2_at_t_0_with_RGE(generate_random_model_no_CW_with_RGE):
    model = generate_random_model_no_CW_with_RGE
    vev_field = np.concatenate([[246., 0.], model.N*[0.]])
    vev_scale = model.current_scale(vev_field)
    vev_field = helper.convert_to_full(vev_field)
    assert not np.any(model.Improved_Daisy(vev_field, 0., scale=vev_scale,
                                           improved=False))

def test_daisy_3_at_t_0_no_RGE(generate_random_model_no_CW_no_RGE):
    model = generate_random_model_no_CW_no_RGE
    vev_field = helper.convert_to_full(np.concatenate([[246., 0.],
                                                       model.N*[0.]]))
    assert not np.any(model.Improved_Daisy(vev_field, 0., improved=True))

def test_daisy_3_at_t_0_with_RGE(generate_random_model_no_CW_with_RGE):
    model = generate_random_model_no_CW_with_RGE
    vev_field = np.concatenate([[246., 0.], model.N*[0.]])
    vev_scale = model.current_scale(vev_field)
    vev_field = helper.convert_to_full(vev_field)
    assert not np.any(model.Improved_Daisy(vev_field, 0., scale=vev_scale,
                                           improved=True))

def test_daisy_4_at_t_0_no_RGE(generate_random_model_no_CW_no_RGE):
    model = generate_random_model_no_CW_no_RGE
    vev_field = helper.convert_to_full(np.concatenate([[246., 0.],
                                                       model.N*[0.]]))
    assert not np.any(model.Gap_Daisy([vev_field], 0.))

def test_daisy_4_at_t_0_with_RGE(generate_random_model_no_CW_with_RGE):
    model = generate_random_model_no_CW_with_RGE
    vev_field = np.concatenate([[246., 0.], model.N*[0.]])
    vev_scale = model.current_scale(vev_field)
    vev_field = helper.convert_to_full(vev_field)
    assert not np.any(model.Gap_Daisy([vev_field], 0., scale=vev_scale))

def test_scale_handling_daisy_1(generate_random_model_with_CW_with_RGE):
    model = generate_random_model_with_CW_with_RGE
    fields = generate_field_config(3, model.N, full_field=True, same_chis=True)

    scale_1 = 246.
    scale_2 = 500.
    scale_3 = 1e4

    scale_array = np.array([scale_1, scale_2, scale_3])
    T_now = 1e3*np.random.rand()

    model.use_Daisy = 1.
    direct = model.delta_m_daisy(fields, T_now, scale=scale_array)
    indirect = []
    for i in range(3):
        indirect.append(model.delta_m_daisy(fields[i], T_now, scale=scale_array[i]))
    indirect = np.array(indirect)
    assert np.all(direct == indirect)

def test_scale_handling_daisy_2(generate_random_model_with_CW_with_RGE):
    model = generate_random_model_with_CW_with_RGE
    fields = generate_field_config(3, model.N, full_field=True, same_chis=True)
    
    scale_1 = 246.
    scale_2 = 500.
    scale_3 = 1e4

    scale_array = np.array([scale_1, scale_2, scale_3])
    T_now = 1e3*np.random.rand()

    model.use_Daisy = 2.
    direct = model.delta_m_daisy(fields, T_now, scale=scale_array)
    indirect = []
    for i in range(3):
        indirect.append(model.delta_m_daisy(fields[i], T_now, scale=scale_array[i]))
    indirect = np.array(indirect)
    assert np.all(direct == indirect)

def test_scale_handling_daisy_3(generate_random_model_with_CW_with_RGE):
    model = generate_random_model_with_CW_with_RGE
    fields = generate_field_config(3, model.N, full_field=True, same_chis=True)

    scale_1 = 246.
    scale_2 = 500.
    scale_3 = 1e4

    scale_array = np.array([scale_1, scale_2, scale_3])
    T_now = 1e3*np.random.rand()

    model.use_Daisy = 3.
    direct = model.delta_m_daisy(fields, T_now, scale=scale_array)
    indirect = []
    for i in range(3):
        indirect.append(model.delta_m_daisy(fields[i], T_now, scale=scale_array[i]))
    indirect = np.array(indirect)
    assert np.all(direct == indirect)

def test_scale_handling_daisy_4(generate_random_model_with_CW_with_RGE):
    model = generate_random_model_with_CW_with_RGE
    fields = generate_field_config(3, model.N, full_field=True, same_chis=True)

    scale_1 = 246.
    scale_2 = 500.
    scale_3 = 1e4

    scale_array = np.array([scale_1, scale_2, scale_3])
    T_now = 1e3*np.random.rand()

    model.use_Daisy = 4.
    direct = model.delta_m_daisy(fields, T_now, scale=scale_array)
    indirect = []
    for i in range(3):
        indirect.append(model.delta_m_daisy(fields[i], T_now, scale=scale_array[i]))
    indirect = np.array(indirect)
    assert np.all(direct == indirect)

def test_delta_m_storage_daisy_2(generate_random_model_with_CW_no_RGE, capfd):
    model = generate_random_model_with_CW_no_RGE
    fields = generate_field_config(5, model.N, full_field=False, same_chis=True)
    scales = model.current_scale(fields)
    which = np.random.choice(range(5), 3, replace=False)
    T_now = 1e3*np.random.rand()
    model.use_Daisy = 2.
    model.delta_m_daisy(helper.convert_to_full(fields[which]), T_now, scale=scales[which])

    model.delta_m_daisy(helper.convert_to_full(fields), T_now, scale=scales)
    out, _ = capfd.readouterr()
    assert out.count("Recycled") == 3

    helper.save_delta_dics(model)
    model.store_delta_m_2 = {}
    helper.load_delta_dics(model)
    model.delta_m_daisy(helper.convert_to_full(fields), T_now, scale=scales)
    out, _ = capfd.readouterr()
    assert out.count("Recycled") == 5
    helper.clean_delta_dics(model)

def test_delta_m_storage_daisy_3(generate_random_model_with_CW_no_RGE, capfd):
    model = generate_random_model_with_CW_no_RGE
    fields = generate_field_config(5, model.N, full_field=False, same_chis=True)
    scales = model.current_scale(fields)
    which = np.random.choice(range(5), 3, replace=False)
    T_now = 1e3*np.random.rand()
    model.use_Daisy = 3.
    model.delta_m_daisy(helper.convert_to_full(fields[which]), T_now, scale=scales[which])

    model.delta_m_daisy(helper.convert_to_full(fields), T_now, scale=scales)
    out, _ = capfd.readouterr()
    assert out.count("Recycled") == 3

    helper.save_delta_dics(model)
    model.store_delta_m_3 = {}
    helper.load_delta_dics(model)
    model.delta_m_daisy(helper.convert_to_full(fields), T_now, scale=scales)
    out, _ = capfd.readouterr()
    assert out.count("Recycled") == 5
    helper.clean_delta_dics(model)

def test_delta_m_storage_daisy_4(generate_random_model_with_CW_no_RGE, capfd):
    model = generate_random_model_with_CW_no_RGE
    fields = generate_field_config(5, model.N, full_field=False, same_chis=True)
    scales = model.current_scale(fields)
    which = np.random.choice(range(5), 3, replace=False)
    T_now = 1e3*np.random.rand()
    model.use_Daisy = 4.
    model.delta_m_daisy(helper.convert_to_full(fields[which]), T_now, scale=scales[which])

    model.delta_m_daisy(helper.convert_to_full(fields), T_now, scale=scales)
    out, _ = capfd.readouterr()
    assert out.count("Recycled") == 3

    helper.save_delta_dics(model)
    model.store_delta_m_4 = {}
    helper.load_delta_dics(model)
    model.delta_m_daisy(helper.convert_to_full(fields), T_now, scale=scales)
    out, _ = capfd.readouterr()
    assert out.count("Recycled") == 5
    helper.clean_delta_dics(model)

def test_BFB_lbda(generate_random_model_no_CW_no_RGE):
    model = generate_random_model_no_CW_no_RGE
    assert model.BFB() == True

def test_BFB_b4(generate_param_dict):
    param_dict = generate_param_dict
    model = model_2HDM_N(**param_dict, use_CW=0., use_Daisy=0,
                         use_RGE=False, use_CW_in_delta_m=False)
    assert model.BFB() == True
    param_dict['b4'] = -model.b4
    model = model_2HDM_N(**param_dict, use_CW=0., use_Daisy=0,
                         use_RGE=False, use_CW_in_delta_m=False)
    assert model.BFB() == False

def test_BFB_c4c5(generate_param_dict):
    param_dict = generate_param_dict
    model = model_2HDM_N(**param_dict, use_CW=0., use_Daisy=0,
                         use_RGE=False, use_CW_in_delta_m=False)
    assert model.BFB() == True
    param_dict['c4'] = 0.5
    param_dict['c5'] = -0.1
    param_dict['N'] = 4
    model = model_2HDM_N(**param_dict, use_CW=0., use_Daisy=0,
                         use_RGE=False, use_CW_in_delta_m=False)
    assert model.BFB() == True
    param_dict['N'] = 10
    model = model_2HDM_N(**param_dict, use_CW=0., use_Daisy=0,
                         use_RGE=False, use_CW_in_delta_m=False)
    assert model.BFB() == False
    param_dict['c4'] = -0.5
    param_dict['c5'] = -0.1
    model = model_2HDM_N(**param_dict, use_CW=0., use_Daisy=0,
                         use_RGE=False, use_CW_in_delta_m=False)
    assert model.BFB() == False

