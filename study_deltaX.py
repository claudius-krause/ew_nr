""" This script will scan different stepsizes delta_x in the numerical
    derivative used to find the thermal mass, in order to find a prescription
    that works for most field values/temperatures.
"""

import numpy as np
import pandas as pd

import helper.helper as helper
from helper.helper import partial_derivative as part_der
from model.Inert2HDM_N import model_2HDM_N


# set up the model
WHICH_DAISY = 2.

num_chi = 100
model_parameters = {'b2': 100**2, 'b4': 0.126, 'c2': 10**2, 'c4': 1.,
                    'a2': -0.02, 'ct': -0.06, 'dt': 0., 'at': 0., 'c5': 0.,
                    'N': num_chi, 'use_Daisy': WHICH_DAISY,
                    'use_CW_in_delta_m':False,
                    'use_CW': 1., 'use_RGE': False}

model = model_2HDM_N(**model_parameters)

def add_all_directions(field_array, field_value):
    """ adds all 8 direction of field space to field_array """
    new_field = np.zeros((8, num_chi+8))
    field_content = helper.guess_plain(field_value)
    new_field[..., 0] = field_content[..., 0]
    new_field[..., 4] = field_content[..., 1]
    new_field[..., 8:] = np.ones((1, model.N)) * field_content[..., 2, np.newaxis]

    for entry in new_field:
        field_array.append(entry)
    return field_array

def add_specific(field_array, higgs, inert, chi):
    higgsGB = 0.
    inertGB = 0.
    new_field = np.concatenate([[higgs], 3*[higgsGB], [inert], 3*[inertGB], model.N*[chi]])
    field_array.append(new_field)
    return field_array

def add_random(field_array, num_fields, with_GB=False):
    for _ in range(num_fields):
        higgs = 1e3 * np.random.rand()
        inert = 1e3 * np.random.rand()
        chi = 1e3 * np.random.rand()
        if with_GB:
            higgsGB = 1e3 * np.random.rand()
            inertGB = 1e3 * np.random.rand()
        else:
            higgsGB = 0.
            inertGB = 0.
        new_field = np.concatenate([[higgs], 3*[higgsGB], [inert], 3*[inertGB], model.N*[chi]])
        field_array.append(new_field)
    return field_array

temp = np.array([100., 200., 500., 1000.])
delta_x_factors = np.array([0.001, 0.01, 0.05, 0.1, 0.5, 1., 2., 10., 20., 100., 1e3])

# add fields to study
fields = []
fields = add_all_directions(fields, 200.)
fields = add_random(fields, 5, with_GB=True)
fields = add_random(fields, 5)
fields = np.array(len(temp)*fields)

short_fields = helper.convert_to_short(fields)

# create pandas dataframe with field entries
output = pd.DataFrame(fields[..., [0, 1, 4, 5, 8]])
output.columns = ['Higgs', 'HiggsGB', 'Inert', 'InertGB', 'Chi']
print("DataFrame created")

# add temperature entry
temp_list = np.outer(temp, np.ones(int(len(fields)/len(temp)))).flatten()
output['temperature'] = temp_list
print("Temperature added")

# delta_x taken from our code in Inert2HDM_N
delta_x = np.maximum(np.maximum(np.max(fields, axis=-1)*2.5e-4*np.sqrt(temp_list), 2.5e-4*np.sqrt(temp_list)), 2.5e-4)
output['dx'] = delta_x
print("delta x added")

# the actual work
deltaM = []
for index, field in enumerate(fields):
    for del_dx in delta_x_factors:
        deltaM.append(model.Improved_Daisy(field, temp_list[index], rel_dx_factor=del_dx)[:9])
    print("done with field {}/{}".format(index+1, len(fields)))

# deltaM has shape (len(rel_dx_factors)*len(fields), 9)
deltaM = np.array(deltaM)

for index, value in enumerate(delta_x_factors):
    output[str(value)+'*dx, Higgs'] = deltaM[index::len(delta_x_factors), 0]
print("Higgs added")
for index, value in enumerate(delta_x_factors):
    output[str(value)+'*dx, HiggsGB1'] = deltaM[index::len(delta_x_factors), 1]
print("HiggsGB1 added")
for index, value in enumerate(delta_x_factors):
    output[str(value)+'*dx, HiggsGB2'] = deltaM[index::len(delta_x_factors), 2]
print("HiggsGB2 added")
for index, value in enumerate(delta_x_factors):
    output[str(value)+'*dx, HiggsGB3'] = deltaM[index::len(delta_x_factors), 3]
print("HiggsGB3 added")
for index, value in enumerate(delta_x_factors):
    output[str(value)+'*dx, Inert'] = deltaM[index::len(delta_x_factors), 4]
print("Inert added")
for index, value in enumerate(delta_x_factors):
    output[str(value)+'*dx, InertGB1'] = deltaM[index::len(delta_x_factors), 5]
print("InertGB1 added")
for index, value in enumerate(delta_x_factors):
    output[str(value)+'*dx, InertGB2'] = deltaM[index::len(delta_x_factors), 6]
print("InertGB2 added")
for index, value in enumerate(delta_x_factors):
    output[str(value)+'*dx, InertGB3'] = deltaM[index::len(delta_x_factors), 7]
print("InertGB3 added")
for index, value in enumerate(delta_x_factors):
    output[str(value)+'*dx, Chi'] = deltaM[index::len(delta_x_factors), 8]
print("Singlet added")

print(output)

file_name = 'delta_x_study.txt'
output.to_csv(file_name, sep='\t', float_format='%.8e')
print("Values saved to {}".format(file_name))
