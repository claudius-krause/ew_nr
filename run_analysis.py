""" Runs the main analysis of our project. """

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
from matplotlib.patches import Patch
from scipy import optimize
from cycler import cycler
from scipy.misc import derivative as der

from cosmoTransitions.finiteT import Jb_spline as Jb
from cosmoTransitions.finiteT import Jf_spline as Jf

from helper.helper import partial_derivative as part_der
import helper.helper as helper
import helper.highT as highT
from model.Inert2HDM_N import model_2HDM_N
from model.RGE import RGE
from model.washout import SphaleronDecayRate

#WHICH_BM = 'light'  # BM A in https://arxiv.org/abs/2104.00638
WHICH_BM = 'heavy'  # BM B in https://arxiv.org/abs/2104.00638

if WHICH_BM == 'light':
    # BM A of https://arxiv.org/abs/2104.00638
    num_chi = 250
    model_parameters = {'b2': 2.5e3, 'b4': 0.1, 'c2': 1e2, 'c4': 0.,
                        'a2': -0.001, 'ct': -0.06, 'dt': 0., 'at': 0., 'c5': 0.01,
                        'N': num_chi, 'use_CW': 1., 'use_Daisy': 4.,
                        'use_RGE': True, 'use_CW_in_delta_m':False,
                        'use_delta_m_in_ren_scale':True}
elif WHICH_BM == 'heavy':
    # BM B of https://arxiv.org/abs/2104.00638
    num_chi = 600
    model_parameters = {'b2': 5.8e3, 'b4': 0.1, 'c2': 5e3, 'c4': 0.,
                        'a2': 0.01, 'ct': -0.0375, 'dt': 0., 'at': 0., 'c5': 0.004,
                        'N': num_chi, 'use_CW': 0., 'use_Daisy': 2.,
                        'use_RGE': True, 'use_CW_in_delta_m':False,
                        'use_delta_m_in_ren_scale':True}
else:
    raise ValueError

model = model_2HDM_N(**model_parameters)
print("Model is bounded: {}".format(model.BFB()))

# which daisy to include in plots
# PLOT_TRACED_MINIMA:
which_to_plot = ['0'\
                 , '1'\
                 , '2'\
                 #, '3'\
                 #, '4'\
                 ]
# using wave function renormalization with RGE:
USE_WFR = model.use_RGE

# used for https://arxiv.org/abs/2104.00638:
TRACE_MINIMA_GLOBAL = False

PLOT_TRACED_MINIMA = False # True
PLOT_V_T = False #True
PLOT_TRACED_MINIMA_CW = False  # plots both and combines CW results

PRINT_PHYSICAL_MASSES = True
COMPUTE_BARYON_WASHOUT = False

# old functions/checks :
PLOT_THERMAL_OF_MINIMA = False #True
CHECK_HIGH_T = False
PLOT_SCALE = False
PLOT_RUNNING = False
PLOT_THERMAL = False
PLOT_THERMAL_ZOOM = False
PLOT_THERMAL_JOINT = False
PLOT_THERMAL_AT_FIELD = False
CHECK_MIN_TZERO = False
TRACE_MINIMA_LOCAL = False
DENOISE_MINIMA = False
PLOT_BFB = False
PLOT_POTENTIAL = False
SCAN_GLOBAL = False
SCAN_GLOBAL_2DIM = False
DEBUG_STUFF = False
FIND_BFB_SCALE = False

computed_scales = {}
m_thermal_store = {}
T_array_store = {}

if (not PLOT_SCALE and not PLOT_RUNNING and not PLOT_THERMAL\
    and not PLOT_THERMAL_ZOOM and not CHECK_MIN_TZERO\
    and not TRACE_MINIMA_GLOBAL and not TRACE_MINIMA_LOCAL\
    and not PLOT_TRACED_MINIMA  and not PRINT_PHYSICAL_MASSES\
    and not PLOT_BFB and not PLOT_POTENTIAL and not SCAN_GLOBAL\
    and not PLOT_V_T and not PLOT_THERMAL_OF_MINIMA\
    and not DENOISE_MINIMA and not COMPUTE_BARYON_WASHOUT\
    and not SCAN_GLOBAL_2DIM and not DEBUG_STUFF and not CHECK_HIGH_T\
    and not PLOT_THERMAL_AT_FIELD and not FIND_BFB_SCALE\
    and not PLOT_THERMAL_JOINT and not PLOT_TRACED_MINIMA_CW):
    print("Nothing selected, what am I supposed to do?")

if PLOT_SCALE:
    print("* * * * * plotting renormalization scale * * * * *")
    raise("change np.linalg.eigvalsh() to np.linalg.eig()[0] in boson_massSq()")

    plot_min = 1e0
    plot_max = 1e5
    plot_num_pts = 251
    x_pts = np.logspace(np.log10(plot_min), np.log10(plot_max), plot_num_pts)

    field = np.concatenate([[np.zeros(x_pts.shape), x_pts], num_chi * [np.zeros(x_pts.shape)]]).T

    temperature = 1e5

    ren_scale = model.current_scale(field, temperature)

    if model.use_delta_m_in_ren_scale:
        if temperature == 0:
            deltaM = None
        else:
            deltaM = model.HighT_Daisy(helper.convert_to_full(field), temperature, scale=ren_scale)

        masses, _, _ = model.boson_massSq(field, T=temperature, scale=ren_scale, deltaM=deltaM)
    else:
        masses, _, _ = model.boson_massSq(field, T=0., scale=ren_scale, deltaM=None)
    masses = masses.T

    # check for HiggsGB:
    all_higgsGB_same = True
    for i in range(2):
        all_higgsGB_same = all_higgsGB_same and np.allclose(masses[i+1], masses[i+2])

    # check for InertGB:
    all_inertGB_same = True
    for i in range(2):
        all_inertGB_same = all_inertGB_same and np.allclose(masses[i+5], masses[i+6])

    # check for same chis:
    all_chi_same = True
    for chi_i in range(model.N-1):
        all_chi_same = all_chi_same and np.allclose(masses[8+chi_i], masses[9+chi_i])

    print("checking if all Higgs GB have same masses: {}".format(all_higgsGB_same))
    print("checking if all Inert GB have same masses: {}".format(all_inertGB_same))
    print("checking if all {} chis have same masses: {}".format(model.N, all_chi_same))


    # plot 1: M(phi), linear scale

    fig = plt.figure(dpi=150, figsize=[6., 6.])
    axis = fig.add_subplot(111)
    plt.xlim([plot_min, plot_max])
    plt.xscale('log')

    for index, values in enumerate(masses):
        if index in (2, 3) and all_higgsGB_same:
            continue
        if index in (6, 7) and all_inertGB_same:
            continue
        if index in range(9, 8+model.N) and all_chi_same:
            continue
        plt.plot(x_pts, np.sqrt(np.abs(values)), label=model.boson_name(index), lw=1.5)
    plt.plot(x_pts, ren_scale, ls='dashed', lw=1., color='k', label='ren. scale')

    plt.title(helper.make_param_string(model_parameters))
    plt.legend(loc='upper left')
    plt.xlabel(r"$\phi$")
    plt.ylabel(r"$M(\mu_R)$")
    plt.savefig('plots/M_scan.png')
    plt.close()
    print("1st plot done, repeating with log-scale")


    # plot 2: M(phi), log scale

    fig = plt.figure(dpi=150, figsize=[6., 6.])
    axis = fig.add_subplot(111)
    plt.xlim([plot_min, plot_max])
    plt.xscale('log')
    plt.yscale('log')

    for index, values in enumerate(masses):
        if index in (2, 3) and all_higgsGB_same:
            continue
        if index in (6, 7) and all_inertGB_same:
            continue
        if index in range(9, 8+model.N) and all_chi_same:
            continue
        if index == len(masses)-1:
            # exclude photon in this plot
            continue
        plt.plot(x_pts, np.sqrt(np.abs(values)), label=model.boson_name(index), lw=1.5)
    plt.plot(x_pts, ren_scale, ls='dashed', lw=1., color='k', label='ren. scale')

    plt.title(helper.make_param_string(model_parameters))
    plt.legend(loc='upper left')
    plt.xlabel(r"$\phi$")
    plt.ylabel(r"$M(\mu_R)$")
    plt.savefig('plots/M_scan_log.png')
    plt.close()
    print("2nd plot done, now ratios")


    # plot 3: M(phi)/mu_R, linear scale

    fig = plt.figure(dpi=150, figsize=[6., 6.])
    axis = fig.add_subplot(111)
    plt.xlim([plot_min, plot_max])
    plt.xscale('log')

    for index, values in enumerate(masses):
        if index in (2, 3) and all_higgsGB_same:
            continue
        if index in (6, 7) and all_inertGB_same:
            continue
        if index in range(9, 8+model.N) and all_chi_same:
            continue
        plt.plot(x_pts, np.sqrt(np.abs(values))/ren_scale, label=model.boson_name(index), lw=1.5)
    plt.title(helper.make_param_string(model_parameters))
    plt.text(1e3, 0.2, r'normalized to $\mu_R$')
    plt.legend(loc='upper left')
    plt.xlabel(r"$\phi$")
    plt.ylabel(r"$M(\mu_R)/\mu_R$")
    plt.savefig('plots/M_scan_ratio.png')
    plt.close()
    print("3rd plot done, repeating with log-scale")


    # plot 4: M(phi)/mu_R, log scale

    fig = plt.figure(dpi=150, figsize=[6., 6.])
    axis = fig.add_subplot(111)
    plt.xlim([plot_min, plot_max])
    plt.xscale('log')
    plt.yscale('log')

    for index, values in enumerate(masses):
        if index in (2, 3) and all_higgsGB_same:
            continue
        if index in (6, 7) and all_inertGB_same:
            continue
        if index in range(9, 8+model.N) and all_chi_same:
            continue
        if index == len(masses)-1:
            # exclude photon in this plot
            continue
        plt.plot(x_pts, np.sqrt(np.abs(values))/ren_scale, label=model.boson_name(index), lw=1.5)

    plt.title(helper.make_param_string(model_parameters))
    plt.text(1e1, 5e-3, r'normalized to $\mu_R$')
    plt.legend(loc='lower right')
    plt.xlabel(r"$\phi$")
    plt.ylabel(r"$M(\mu_R)/\mu_R$")
    plt.savefig('plots/M_scan_ratio_log.png')
    plt.close()
    print("4th plot done")

    print(model.dict_gs, model.dict_g, model.dict_gp, model.dict_yt,
          model.dict_b2, model.dict_b4, model.dict_a2, model.dict_c2,
          model.dict_c4, model.dict_ct, model.dict_dt, model.dict_at,
          model.dict_c5, model.dict_lbda, model.dict_mu2, model.dict_omega,
          sep='\n\n\n')

if PLOT_RUNNING:
    print("* * * * * plotting running couplings * * * * *")
    raise NotImplementedError

if PLOT_THERMAL:
    print("* * * * * plotting thermal masses * * * * *")

    plt.rc('text', usetex=True)
    plt.rc('text.latex', preamble=r'\usepackage{amsmath,amssymb}')
    plt.rc('font', family='serif')
    plt.rc('font', size=12)

    plot_min = 0.
    plot_max = 5e3
    plot_num_pts = 45
    phi_grid = np.linspace(plot_min, plot_max, plot_num_pts)
    T = np.array([10., 50., 100., 200., 500., 1e3])
    #T = np.array([150., 250.])
    field = np.concatenate([[np.zeros_like(phi_grid), phi_grid], model.N * [np.zeros_like(phi_grid)]]).T
    ren_scale = []
    for current_T in T:
        ren_scale.append(model.current_scale(field, current_T))
    field = helper.convert_to_full(field)
    print("done computing renormalization scale, start with thermal masses ....")

    #print(model.dict_gs, model.dict_g, model.dict_gp, model.dict_yt, model.dict_b2, model.dict_b4, model.dict_a2, model.dict_c2, model.dict_c4, model.dict_ct, model.dict_dt, model.dict_at, model.dict_c5, model.dict_lbda, model.dict_mu2, model.dict_omega, sep='\n')

    daisy_label_1 = helper.get_daisy_label('1')
    daisy_label_2 = helper.get_daisy_label('2')
    #daisy_label_3 = helper.get_daisy_label('3')
    daisy_label_4 = helper.get_daisy_label('4')

    for index, current_T in enumerate(T):
        #helper.load_delta_dics(model)
        model.use_Daisy = 1.
        daisy_1_RGE = model.delta_m_daisy(field, current_T, scale=ren_scale[index])
        print("Done with Daisy {} at T={}".format(1, current_T))
        model.use_Daisy = 2.
        daisy_2_RGE = model.delta_m_daisy(field, current_T, scale=ren_scale[index])
        print("Done with Daisy {} at T={}".format(2, current_T))
        #model.use_Daisy = 3.
        #daisy_3_RGE = model.delta_m_daisy(field, current_T, scale=ren_scale[index])
        #print("Done with Daisy {} at T={}".format(3, current_T))
        model.use_Daisy = 4.
        daisy_4_RGE = model.delta_m_daisy(field, current_T, scale=ren_scale[index])
        print("Done with Daisy {} at T={}".format(4, current_T))
        #helper.save_delta_dics(model)

        print("Higgs plot:")
        fig = plt.figure(dpi=300, figsize=[8., 8.])
        axis = fig.add_subplot(111)
        plt.xlim([phi_grid[0], phi_grid[-1]])


        plt.plot(phi_grid, daisy_1_RGE[..., 0], label="Higgs and GB, "+daisy_label_1,
                 lw=3., color='lightcoral', ls='dashed')

        plt.plot(phi_grid, daisy_2_RGE[..., 1], label="HiggsGB, "+daisy_label_2,
                 color='lightcoral', ls='dashdot', lw=2.)
        plt.plot(phi_grid, daisy_2_RGE[..., 2], label="HiggsGB, "+daisy_label_2,
                 color='lightcoral', ls='dashdot', lw=2.)
        plt.plot(phi_grid, daisy_2_RGE[..., 3], label="HiggsGB, "+daisy_label_2,
                 color='lightcoral', ls='dashdot', lw=2.)
        plt.plot(phi_grid, daisy_2_RGE[..., 0], label="Higgs, "+daisy_label_2,
                 color='red', ls='dashdot', lw=2.)

        plt.plot(phi_grid, daisy_4_RGE[..., 1], label="HiggsGB, "+daisy_label_4,
                 color='lightcoral', ls='dotted', lw=1.)
        plt.plot(phi_grid, daisy_4_RGE[..., 2], label="HiggsGB, "+daisy_label_4,
                 color='lightcoral', ls='dotted', lw=1.)
        plt.plot(phi_grid, daisy_4_RGE[..., 3], label="HiggsGB, "+daisy_label_4,
                 color='lightcoral', ls='dotted', lw=1.)
        plt.plot(phi_grid, daisy_4_RGE[..., 0], label="Higgs, "+daisy_label_4,
                 color='red', ls='dotted', lw=1.)

        plt.title(r"Thermal masses, Higgs Doublet, $T = $"+str(current_T)+" GeV")
        plt.legend()
        plt.xlabel(r"Inert $\phi$")
        plt.savefig('plots/'+WHICH_BM+'_M_th_Higgs_T_'+str(current_T)+'.png')
        plt.close()

        print("Inert plot:")
        fig = plt.figure(dpi=300, figsize=[8., 8.])
        axis = fig.add_subplot(111)
        plt.xlim([phi_grid[0], phi_grid[-1]])
        # 'cornflowerblue'

        plt.plot(phi_grid, daisy_1_RGE[..., 4], label="Inert and GB, "+daisy_label_1,
                 lw=3., color='cornflowerblue', ls='dashed')

        plt.plot(phi_grid, daisy_2_RGE[..., 5], label="InertGB, "+daisy_label_2,
                 color='cornflowerblue', ls='dashdot', lw=2.)
        plt.plot(phi_grid, daisy_2_RGE[..., 6], label="InertGB, "+daisy_label_2,
                 color='cornflowerblue', ls='dashdot', lw=2.)
        plt.plot(phi_grid, daisy_2_RGE[..., 7], label="InertGB, "+daisy_label_2,
                 color='cornflowerblue', ls='dashdot', lw=2.)
        plt.plot(phi_grid, daisy_2_RGE[..., 4], label="Inert, "+daisy_label_2,
                 color='blue', ls='dashdot', lw=2.)

        plt.plot(phi_grid, daisy_4_RGE[..., 5], label="InertGB, "+daisy_label_4,
                 color='cornflowerblue', ls='dotted', lw=1.)
        plt.plot(phi_grid, daisy_4_RGE[..., 6], label="InertGB, "+daisy_label_4,
                 color='cornflowerblue', ls='dotted', lw=1.)
        plt.plot(phi_grid, daisy_4_RGE[..., 7], label="InertGB, "+daisy_label_4,
                 color='cornflowerblue', ls='dotted', lw=1.)
        plt.plot(phi_grid, daisy_4_RGE[..., 4], label="Inert, "+daisy_label_4,
                 color='blue', ls='dotted', lw=1.)



        plt.title(r"Thermal masses, Inert Doublet, $T = $"+str(current_T)+" GeV")
        plt.legend()
        plt.xlabel(r"Inert $\phi$")
        plt.savefig('plots/'+WHICH_BM+'_M_th_Inert_T_'+str(current_T)+'.png')
        plt.close()

        print("Chi plot:")
        fig = plt.figure(dpi=300, figsize=[8., 8.])
        axis = fig.add_subplot(111)
        plt.xlim([phi_grid[0], phi_grid[-1]])
        #'lightgreen'

        plt.plot(phi_grid, daisy_1_RGE[..., 8], label="Singlets, "+daisy_label_1,
                 lw=3., color='lightgreen', ls='dashed')

        plt.plot(phi_grid, daisy_2_RGE[..., 8], label="Singlets, "+daisy_label_2,
                 color='lightgreen', ls='dashdot', lw=2.)

        plt.plot(phi_grid, daisy_4_RGE[..., 8], label="Singlets, "+daisy_label_4,
                 color='lightgreen', ls='dotted', lw=1.)

        plt.title(r"Thermal masses, Chi singlets, $T = $"+str(current_T)+" GeV")
        plt.legend()
        plt.xlabel(r"Inert $\phi$")
        plt.savefig('plots/'+WHICH_BM+'_M_th_chi_T_'+str(current_T)+'.png')
        plt.close()


if PLOT_THERMAL_ZOOM:
    print("* * * * * plotting thermal masses in zoomed region * * * * *")

    plt.rc('text', usetex=True)
    plt.rc('text.latex', preamble=r'\usepackage{amsmath,amssymb}')
    plt.rc('font', family='serif')
    plt.rc('font', size=12)

    model.use_RGE = True
    plot_min = 0.
    plot_num_pts = 45
    #T = np.array([10., 50., 100., 200., 500., 1000.])
    T = np.array([5000., 1e4])

    daisy_label_1 = helper.get_daisy_label('1')
    daisy_label_2 = helper.get_daisy_label('2')
    #daisy_label_3 = helper.get_daisy_label('3')
    daisy_label_4 = helper.get_daisy_label('4')

    for current_T in T:
        plot_max = 5.*current_T
        phi_grid = np.linspace(plot_min, plot_max, plot_num_pts)
        field = np.concatenate([[np.zeros_like(phi_grid), phi_grid],
                                model.N * [np.zeros_like(phi_grid)]]).T
        #raise NotImplementedError("new T-dep. ren_scale not implemented yet!")
        ren_scale = model.current_scale(field, current_T)
        field = helper.convert_to_full(field)
        print("Done computing renormalization scale for field configuration of T = {}".format(current_T))

        #helper.load_delta_dics(model)
        model.use_Daisy = 1.
        daisy_1_RGE = model.delta_m_daisy(field, current_T, scale=ren_scale)
        print("Done with Daisy {} at T={}".format(1, current_T))
        model.use_Daisy = 2.
        daisy_2_RGE = model.delta_m_daisy(field, current_T, scale=ren_scale)
        print("Done with Daisy {} at T={}".format(2, current_T))
        #model.use_Daisy = 3.
        #daisy_3_RGE = model.delta_m_daisy(field, current_T, scale=ren_scale)
        #print("Done with Daisy {} at T={}".format(3, current_T))
        model.use_Daisy = 4.
        daisy_4_RGE = model.delta_m_daisy(field, current_T, scale=ren_scale)
        print("Done with Daisy {} at T={}".format(4, current_T))
        #helper.save_delta_dics(model)

        print("Higgs plot:")
        fig = plt.figure(dpi=300, figsize=[8., 8.])
        axis = fig.add_subplot(111)
        plt.xlim([phi_grid[0], phi_grid[-1]])

        plt.plot(phi_grid, daisy_1_RGE[..., 0], label="Higgs and GB, "+daisy_label_1,
                 lw=3., color='lightcoral', ls='dashed')

        plt.plot(phi_grid, daisy_2_RGE[..., 1], label="HiggsGB, "+daisy_label_2,
                 color='lightcoral', ls='dashdot', lw=2.)
        plt.plot(phi_grid, daisy_2_RGE[..., 2], label="HiggsGB, "+daisy_label_2,
                 color='lightcoral', ls='dashdot', lw=2.)
        plt.plot(phi_grid, daisy_2_RGE[..., 3], label="HiggsGB, "+daisy_label_2,
                 color='lightcoral', ls='dashdot', lw=2.)
        plt.plot(phi_grid, daisy_2_RGE[..., 0], label="Higgs, "+daisy_label_2,
                 color='red', ls='dashdot', lw=2.)

        plt.plot(phi_grid, daisy_4_RGE[..., 1], label="HiggsGB, "+daisy_label_4,
                 color='lightcoral', ls='dotted', lw=1.)
        plt.plot(phi_grid, daisy_4_RGE[..., 2], label="HiggsGB, "+daisy_label_4,
                 color='lightcoral', ls='dotted', lw=1.)
        plt.plot(phi_grid, daisy_4_RGE[..., 3], label="HiggsGB, "+daisy_label_4,
                 color='lightcoral', ls='dotted', lw=1.)
        plt.plot(phi_grid, daisy_4_RGE[..., 0], label="Higgs, "+daisy_label_4,
                 color='red', ls='dotted', lw=1.)

        plt.title(r"Thermal masses, Higgs Doublet, $T = $"+str(current_T)+" GeV")
        plt.legend()
        plt.xlabel(r"Inert $\phi$")
        plt.savefig('plots/'+WHICH_BM+'_M_th_Higgs_T_'+str(current_T)+'_zoom.png')
        plt.close()

        print("Inert plot:")
        fig = plt.figure(dpi=300, figsize=[8., 8.])
        axis = fig.add_subplot(111)
        plt.xlim([phi_grid[0], phi_grid[-1]])

        plt.plot(phi_grid, daisy_1_RGE[..., 4], label="Inert and GB, "+daisy_label_1,
                 lw=3., color='cornflowerblue', ls='dashed')

        plt.plot(phi_grid, daisy_2_RGE[..., 5], label="InertGB, "+daisy_label_2,
                 color='cornflowerblue', ls='dashdot', lw=2.)
        plt.plot(phi_grid, daisy_2_RGE[..., 6], label="InertGB, "+daisy_label_2,
                 color='cornflowerblue', ls='dashdot', lw=2.)
        plt.plot(phi_grid, daisy_2_RGE[..., 7], label="InertGB, "+daisy_label_2,
                 color='cornflowerblue', ls='dashdot', lw=2.)
        plt.plot(phi_grid, daisy_2_RGE[..., 4], label="Inert, "+daisy_label_2,
                 color='blue', ls='dashdot', lw=2.)

        plt.plot(phi_grid, daisy_4_RGE[..., 5], label="InertGB, "+daisy_label_4,
                 color='cornflowerblue', ls='dotted', lw=1.)
        plt.plot(phi_grid, daisy_4_RGE[..., 6], label="InertGB, "+daisy_label_4,
                 color='cornflowerblue', ls='dotted', lw=1.)
        plt.plot(phi_grid, daisy_4_RGE[..., 7], label="InertGB, "+daisy_label_4,
                 color='cornflowerblue', ls='dotted', lw=1.)
        plt.plot(phi_grid, daisy_4_RGE[..., 4], label="Inert, "+daisy_label_4,
                 color='blue', ls='dotted', lw=1.)

        plt.title(r"Thermal masses, Inert Doublet, $T = $"+str(current_T)+" GeV")
        plt.legend()
        plt.xlabel(r"Inert $\phi$")
        plt.savefig('plots/'+WHICH_BM+'_M_th_Inert_T_'+str(current_T)+'_zoom.png')
        plt.close()

        print("Chi plot:")
        fig = plt.figure(dpi=300, figsize=[8., 8.])
        axis = fig.add_subplot(111)
        plt.xlim([phi_grid[0], phi_grid[-1]])

        plt.plot(phi_grid, daisy_1_RGE[..., 8], label="Singlets, "+daisy_label_1,
                 lw=3., color='lightgreen', ls='dashed')

        plt.plot(phi_grid, daisy_2_RGE[..., 8], label="Singlets, "+daisy_label_2,
                 color='lightgreen', ls='dashdot', lw=2.)

        plt.plot(phi_grid, daisy_4_RGE[..., 8], label="Singlets, "+daisy_label_4,
                 color='lightgreen', ls='dotted', lw=1.)

        plt.title(r"Thermal masses, Chi singlets, $T = $"+str(current_T)+" GeV")
        plt.legend()
        plt.xlabel(r"Inert $\phi$")
        plt.savefig('plots/'+WHICH_BM+'_M_th_chi_T_'+str(current_T)+'_zoom.png')
        plt.close()

if CHECK_MIN_TZERO:
    print("* * * * * finding minima at T=0 * * * * *")

    helper.load_delta_dics(model)
    LIST_OF_APPROX_MINIMA = model.approxZeroTMin()
    Vmin = 1e99
    def short_Vtot(x, run_scale):
        """ wrapper for Vtot"""
        if (np.abs(x) > 1e8).any():
            return -1e99
        x_long = np.concatenate([[x[0]], [x[1]], model.N*[x[2]]])
        #ret = model.V0(x_long)
        ret = model.Vtot(np.abs(x_long), 0., run_scale=run_scale)
        #print("x is {}\nVtot is {}".format(x, ret))
        return ret
    for entry in LIST_OF_APPROX_MINIMA:
        # first optimize without changing renscale (fast), then correct for it
        current_sol = optimize.fmin(short_Vtot, entry[:3], args=(False,),
                                    ftol=1e0, xtol=1e0, disp=False)
        current_sol = optimize.fmin(short_Vtot, current_sol[:3], args=(True,),
                                    ftol=1e0, xtol=1e0, disp=False)
        current_val = short_Vtot(current_sol, True)
        if current_val < Vmin:
            Vmin = current_val
            xmin = current_sol
        print("Solution {} found at {} with guess {}".format(current_val, current_sol, entry))

    print("Global minimum at {} with value {}".format(xmin, Vmin))
    helper.save_delta_dics(model)
    #print(model.store_delta_m_3)

if TRACE_MINIMA_GLOBAL:
    print("* * * * * tracing global minima * * * * *")

    T_start = 0.
    T_end = 1e5
    T_interval = 'log_final' # 'log'

    helper.trace_global_min(model, T_start, T_end, T_interval)
    for i in range(5):
        current_dic = eval('model.minima_dict_wd'+str(i))
        print('model.minima_dict_wd'+str(i))
        print(current_dic)

if TRACE_MINIMA_LOCAL:
    print("* * * * * tracing local minima * * * * *")
    T_start = 1e3
    T_end = 1e3
    T_interval = 100.
    field_value = np.array([0., 2.1e3, 0.])
    helper.trace_local_min(model, T_start, T_end, T_interval, field_value, 1)
    print(model.minima_dict)

if DENOISE_MINIMA:
    print("* * * * * denoising minima * * * * *")

    T_start = 1e3
    T_end = 1e3
    T_interval = 1e3
    for which in ['2']: #which_to_plot:
        model.use_Daisy = float(which)
        helper.denoise_local_min(model, T_start, T_end, T_interval,
                                 field_direction=1, overwrite=False, resolution=.25)
if PLOT_TRACED_MINIMA:
    print("* * * * * plotting traced minima * * * * *")

    # helper.load_delta_dics(model)
    helper.load_minima_dic(model)

    plt.rc('text', usetex=True)
    plt.rc('text.latex', preamble=r'\usepackage{amsmath,amssymb}')
    plt.rc('font', family='serif')
    plt.rc('font', size=12)

    fig = plt.figure(dpi=150, figsize=[6., 6.])
    axis = fig.add_subplot(111)
    x_max = 1e5
    plt.xlim([0., x_max])
    plt.xscale('symlog', linthreshx=500., linscalex=1.5)
    plt.yscale('symlog', linthreshy=500., linscaley=1.5)

    ticks_x, labels_x = helper.get_ticks_labels(x_max)
    ticks_y, labels_y = helper.get_ticks_labels(1e7)


    plt.xticks(ticks=ticks_x, labels=labels_x, rotation=45)
    plt.yticks(ticks=ticks_y, labels=labels_y)

    ls_cycler = cycler(ls=['solid', 'dashed', 'dashdot', 'dotted'])
    #ms_cycler = cycler(marker=['o', 'x', '.'])
    ms_cycler = cycler(marker=['None', 'None', 'None'])
    cl_cycler = cycler(color = ['lightgreen', 'lightcoral', 'cornflowerblue',
                                'green', 'red', 'blue'])
    my_cycler = (ls_cycler * ms_cycler) + 2*cl_cycler
    axis.set_prop_cycle(my_cycler)
    for which in which_to_plot:
        minima = model.__dict__[f'minima_dict_wd{which}']
        T_array = []
        minima_array = []
        for entry in minima:
            T_array.append(entry)
            minima_array.append(minima[entry])
        T_array = np.array(T_array, dtype=np.float64)
        minima_array = np.array(minima_array)

        # wfr:
        if USE_WFR:
            minima_array_ext = np.zeros((len(minima_array), 2+model.N))
            minima_array_ext[..., 0] = minima_array[..., 0]
            minima_array_ext[..., 1] = minima_array[..., 1]
            minima_array_ext[..., 2:] = np.ones((1, model.N)) * minima_array[..., 2, np.newaxis]
            if which in computed_scales:
                scales = computed_scales[which]
                print("recycled renormalization scales")
            else:
                scales = model.current_scale(minima_array_ext, T_array)
                computed_scales[which] = scales
            print("Applying wave function renormalization")
            wfr_higgs = model.wfr_higgs(scales)
            wfr_phi = model.wfr_phi(scales)
            summary = "{} correction is between {} and {} for Daisy = {}"
            print(summary.format('Higgs', wfr_higgs.min(), wfr_higgs.max(), which))
            print(summary.format('Inert', wfr_phi.min(), wfr_phi.max(), which))

            minima_array[..., 0] *= wfr_higgs
            minima_array[..., 1] *= wfr_phi

        #print(T_array, minima_array)
        T_array = T_array[T_array <= x_max]
        sorted_indices = np.argsort(T_array)

        daisy_label = helper.get_daisy_label(which)

        plt.plot(T_array[sorted_indices], np.abs(minima_array[..., 2])[sorted_indices], #'o',
                 label='Singlet, '+daisy_label, lw=1.25)
        plt.plot(T_array[sorted_indices], np.abs(minima_array[..., 0])[sorted_indices], #'x',
                 label='Higgs, '+daisy_label, lw=1.25)
        plt.plot(T_array[sorted_indices], np.abs(minima_array[..., 1])[sorted_indices], #'.',
                 label='Inert, '+daisy_label, lw=1.25)

    plt.plot([0., 500., 500.], [500., 500., 0.], lw=0.5, ls='dashed', color='k', marker='None')
    plt.title('Phase Structure of '+WHICH_BM+' BM')
    plt.legend(loc='upper left')
    plt.xlabel(r"$T$ [GeV]")
    plt.ylabel(r"Field value at Minimum")
    plt.tight_layout()
    plt.savefig('plots/'+WHICH_BM+'_BM_phase_structure.png')
    plt.close()
    print("plotting done")

if PRINT_PHYSICAL_MASSES:
    print("* * * * * finding physical masses  * * * * *")
    print("ASSUMING MINIMA IS EW FOR NOW!")

    x_vac = np.concatenate([[246.], [0.], model.N*[0.]])
    def Vtot_of_field(field):
        return model.Vtot(field, 0., run_scale=False)
    m_higgs2 = part_der(Vtot_of_field, x_vac, 0, dx=1e-2, n=2, order=9)
    m_inert2 = part_der(Vtot_of_field, x_vac, 1, dx=1e-2, n=2, order=9)
    m_chi2 = part_der(Vtot_of_field, x_vac, 2, dx=1e-2, n=2, order=9)
    print_string = "Mass of {} is sqrt(|{}|) = {}"
    print(print_string.format("Higgs", m_higgs2, np.sqrt(np.abs(m_higgs2))))
    print(print_string.format("Inert", m_inert2, np.sqrt(np.abs(m_inert2))))
    print(print_string.format("Singlet", m_chi2, np.sqrt(np.abs(m_chi2))))

if PLOT_BFB:
    print("* * * * * plotting bounded from below region * * * * *")

    print("This is old and not used anymore")

    ctN_min = -9.
    ctN_max = 0.
    a2_min = -0.25
    a2_max = 0.2

    num_grid = 81
    min_Vtot_factor = 4

    def make_model(a2_input, ctN_input, use_CW=0., use_RGE=True):
        """ returns model with given a2 and ct for BFB scan """
        num_chis = 40
        local_parameters = {'b2': 10000., 'b4': 0.126, 'c2': 10.**2, 'c4': 0.,
                            'a2': a2_input, 'ct': ctN_input/num_chis, 'dt': 0., 'at': 0., 'c5': 3./num_chis,
                            'N': num_chis, 'use_CW': use_CW, 'use_Daisy': 0.,
                            'use_RGE': use_RGE, 'use_CW_in_delta_m':False}

        model_ret = model_2HDM_N(**local_parameters)
        return model_ret

    ctN_scan = np.linspace(ctN_min, ctN_max, num_grid)
    a2_scan = np.linspace(a2_min, a2_max, num_grid)

    BFB_grid_246 = np.zeros((num_grid,num_grid))
    BFB_grid_4000 = np.zeros((num_grid,num_grid))
    color_grid = [['k' for _ in range(num_grid)] for _ in range(num_grid)]
    color_grid = np.array(color_grid)

    for a2_index, a2_loop in enumerate(a2_scan):
        for ct_index, ctN_loop in enumerate(ctN_scan):
            model = make_model(a2_loop, ctN_loop, use_CW=0., use_RGE=True)
            if model.BFB(scale=246.):
                BFB_grid_246[a2_index, ct_index] = 1.
            if model.BFB(scale=4e3):
                BFB_grid_4000[a2_index, ct_index] = 1.
            model = make_model(a2_loop, ctN_loop, use_CW=1., use_RGE=True) #False
            if a2_index % min_Vtot_factor == 0 and ct_index %min_Vtot_factor == 0:
                helper.trace_global_min(model, 0., 0., 1.)
                minimum = model.minima_dict_wd0[0]
                minimum = np.around(minimum, 0)
                if minimum[0] == 246. and minimum[1] == 0. and minimum[2] == 0.:
                    color_grid[a2_index, ct_index] = 'green'
                else:
                    color_grid[a2_index, ct_index] = 'red'

        print("{:.2f} % done".format(100.*(a2_index+1.)/len(a2_scan)))

    plt.figure(dpi=150,figsize=[5.,4.])
    plt.xlim(ctN_min, ctN_max)
    plt.ylim(a2_min, a2_max)

    nl='\n\n\n\n\n\n'
    cont_BFB=plt.contourf(ctN_scan,a2_scan,BFB_grid_246,np.array([-.5,0.5,1.5]),
                          colors=['white', 'green'], alpha=0.5)
    cont_BFB_high=plt.contourf(ctN_scan,a2_scan,BFB_grid_4000,np.array([-.5,0.5,1.5]),
                               colors=['white', 'green'], alpha=0.5)
    cbar_BFB=plt.colorbar(cont_BFB)
    cbar_BFB.ax.set_yticklabels(['not bounded'+nl+nl, 'bounded'+nl+nl])
    plt.text(-6, 0., r'$\mu_R = 246$ GeV')
    plt.text(-8.75, -0.15, r'$\mu_R = 4$ TeV')

    for a2_index in range(num_grid):
        for ct_index in range(num_grid):
            if a2_index % min_Vtot_factor == 0 and ct_index %min_Vtot_factor == 0:
                plt.plot(ctN_scan[ct_index], a2_scan[a2_index], 'x',
                         color=color_grid[a2_index, ct_index])
    plt.savefig('plots/BFB_2.png',bbox_inches='tight')
    #plt.show()

if PLOT_POTENTIAL:
    print("* * * * * plotting potential * * * * *")

    #helper.load_delta_dics(model)
    helper.load_minima_dic(model)

    x_max = 8e3
    fig = plt.figure(dpi=150, figsize=[6., 6.])
    axis = fig.add_subplot(111)
    plt.xscale('symlog', linthreshx=500.)
    ticks = np.concatenate([np.arange(0., 1000., 100), np.arange(1000., 6000., 1000.)])
    labels = list(ticks.copy())
    labels[-2] = ''
    labels[-3] = ''
    labels[6] = ''
    labels[7] = ''
    labels[8] = ''
    labels[9] = ''
    plt.xticks(ticks=ticks, labels=labels)

    plot_min = 4e3
    plot_max = x_max
    plot_num_pts = 201
    TEMP = 2e3
    phi_grid = np.linspace(plot_min, plot_max, plot_num_pts)
    field = np.concatenate([[np.zeros_like(phi_grid), phi_grid],
                            num_chi * [np.zeros_like(phi_grid)]]).T

    ls_cycler = cycler(ls=['solid', 'dashed', 'dashdot', 'dotted'])
    cl_cycler = cycler(color=['green', 'red', 'blue', 'black'])
    my_cycler = ls_cycler + cl_cycler
    axis.set_prop_cycle(my_cycler)
    for which in which_to_plot:
        model.use_Daisy = float(which)
        potential = model.Vtot(field, TEMP, run_scale=model.use_RGE)
        print("Plotting potential for Daisy= {}".format(model.use_Daisy))
        plt.plot(phi_grid, potential, label='Daisy = '+which, lw=1.5)

    plt.plot(phi_grid, model.V0(field), label='V0 no RGE')
    raise NotImplementedError("new T-dep. ren_scale not implemented yet!")
    plt.plot(phi_grid, model.V0(field, scale=model.current_scale(field)), label='V0 + RGE')
    #helper.save_delta_dics(model)
    plt.title(helper.make_param_string(model_parameters))
    plt.legend(loc='upper left')
    plt.xlabel(r"$\phi$ [GeV]")
    plt.ylabel(r"$V_{tot}$")
    plt.xlim([plot_min, plot_max])
    plt.ylim([-4.75e14, -4.5e14])

    plt.savefig('plots/Vtot_'+str(TEMP)+'.png')
    plt.close()
    print("plotting done")

if SCAN_GLOBAL:
    print("* * * * * scanning potential globally * * * * *")

    helper.load_delta_dics(model)
    helper.load_minima_dic(model)

    scan_min = 0.
    scan_max = 500.
    scan_num_pts_per_dim = 10
    #TEMPERATURE = np.array([0., 100., 200., 300., 400., 500.])
    TEMPERATURE = np.array([200.])

    field_grid_1dim = np.linspace(scan_min, scan_max, scan_num_pts_per_dim)

    field_grid_3dim = np.array(np.meshgrid(field_grid_1dim,
                                           field_grid_1dim,
                                           field_grid_1dim,
                                           indexing='ij')).T.reshape(-1, 3)
    field_grid_Ndim = np.zeros((field_grid_3dim.shape[0], model.N+2))
    field_grid_Ndim[..., :3] = field_grid_3dim
    field_grid_Ndim[..., 2:] = np.ones((1, model.N)) * field_grid_Ndim[..., 2, np.newaxis]

    for TEMP in TEMPERATURE:
        print("evaluating T = {} GeV".format(TEMP))
        potential = model.Vtot(field_grid_Ndim, TEMP, run_scale=model.use_RGE)
        print("Minimum of T = {} GeV at {}".format(TEMP, field_grid_Ndim[np.argmin(potential)][:3]))
        helper.save_delta_dics(model)

if SCAN_GLOBAL_2DIM:
    print("* * * * * scanning potential globally in Higgs and Inert Dimension* * * * *")

    # helper.load_delta_dics(model)
    helper.load_minima_dic(model)

    scan_min = 0.
    scan_max = 500.
    scan_num_pts_per_dim = 11
    #TEMPERATURE = np.array([0., 100., 200., 300., 400., 500.])
    TEMPERATURE = np.array([200.])

    Singlet_value = 0.

    field_grid_1dim = np.linspace(scan_min, scan_max, scan_num_pts_per_dim)

    field_grid_2dim = np.array(np.meshgrid(field_grid_1dim,
                                           field_grid_1dim,
                                           indexing='ij')).T.reshape(-1, 2)
    field_grid_Ndim = Singlet_value * np.ones((field_grid_2dim.shape[0], model.N+2))
    field_grid_Ndim[..., :2] = field_grid_2dim

    for TEMP in TEMPERATURE:
        print("evaluating T = {} GeV".format(TEMP))
        potential = model.Vtot(field_grid_Ndim, TEMP, run_scale=model.use_RGE)
        print("Minimum of T = {} GeV at {}".format(TEMP, field_grid_Ndim[np.argmin(potential)][:3]))
        #helper.save_delta_dics(model)

if PLOT_V_T:
    print("* * * * * plotting v/T * * * * *")

    # helper.load_delta_dics(model)
    helper.load_minima_dic(model)

    plt.rc('text', usetex=True)
    plt.rc('text.latex', preamble=r'\usepackage{amsmath,amssymb}')
    plt.rc('font', family='serif')
    plt.rc('font', size=12)

    fig = plt.figure(dpi=150, figsize=[6., 6.])
    axis = fig.add_subplot(111)
    x_max = 1e5
    plt.xlim([0., x_max])
    plt.ylim([0., 5.])
    plt.xscale('symlog', linthreshx=500., linscalex=1.5)

    ticks, labels = helper.get_ticks_labels(x_max)

    plt.xticks(ticks=ticks, labels=labels, rotation=45)

    ls_cycler = cycler(ls=['solid', 'dashed', 'dashdot', 'dotted'])
    cl_cycler = cycler(color=['lightgreen', 'lightcoral', 'cornflowerblue',
                              'green'])
    my_cycler = ls_cycler + cl_cycler
    axis.set_prop_cycle(my_cycler)
    for which in which_to_plot:
        minima = model.__dict__[f'minima_dict_wd{which}']
        T_array = []
        minima_array = []
        for entry in minima:
            T_array.append(entry)
            minima_array.append(minima[entry])
        T_array = np.array(T_array, dtype=np.float64)
        minima_array = np.array(minima_array)

        # wfr:
        if USE_WFR:
            minima_array_ext = np.zeros((len(minima_array), 2+model.N))
            minima_array_ext[..., 0] = minima_array[..., 0]
            minima_array_ext[..., 1] = minima_array[..., 1]
            minima_array_ext[..., 2:] = np.ones((1, model.N)) * minima_array[..., 2, np.newaxis]
            if which in computed_scales:
                scales = computed_scales[which]
                print("recycled renormalization scales")
            else:
                scales = model.current_scale(minima_array_ext, T_array)
                computed_scales[which] = scales
            print("Applying wave function renormalization")
            wfr_higgs = model.wfr_higgs(scales)
            wfr_phi = model.wfr_phi(scales)
            summary = "{} correction is between {} and {} for Daisy = {}"
            print(summary.format('Higgs', wfr_higgs.min(), wfr_higgs.max(), which))
            print(summary.format('Inert', wfr_phi.min(), wfr_phi.max(), which))

            minima_array[..., 0] *= wfr_higgs
            minima_array[..., 1] *= wfr_phi


        v_eff = np.sqrt(minima_array[..., 0]**2 + minima_array[..., 1]**2)
        #print(T_array, minima_array)
        sorted_indices = np.argsort(T_array)

        daisy_label = helper.get_daisy_label(which)

        if T_array[sorted_indices[0]] == 0.:
            plt.plot(T_array[sorted_indices][1:],
                     v_eff[sorted_indices][1:]/T_array[sorted_indices][1:],
                     label=daisy_label, lw=1.5)
        else:
            plt.plot(T_array[sorted_indices], v_eff[sorted_indices]/T_array[sorted_indices],
                     label='Daisy = '+which, lw=1.5)
        #print(which, T_array[sorted_indices], v_eff[sorted_indices]/T_array[sorted_indices])

    plt.plot([0., x_max], [1., 1.], lw=0.5, ls='dashed', color='r', marker='None')
    plt.plot([500., 500.], [0., 5.], lw=0.5, ls='dashed', color='k', marker='None')

    plt.title(r'$v_T/T$ of '+WHICH_BM+' BM')
    plt.legend(loc='upper right')
    plt.xlabel(r"$T$ [GeV]")
    plt.ylabel(r"$v_T / T$")
    plt.tight_layout()
    plt.savefig('plots/'+WHICH_BM+'_BM_v_over_T.png')
    plt.close()
    print("plotting done")

if PLOT_THERMAL_OF_MINIMA:
    print("* * * * * plotting m_thermal at minima * * * * *")

    # helper.load_delta_dics(model) # too slow for large scans
    helper.load_minima_dic(model)

    plt.rc('text', usetex=True)
    plt.rc('text.latex', preamble=r'\usepackage{amsmath,amssymb}')
    plt.rc('font', family='serif')
    plt.rc('font', size=12)

    x_max = 1e5
    ticks_x, labels_x = helper.get_ticks_labels(x_max)

    for which in which_to_plot:
        minima = model.__dict__[f'minima_dict_wd{which}']
        T_array = []
        minima_array = []
        for entry in minima:
            T_array.append(entry)
            minima_array.append(minima[entry])
        T_array = np.array(T_array, dtype=np.float64)
        minima_array = np.array(minima_array)

        minima_array_Ndim = np.zeros((minima_array.shape[0], model.N+2))
        minima_array_Ndim[..., :3] = minima_array
        minima_array_Ndim[..., 2:] = np.ones((1, model.N)) * minima_array_Ndim[..., 2, np.newaxis]

        if model.use_RGE:
            if which in computed_scales:
                ren_scale = computed_scales[which]
                print("recycled renormalization scales")
            else:
                print("finding renormalization scale")
                ren_scale = model.current_scale(minima_array_Ndim, T_array)
                computed_scales[which] = ren_scale
        else:
            ren_scale = 246.
        minima_array_Ndim_full = helper.convert_to_full(minima_array_Ndim)
        model.use_Daisy = float(which)
        thermal_mass = []
        for index, temp in enumerate(T_array):
            thermal_mass.append(model.delta_m_daisy(np.abs(minima_array_Ndim_full[index]),
                                                    temp, scale=ren_scale[index]))
        m_thermal_store[which] = np.array(thermal_mass)
        T_array_store[which] = T_array

    print("Higgs plot:")

    fig = plt.figure(dpi=150, figsize=[6., 6.])
    axis = fig.add_subplot(111)
    plt.xlim([0., x_max])
    plt.xscale('symlog', linthreshx=500., linscalex=1.5)
    plt.yscale('symlog', linthreshy=500., linscaley=2.5)

    plt.xticks(ticks=ticks_x, labels=labels_x, rotation=45)
    ticks_y = np.arange(0., 600., 100.)
    labels_y = ["%.0f" % x for x in ticks_y.copy()]

    new_ticks = np.logspace(3, 2*np.log10(x_max), 2*np.log10(x_max)-2)
    new_label = list(new_ticks.copy())
    for i, val in enumerate(new_label):
        new_label[i] = '{:1.0e}'.format(val)
    ticks_y = np.append(ticks_y, new_ticks)
    labels_y.extend(new_label)
    plt.yticks(ticks=ticks_y, labels=labels_y)

    ls_cycler = cycler(ls=['solid', 'dashed', 'dashdot', 'dotted'])
    cl_cycler = cycler(color = ['lightgreen', 'lightcoral', 'cornflowerblue',
                                'green'])
    lw_cycler = cycler(lw=[1., 1.5, 2., 2.5])
    my_cycler = cl_cycler * (ls_cycler + lw_cycler)
    axis.set_prop_cycle(my_cycler)

    for which in which_to_plot:
        T_array = T_array_store[which]
        thermal_mass = m_thermal_store[which]
        sorted_indices = np.argsort(T_array)

        daisy_label = helper.get_daisy_label(which)

        if np.allclose(thermal_mass[..., 0], thermal_mass[..., 1], rtol=1e-2) and\
           np.allclose(thermal_mass[..., 1], thermal_mass[..., 2], rtol=1e-2) and\
           np.allclose(thermal_mass[..., 2], thermal_mass[..., 3], rtol=1e-2):
            plt.plot(T_array[sorted_indices], thermal_mass[..., 0][sorted_indices],
                     label='Higgs and GBs, '+daisy_label)
            plt.plot([], [])
            plt.plot([], [])
            plt.plot([], [])
        elif not np.allclose(thermal_mass[..., 0], thermal_mass[..., 1], rtol=1e-2) and\
             np.allclose(thermal_mass[..., 1], thermal_mass[..., 2], rtol=1e-2) and\
             np.allclose(thermal_mass[..., 2], thermal_mass[..., 3], rtol=1e-2):
            plt.plot(T_array[sorted_indices], thermal_mass[..., 0][sorted_indices],
                     label='Higgs, '+daisy_label)
            plt.plot(T_array[sorted_indices], thermal_mass[..., 1][sorted_indices],
                     label='GBs, '+daisy_label)
            plt.plot([], [])
            plt.plot([], [])
        else:
            plt.plot(T_array[sorted_indices], thermal_mass[..., 0][sorted_indices],
                     label='Higgs, '+daisy_label)
            plt.plot(T_array[sorted_indices], thermal_mass[..., 1][sorted_indices],
                     label='GB1, '+daisy_label)
            plt.plot(T_array[sorted_indices], thermal_mass[..., 2][sorted_indices],
                     label='GB2, '+daisy_label)
            plt.plot(T_array[sorted_indices], thermal_mass[..., 3][sorted_indices],
                     label='GB3, '+daisy_label)

    plt.plot([0., 500., 500.], [500., 500., 0.], lw=0.5, ls='dashed', color='k', marker='None')
    plt.title('Square of Thermal Mass of Higgs Doublet in '+WHICH_BM+' BM')
    plt.legend(loc='upper left')
    plt.xlabel(r"$T$ [GeV]")
    plt.tight_layout()
    plt.savefig('plots/'+WHICH_BM+'_BM_M_th_Higgs_minima.png')
    plt.close()

    print("Inert plot:")
    fig = plt.figure(dpi=150, figsize=[6., 6.])
    axis = fig.add_subplot(111)
    plt.xlim([0., x_max])
    plt.ylim([-x_max**2, 500.])

    plt.xscale('symlog', linthreshx=500., linscalex=1.5)
    plt.yscale('symlog', linthreshy=500., linscaley=2.5)

    plt.xticks(ticks=ticks_x, labels=labels_x, rotation=45)
    ticks_y = np.arange(-500., 600., 100.)
    labels_y = ["%.0f" % x for x in ticks_y.copy()]

    new_ticks = np.logspace(3, 2*np.log10(x_max), 2*np.log10(x_max)-2)
    new_label = list(new_ticks.copy())
    new_label_neg = list(new_ticks.copy())
    for i, val in enumerate(new_label):
        new_label[i] = '{:1.0e}'.format(val)
        new_label_neg[i] = '{:1.0e}'.format(-val)
    ticks_y = np.append(ticks_y, new_ticks)
    labels_y.extend(new_label)

    ticks_y = np.append(ticks_y, -new_ticks)
    labels_y.extend(new_label_neg)
    plt.yticks(ticks=ticks_y, labels=labels_y)

    ls_cycler = cycler(ls=['solid', 'dashed', 'dashdot', 'dotted'])
    cl_cycler = cycler(color=['lightgreen', 'lightcoral', 'cornflowerblue',
                              'green'])
    lw_cycler = cycler(lw=[1., 1.5, 2., 2.5])
    my_cycler = cl_cycler * (ls_cycler + lw_cycler)
    axis.set_prop_cycle(my_cycler)

    for which in which_to_plot:
        T_array = T_array_store[which]
        thermal_mass = m_thermal_store[which]
        sorted_indices = np.argsort(T_array)

        daisy_label = helper.get_daisy_label(which)

        if np.allclose(thermal_mass[..., 4], thermal_mass[..., 5], rtol=1e-2) and\
           np.allclose(thermal_mass[..., 5], thermal_mass[..., 6], rtol=1e-2) and\
           np.allclose(thermal_mass[..., 6], thermal_mass[..., 7], rtol=1e-2):
            plt.plot(T_array[sorted_indices], thermal_mass[..., 4][sorted_indices],
                     label='Inert and GBs, '+daisy_label)
            plt.plot([], [])
            plt.plot([], [])
            plt.plot([], [])
        elif not np.allclose(thermal_mass[..., 4], thermal_mass[..., 5], rtol=1e-2) and\
             np.allclose(thermal_mass[..., 5], thermal_mass[..., 6], rtol=1e-2) and\
             np.allclose(thermal_mass[..., 6], thermal_mass[..., 7], rtol=1e-2):
            plt.plot(T_array[sorted_indices], thermal_mass[..., 4][sorted_indices],
                     label='Inert, '+daisy_label)
            plt.plot(T_array[sorted_indices], thermal_mass[..., 5][sorted_indices],
                     label='GBs, '+daisy_label)
            plt.plot([], [])
            plt.plot([], [])
        else:
            plt.plot(T_array[sorted_indices], thermal_mass[..., 4][sorted_indices],
                     label='Inert, '+daisy_label)
            plt.plot(T_array[sorted_indices], thermal_mass[..., 5][sorted_indices],
                     label='GB1, '+daisy_label)
            plt.plot(T_array[sorted_indices], thermal_mass[..., 6][sorted_indices],
                     label='GB2, '+daisy_label)
            plt.plot(T_array[sorted_indices], thermal_mass[..., 7][sorted_indices],
                     label='GB3, '+daisy_label)


    plt.plot([0., 500., 500., 0.], [500., 500., -500., -500.],
             lw=0.5, ls='dashed', color='k', marker='None')
    plt.title('Square of Thermal Mass of Inert Doublet in '+WHICH_BM+' BM')
    plt.legend(loc='upper right')
    plt.xlabel(r"$T$ [GeV]")
    plt.tight_layout()
    plt.savefig('plots/'+WHICH_BM+'_BM_M_th_Inert_minima.png')
    plt.close()

    print("Chi plot:")
    fig = plt.figure(dpi=150, figsize=[6., 6.])
    axis = fig.add_subplot(111)
    plt.xlim([0., x_max])
    plt.ylim([-20., x_max**2])
    plt.xscale('symlog', linthreshx=500., linscalex=1.5)
    plt.yscale('symlog', linthreshy=500., linscaley=2.5)

    plt.xticks(ticks=ticks_x, labels=labels_x, rotation=45)
    ticks_y = np.arange(0., 600., 100.)
    labels_y = ["%.0f" % x for x in ticks_y.copy()]

    new_ticks = np.logspace(3, 2*np.log10(x_max), 2*np.log10(x_max)-2)
    new_label = list(new_ticks.copy())
    for i, val in enumerate(new_label):
        new_label[i] = '{:1.0e}'.format(val)
    ticks_y = np.append(ticks_y, new_ticks)
    labels_y.extend(new_label)
    plt.yticks(ticks=ticks_y, labels=labels_y)

    cl_cycler = cycler(color=['lightgreen', 'lightcoral', 'cornflowerblue',
                              'green'])
    my_cycler = cl_cycler
    axis.set_prop_cycle(my_cycler)

    for which in which_to_plot:
        T_array = T_array_store[which]
        thermal_mass = m_thermal_store[which]
        sorted_indices = np.argsort(T_array)

        daisy_label = helper.get_daisy_label(which)

        plt.plot(T_array[sorted_indices], thermal_mass[..., 8][sorted_indices],
                 label='Singlets, '+daisy_label)


    plt.plot([0., 500., 500.], [500., 500., 0.], lw=0.5, ls='dashed', color='k', marker='None')
    plt.title('Square of Thermal Mass of Singlet Scalars in '+WHICH_BM+' BM')
    plt.legend(loc='upper left')
    plt.xlabel(r"$T$ [GeV]")
    plt.tight_layout()
    plt.savefig('plots/'+WHICH_BM+'_BM_M_th_Singlet_minima.png')
    plt.close()

if COMPUTE_BARYON_WASHOUT:
    print("* * * Computing washout factor * * *")

    helper.load_minima_dic(model)

    x_max = 1e5 if (WHICH_BM == 'light') else 4e4

    for which in which_to_plot:
        minima = model.__dict__[f'minima_dict_wd{which}']
        T_array = []
        minima_array = []
        for entry in minima:
            T_array.append(entry)
            minima_array.append(minima[entry])
        T_array = np.array(T_array, dtype=np.float64)
        minima_array = np.array(minima_array)

        minima_array_Ndim = np.zeros((minima_array.shape[0], model.N+2))
        minima_array_Ndim[..., :3] = minima_array
        minima_array_Ndim[..., 2:] = np.ones((1, model.N)) * minima_array_Ndim[..., 2, np.newaxis]

        if model.use_RGE:
            if which in computed_scales:
                ren_scale = computed_scales[which]
                print("recycled renormalization scales")
            else:
                print("finding renormalization scale")
                ren_scale = model.current_scale(minima_array_Ndim, T_array)
                computed_scales[which] = ren_scale
        else:
            ren_scale = 246.
        if USE_WFR:
            print("Applying wave function renormalization")
            wfr_higgs = model.wfr_higgs(ren_scale)
            wfr_phi = model.wfr_phi(ren_scale)
            summary = "{} correction is between {} and {} for Daisy = {}"
            print(summary.format('Higgs', wfr_higgs.min(), wfr_higgs.max(), which))
            print(summary.format('Inert', wfr_phi.min(), wfr_phi.max(), which))

            minima_array[..., 0] *= wfr_higgs
            minima_array[..., 1] *= wfr_phi

        which_entries = T_array <= x_max
        my_sphaleron = SphaleronDecayRate(model, minima_array[which_entries],
                                          T_array[which_entries], ren_scale[which_entries])
        output = "Washout is between {} and {} with mean {}"
        lower = 10.
        upper = [5e2, 1e4, 2e4, 3e4, 4e4, 5e4, 1e5]
        for up in upper:
            wsh_out = my_sphaleron.washout_factor_discrete(lower, up)
            print(output.format(wsh_out[2], wsh_out[1], wsh_out[0]))

if DEBUG_STUFF:
    print("* * * some debugging output:  * * *")

    #TEMP = np.array([2990., 3000., 3010.])

    #TEMP = 2990.
    #fields = np.array([[0., 0., 0., 0., 5156., 0., 0., 0., 0.],
    #                   [0., 0., 0., 0., 5156.5, 0., 0., 0., 0.],
    #                   [0., 0., 0., 0., 5157., 0., 0., 0., 0.],
    #                   [0., 0., 0., 0., 5157.5, 0., 0., 0., 0.],
    #                   [0., 0., 0., 0., 5158., 0., 0., 0., 0.],
    #                   [0., 0., 0., 0., 0., 0., 0., 0., 0.]])

    #TEMP = 3010.
    #fields = np.array([[0., 0., 0., 0., 5190., 0., 0., 0., 0.],
    #                   [0., 0., 0., 0., 5190.5, 0., 0., 0., 0.],
    #                   [0., 0., 0., 0., 5191., 0., 0., 0., 0.],
    #                   [0., 0., 0., 0., 5191.5, 0., 0., 0., 0.],
    #                   [0., 0., 0., 0., 5192., 0., 0., 0., 0.],
    #                   [0., 0., 0., 0., 5192.5, 0., 0., 0., 0.],
    #                   [0., 0., 0., 0., 0., 0., 0., 0., 0.]])
    #TEMP = 3000.
    #fields = np.array([[0., 0., 0., 0., 5173.5, 0., 0., 0., 0.],
    #                   [0., 0., 0., 0., 5173.75, 0., 0., 0., 0.],
    #                   [0., 0., 0., 0., 5174., 0., 0., 0., 0.],
    #                   [0., 0., 0., 0., 5174.25, 0., 0., 0., 0.],
    #                   [0., 0., 0., 0., 5174.5, 0., 0., 0., 0.],
    #                   #[0., 0., 0., 0., 0., 0., 0., 0., 0.],
    #                   #[7.58251141e-04, 0., 0., 0., 1.46737189e-03, 0., 0., 0., 6.48621606e-05],
    #])

    #TEMP = 5e4 # 133369 not
    #TEMP = 4.99e4 # 133108
    #TEMP = 5.01e4 # 133630
    #TEMP = 1e5 #264128 not
    #TEMP = 9.999e4
    #fields = np.array([[0., 0., 0., 0., 264100., 0., 0., 0., 0.],
    #                   [0., 0., 0., 0., 264101., 0., 0., 0., 0.],
    #                   [0., 0., 0., 0., 264102., 0., 0., 0., 0.],
    #                   [0., 0., 0., 0., 264103., 0., 0., 0., 0.],
    #                   [0., 0., 0., 0., 264104., 0., 0., 0., 0.],
    #                   [0., 0., 0., 0., 264105., 0., 0., 0., 0.],
    #                   [0., 0., 0., 0., 264106., 0., 0., 0., 0.],
    #                   [0., 0., 0., 0., 264099, 0., 0., 0., 0.],
    #                   [0., 0., 0., 0., 264098., 0., 0., 0., 0.],
    #                   [0., 0., 0., 0., 0., 0., 0., 0., 0.],
    #])
    #TEMP = 6e4
    #fields = np.array([[0., 0., 0., 0., 159405., 0., 0., 0., 0.],
    #                   [0., 0., 0., 0., 159406., 0., 0., 0., 0.],
    #                   [0., 0., 0., 0., 159407., 0., 0., 0., 0.],
    #                   [0., 0., 0., 0., 159408., 0., 0., 0., 0.],
    #                   [0., 0., 0., 0., 159409., 0., 0., 0., 0.],
    #                   [0., 0., 0., 0., 159410., 0., 0., 0., 0.],
    #                   [0., 0., 0., 0., 159411., 0., 0., 0., 0.],
    #                   [0., 0., 0., 0., 159412., 0., 0., 0., 0.],
    #                   [0., 0., 0., 0., 159413., 0., 0., 0., 0.],
    #                   [0., 0., 0., 0., 159414., 0., 0., 0., 0.],
    #                   [0., 0., 0., 0., 159415., 0., 0., 0., 0.],
    #                   [0., 0., 0., 0., 0., 0., 0., 0., 0.],
    #])
    #fields_Ndim = np.zeros((fields.shape[0], model.N+8))
    #fields_Ndim[..., :9] = fields
    #fields_Ndim[..., 8:] = np.ones((1, model.N)) * fields_Ndim[..., 8, np.newaxis]

    #short_fields = helper.convert_to_short(fields_Ndim)
    #scales = model.current_scale(short_fields, TEMP)

    #my_Vtot = model.Vtot(short_fields, TEMP, run_scale=model.use_RGE)
    #print("Vtot", my_Vtot)
    #print("field at min", fields[np.argmin(my_Vtot)])
    #print("Diff to min", my_Vtot - my_Vtot[np.argmin(my_Vtot)])

    #phi = np.array([4029.41176471, 4058.82352941, 4088.23529412, 4117.64705882,
    #                4147.05882353, 4176.47058824, 4205.88235294, 4235.29411765,
    #                4264.70588235, 4294.11764706, 4323.52941176, 4352.94117647,
    #                4382.35294118, 4411.76470588, 4441.17647059, 4470.58823529,
    #                4500.        , 4529.41176471, 4558.82352941, 4588.23529412,
    #                4617.64705882, 4647.05882353, 4676.47058824, 4705.88235294,
    #                4735.29411765, 4764.70588235, 4794.11764706, 4823.52941176,
    #                4852.94117647, 4882.35294118, 4911.76470588, 4941.17647059,
    #                4970.58823529, 5000.        , 5029.41176471, 5058.82352941,
    #                5088.23529412, 5117.64705882, 5147.05882353, 5176.47058824,
    #                5205.88235294, 5235.29411765, 5264.70588235, 5294.11764706,
    #                5323.52941176, 5352.94117647, 5382.35294118, 5411.76470588,
    #                5441.17647059, 5470.58823529])
    #fields_Ndim = np.zeros((phi.shape[0], model.N+2))
    #fields_Ndim[..., 1] = phi
    #print(fields_Ndim)
    #scales = model.current_scale(fields_Ndim)

    #for i in range(len(phi)):
    #    print(phi[i], scales[i])
    #
    raise

    deltaM = model.daisy_with_dict(model.Improved_Daisy, {}, fields_Ndim,
                                   TEMP, scale=scales, improved=False)
    print("scales", scales)
    print("b4", model.cp_b4(scales))
    print("ct", model.cp_ct(scales))
    print("a2", model.cp_a2(scales))
    print("b2", model.cp_b2(scales))
    print("mu2", model.cp_mu2(scales))
    print("lambda", model.cp_lbda(scales))
    print("deltaM", deltaM[..., :10])
    print("Vtot", model.Vtot(short_fields, TEMP, run_scale=model.use_RGE))
    bosons = model.boson_massSq(short_fields, TEMP, scale=scales, deltaM=deltaM)
    print("bosons", bosons)
    fermions = model.fermion_massSq(short_fields, scale=scales)
    print("fermions", fermions)
    print("V0", model.V0(short_fields, scale=scales))
    print("V1", model.V1(bosons, fermions, scale=scales))
    print("V1T", model.V1T(bosons, fermions, TEMP, True))

if CHECK_HIGH_T:
    print("* * * * * plotting cross checks for high-T approximation * * * * *")

    which = '1'

    # helper.load_delta_dics(model)
    helper.load_minima_dic(model)

    plt.rc('text', usetex=True)
    plt.rc('text.latex', preamble=r'\usepackage{amsmath,amssymb}')
    plt.rc('font', family='serif')
    plt.rc('font', size=12)

    fig = plt.figure(dpi=150, figsize=[6., 6.])
    axis = fig.add_subplot(111)
    x_max = 1e5
    plt.xlim([0., x_max])
    plt.ylim([-3., 3.])
    plt.xscale('symlog', linthreshx=500., linscalex=1.5)

    ticks, labels = helper.get_ticks_labels(x_max)

    plt.xticks(ticks=ticks, labels=labels, rotation=45)

    ls_cycler = cycler(ls=['solid', 'dashed', 'dashdot', 'dotted'])
    cl_cycler = cycler(color=['lightgreen', 'lightcoral', 'cornflowerblue',
                              'green'])
    my_cycler = ls_cycler + cl_cycler
    axis.set_prop_cycle(my_cycler)

    minima = model.__dict__[f'minima_dict_wd{which}']

    T_array = []
    minima_array = []
    for entry in minima:
        T_array.append(entry)
        minima_array.append(minima[entry])
    T_array = np.array(T_array, dtype=np.float64)
    minima_array = np.array(minima_array)

    minima_array_ext = np.zeros((len(minima_array), 2+model.N))
    minima_array_ext[..., 0] = minima_array[..., 0]
    minima_array_ext[..., 1] = minima_array[..., 1]
    minima_array_ext[..., 2:] = np.ones((1, model.N)) * minima_array[..., 2, np.newaxis]

    # wfr:
    if USE_WFR:
        if which in computed_scales:
            scales = computed_scales[which]
            print("recycled renormalization scales")
        else:
            scales = model.current_scale(minima_array_ext, T_array)
            computed_scales[which] = scales
        print("Applying wave function renormalization")
        wfr_higgs = model.wfr_higgs(scales)
        wfr_phi = model.wfr_phi(scales)
        summary = "{} correction is between {} and {} for Daisy = {}"
        print(summary.format('Higgs', wfr_higgs.min(), wfr_higgs.max(), which))
        print(summary.format('Inert', wfr_phi.min(), wfr_phi.max(), which))

        minima_array[..., 0] *= wfr_higgs
        minima_array_ext[..., 0] *= wfr_higgs
        minima_array[..., 1] *= wfr_phi
        minima_array_ext[..., 1] *= wfr_phi
    else:
        scales = 246. * np.ones((len(minima_array),))

    #print(np.c_[T_array, minima_array])

    sorted_indices = np.argsort(T_array)

    T_array = T_array[sorted_indices]
    minima_array = minima_array[sorted_indices]

    minima_array_ext = minima_array_ext[sorted_indices]
    scales = scales[sorted_indices]

    if which in m_thermal_store:
        thermal_mass = m_thermal_store[which]
        print("recycled thermal masses for Daisy = {}".format(which))
        thermal_mass = thermal_mass[sorted_indices]
    else:
        model.use_Daisy = float(which)
        minima_array_Ndim_full = helper.convert_to_full(minima_array_ext)
        thermal_mass = []
        for index, temp in enumerate(T_array):
            if temp == 0.:
                thermal_mass.append(np.zeros((model.N + 8)))
            else:
                thermal_mass.append(model.delta_m_daisy(np.abs(minima_array_Ndim_full[index]),
                                                        temp, scale=scales[index]))
        thermal_mass = np.array(thermal_mass)

    daisy_label = helper.get_daisy_label(which)

    boson_masses, _, _ = model.boson_massSq(minima_array_ext, T_array,
                                            scale=scales, deltaM=thermal_mass)
    if T_array[0] == 0.:
        m_sq_over_T = boson_masses[1:] / T_array[..., np.newaxis][1:]**2
    else:
        m_sq_over_T = boson_masses / T_array[..., np.newaxis]**2

    for entry in np.unique(np.around(m_sq_over_T.T, 3), axis=0):
        plt.plot(T_array[1:], entry)
    plt.plot([100., 100.], [-5., 5.], lw=0.5, ls='dashed', color='k', marker='None')
    plt.plot([500., 500.], [-5., 5.], lw=0.5, ls='dashed', color='k', marker='None')

    plt.title(r'$m^2/T^2$ of '+WHICH_BM+' BM')
#    plt.legend(loc='upper right')
    plt.xlabel(r"$T$ [GeV]")
    plt.ylabel(r"$m^2/T^2$")
    plt.tight_layout()
    plt.savefig('plots/'+WHICH_BM+'_msq_over_Tsq.png')
    plt.close()

    # plotting difference in J
    from cosmoTransitions.finiteT import Jb_spline as Jb
    from helper import highT

    plt.rc('text', usetex=True)
    plt.rc('text.latex', preamble=r'\usepackage{amsmath,amssymb}')
    plt.rc('font', family='serif')
    plt.rc('font', size=12)

    fig = plt.figure(dpi=150, figsize=[6., 6.])
    axis = fig.add_subplot(111)
    x_max = 1e5
    plt.xlim([0., x_max])
    plt.ylim([-0.05, 0.05])
    plt.xscale('symlog', linthreshx=500., linscalex=1.5)

    ticks, labels = helper.get_ticks_labels(x_max)

    plt.xticks(ticks=ticks, labels=labels, rotation=45)

    ls_cycler = cycler(ls=['solid', 'dashed', 'dashdot', 'dotted'])
    cl_cycler = cycler(color=['lightgreen', 'lightcoral', 'cornflowerblue',
                              'green'])
    my_cycler = ls_cycler + cl_cycler
    axis.set_prop_cycle(my_cycler)

    for entry in np.unique(np.around(m_sq_over_T.T, 3), axis=0):
        plt.plot(T_array[1:], (Jb(entry) - highT.Jb_high(entry))/Jb(entry))
    plt.plot([100., 100.], [-5., 5.], lw=0.5, ls='dashed', color='k', marker='None')
    plt.plot([500., 500.], [-5., 5.], lw=0.5, ls='dashed', color='k', marker='None')

    plt.title(r'$(J_{b, spline}(m^2/T^2) - J_{b, highT}(m^2/T^2))/J_{b, spline}(m^2/T^2)$ of '+WHICH_BM+' BM')
#    plt.legend(loc='upper right')
    plt.xlabel(r"$T$ [GeV]")
    plt.ylabel(r"$(J_{b, spline}(m^2/T^2) - J_{b, highT}(m^2/T^2))/J_{b, spline}(m^2/T^2)$")
    plt.tight_layout()
    plt.savefig('plots/'+WHICH_BM+'_delta_Jb.png')
    plt.close()

    # plotting difference in J'

    plt.rc('text', usetex=True)
    plt.rc('text.latex', preamble=r'\usepackage{amsmath,amssymb}')
    plt.rc('font', family='serif')
    plt.rc('font', size=12)

    fig = plt.figure(dpi=150, figsize=[6., 6.])
    axis = fig.add_subplot(111)
    x_max = 1e5
    plt.xlim([0., x_max])
    plt.ylim([-.05, .05])
    plt.xscale('symlog', linthreshx=500., linscalex=1.5)

    ticks, labels = helper.get_ticks_labels(x_max)

    plt.xticks(ticks=ticks, labels=labels, rotation=45)

    ls_cycler = cycler(ls=['solid', 'dashed', 'dashdot', 'dotted'])
    cl_cycler = cycler(color=['lightgreen', 'lightcoral', 'cornflowerblue',
                              'green'])
    my_cycler = ls_cycler + cl_cycler
    axis.set_prop_cycle(my_cycler)

    for entry in np.unique(np.around(m_sq_over_T.T, 3), axis=0):
        Jb_prime = der(Jb, entry, n=1, dx=1e-2, order=9)
        Jb_high_prime = der(highT.Jb_high, entry, n=1, dx=1e-2, order=9)
        plt.plot(T_array[1:], (Jb_prime - Jb_high_prime)/Jb_prime)
    plt.plot([100., 100.], [-5., 5.], lw=0.5, ls='dashed', color='k', marker='None')
    plt.plot([500., 500.], [-5., 5.], lw=0.5, ls='dashed', color='k', marker='None')

    plt.title(r'$(J_{b, spline}^{\prime}(m^2/T^2) - J_{b, highT}^{\prime}(m^2/T^2))/J_{b, spline}^{\prime}(m^2/T^2)$ of '+WHICH_BM+' BM')
#    plt.legend(loc='upper right')
    plt.xlabel(r"$T$ [GeV]")
    plt.ylabel(r"$(J_{b, spline}^{\prime}(m^2/T^2) - J_{b, highT}^{\prime}(m^2/T^2))/J_{b, spline}^{\prime}(m^2/T^2)$")
    plt.tight_layout()
    plt.savefig('plots/'+WHICH_BM+'_delta_Jb_prime.png')
    plt.close()

    # plotting difference in J''

    plt.rc('text', usetex=True)
    plt.rc('text.latex', preamble=r'\usepackage{amsmath,amssymb}')
    plt.rc('font', family='serif')
    plt.rc('font', size=12)

    fig = plt.figure(dpi=150, figsize=[6., 6.])
    axis = fig.add_subplot(111)
    x_max = 1e5
    plt.xlim([0., x_max])
    plt.ylim([-.05, .05])
    plt.xscale('symlog', linthreshx=500., linscalex=1.5)

    ticks, labels = helper.get_ticks_labels(x_max)

    plt.xticks(ticks=ticks, labels=labels, rotation=45)

    ls_cycler = cycler(ls=['solid', 'dashed', 'dashdot', 'dotted'])
    cl_cycler = cycler(color=['lightgreen', 'lightcoral', 'cornflowerblue',
                              'green'])
    my_cycler = ls_cycler + cl_cycler
    axis.set_prop_cycle(my_cycler)

    for entry in np.unique(np.around(m_sq_over_T.T, 3), axis=0):
        Jb_2prime = der(Jb, entry, n=2, dx=1e-2, order=9)
        Jb_high_2prime = der(highT.Jb_high, entry, n=2, dx=1e-2, order=9)
        plt.plot(T_array[1:], (Jb_2prime - Jb_high_2prime)/Jb_2prime)
    plt.plot([100., 100.], [-5., 5.], lw=0.5, ls='dashed', color='k', marker='None')
    plt.plot([500., 500.], [-5., 5.], lw=0.5, ls='dashed', color='k', marker='None')

    plt.title(r'$(J_{b, spline}^{\prime\prime}(m^2/T^2) - J_{b, highT}^{\prime\prime}(m^2/T^2))/J_{b, spline}^{\prime\prime}(m^2/T^2)$ of '+WHICH_BM+' BM')
#    plt.legend(loc='upper right')
    plt.xlabel(r"$T$ [GeV]")
    plt.ylabel(r"$(J_{b, spline}^{\prime\prime}(m^2/T^2) - J_{b, highT}^{\prime\prime}(m^2/T^2))/J_{b, spline}^{\prime\prime}(m^2/T^2)$")
    plt.tight_layout()
    plt.savefig('plots/'+WHICH_BM+'_delta_Jb_prime2.png')
    plt.close()

    print("plotting done")

if PLOT_THERMAL_AT_FIELD:
    print("* * * * * plotting m_thermal with T=Inert * * * * *")

    # helper.load_delta_dics(model) # too slow for large scans

    plt.rc('text', usetex=True)
    plt.rc('text.latex', preamble=r'\usepackage{amsmath,amssymb}')
    plt.rc('font', family='serif')
    plt.rc('font', size=12)

    T_scan = np.concatenate([np.arange(0., 500., 5.), np.arange(500., 1e3, 50.),
                             np.arange(1e3, 5e3, 5e2), np.arange(5e3, 1e4, 1e3),
                             np.arange(1e4, 5e4, 5e3), np.arange(5e4, 1e5, 1e4),
                             np.arange(1e5, 6e5, 1e5)])
    x_max = T_scan[-1]
    ticks_x, labels_x = helper.get_ticks_labels(x_max)

    phi_grid = T_scan.copy()
    field = np.concatenate([[np.zeros_like(phi_grid), phi_grid],
                            model.N * [np.zeros_like(phi_grid)]]).T

    ren_scale = model.current_scale(field, T_scan)
    field = helper.convert_to_full(field)
    print("done computing renormalization scale, start with thermal masses ....")

    if '1' in which_to_plot:
        daisy_label_1 = helper.get_daisy_label('1')
        model.use_Daisy = 1.
        daisy_1_RGE = model.delta_m_daisy(field, T_scan, scale=ren_scale)
        print("Done with Daisy {}".format(1))

    if '2' in which_to_plot:
        daisy_label_2 = helper.get_daisy_label('2')
        model.use_Daisy = 2.
        daisy_2_RGE = []
        for i, current_T in enumerate(T_scan):
            daisy_2_RGE.append(model.delta_m_daisy(field[i], current_T, scale=ren_scale[i]))
            print("Done with step {}/{}.".format(i+1, len(T_scan)))
        daisy_2_RGE = np.array(daisy_2_RGE)
        print("Done with Daisy {}".format(2))

    if '3' in which_to_plot:
        daisy_label_3 = helper.get_daisy_label('3')
        model.use_Daisy = 3.
        daisy_3_RGE = []
        for i, current_T in enumerate(T_scan):
            daisy_3_RGE.append(model.delta_m_daisy(field[i], current_T, scale=ren_scale[i]))
            print("Done with step {}/{}.".format(i+1, len(T_scan)))
        daisy_3_RGE = np.array(daisy_3_RGE)
        print("Done with Daisy {}".format(3))

    if '4' in which_to_plot:
        daisy_label_4 = helper.get_daisy_label('4')
        model.use_Daisy = 4.
        daisy_4_RGE = []
        for i, current_ in enumerate(T_scan):
            daisy_4_RGE.append(model.delta_m_daisy(field[i], current_T, scale=ren_scale[i]))
            print(daisy_4_RGE[-1])
            print("Done with step {}/{}.".format(i+1, len(T_scan)))
        daisy_4_RGE = np.array(daisy_4_RGE)
        print("Done with Daisy {}".format(4))

    print("Higgs plot:")

    fig = plt.figure(dpi=150, figsize=[8., 8.])
    axis = fig.add_subplot(111)
    plt.xlim([0., x_max])
    plt.xscale('symlog', linthreshx=500., linscalex=1.5)
    plt.yscale('symlog', linthreshy=500., linscaley=2.5)

    plt.xticks(ticks=ticks_x, labels=labels_x, rotation=45)
    ticks_y = np.arange(0., 600., 100.)
    labels_y = ["%.0f" % x for x in ticks_y.copy()]

    new_ticks = np.logspace(3, 12, 10)
    new_label = list(new_ticks.copy())
    for i, val in enumerate(new_label):
        new_label[i] = '{:1.0e}'.format(val)
    ticks_y = np.append(ticks_y, new_ticks)
    labels_y.extend(new_label)
    plt.yticks(ticks=ticks_y, labels=labels_y)

    if '1' in which_to_plot:
        plt.plot(phi_grid, daisy_1_RGE[..., 0], label="Higgs and GB, "+daisy_label_1,
                 lw=3., color='lightcoral', ls='dashed')

    if '2' in which_to_plot:
        plt.plot(phi_grid, daisy_2_RGE[..., 1], label="HiggsGB, "+daisy_label_2,
                 color='lightcoral', ls='dashdot', lw=2.)
        plt.plot(phi_grid, daisy_2_RGE[..., 2], label="HiggsGB, "+daisy_label_2,
                 color='lightcoral', ls='dashdot', lw=2.)
        plt.plot(phi_grid, daisy_2_RGE[..., 3], label="HiggsGB, "+daisy_label_2,
                 color='lightcoral', ls='dashdot', lw=2.)
        plt.plot(phi_grid, daisy_2_RGE[..., 0], label="Higgs, "+daisy_label_2,
                 color='red', ls='dashdot', lw=2.)

    if '3' in which_to_plot:
        plt.plot(phi_grid, daisy_3_RGE[..., 1], label="HiggsGB, "+daisy_label_3,
                 color='lightcoral', ls='solid', lw=1.5)
        plt.plot(phi_grid, daisy_3_RGE[..., 2], label="HiggsGB, "+daisy_label_3,
                 color='lightcoral', ls='solid', lw=1.5)
        plt.plot(phi_grid, daisy_3_RGE[..., 3], label="HiggsGB, "+daisy_label_3,
                 color='lightcoral', ls='solid', lw=1.5)
        plt.plot(phi_grid, daisy_3_RGE[..., 0], label="Higgs, "+daisy_label_3,
                 color='red', ls='solid', lw=1.5)

    if '4' in which_to_plot:
        plt.plot(phi_grid, daisy_4_RGE[..., 1], label="HiggsGB, "+daisy_label_4,
                 color='lightcoral', ls='dotted', lw=1.)
        plt.plot(phi_grid, daisy_4_RGE[..., 2], label="HiggsGB, "+daisy_label_4,
                 color='lightcoral', ls='dotted', lw=1.)
        plt.plot(phi_grid, daisy_4_RGE[..., 3], label="HiggsGB, "+daisy_label_4,
                 color='lightcoral', ls='dotted', lw=1.)
        plt.plot(phi_grid, daisy_4_RGE[..., 0], label="Higgs, "+daisy_label_4,
                 color='red', ls='dotted', lw=1.)

    plt.plot([0., 500., 500.], [500., 500., 0.], lw=0.5, ls='dashed', color='k', marker='None')
    plt.plot([T_scan[0], T_scan[-1]], [0., 0.], lw=0.75, ls='solid', color='k', marker='None')
    plt.title(r"Thermal masses, Higgs Doublet, $T = \phi$")
    plt.legend()
    plt.xlabel(r"Inert $\phi$")
    plt.savefig('plots/'+WHICH_BM+'_M_th_Higgs_T_phi.png')
    plt.close()


    print("Inert plot:")

    fig = plt.figure(dpi=300, figsize=[8., 8.])
    axis = fig.add_subplot(111)
    plt.xlim([0., x_max])
    plt.xscale('symlog', linthreshx=500., linscalex=1.5)
    plt.yscale('symlog', linthreshy=500., linscaley=2.5)

    plt.xticks(ticks=ticks_x, labels=labels_x, rotation=45)
    ticks_y = np.arange(-500., 600., 100.)
    labels_y = ["%.0f" % x for x in ticks_y.copy()]

    new_ticks = np.logspace(3, 12, 10)
    new_label = list(new_ticks.copy())
    new_label_neg = list(new_ticks.copy())
    for i, val in enumerate(new_label):
        new_label[i] = '{:1.0e}'.format(val)
        new_label_neg[i] = '{:1.0e}'.format(-val)
    ticks_y = np.append(ticks_y, new_ticks)
    labels_y.extend(new_label)
    ticks_y = np.append(ticks_y, -new_ticks)
    labels_y.extend(new_label_neg)
    plt.yticks(ticks=ticks_y, labels=labels_y)

    if '1' in which_to_plot:
        plt.plot(phi_grid, daisy_1_RGE[..., 4], label="Inert and GB, "+daisy_label_1,
                 lw=3., color='cornflowerblue', ls='dashed')

    if '2' in which_to_plot:
        plt.plot(phi_grid, daisy_2_RGE[..., 5], label="InertGB, "+daisy_label_2,
                 color='cornflowerblue', ls='dashdot', lw=2.)
        plt.plot(phi_grid, daisy_2_RGE[..., 6], label="InertGB, "+daisy_label_2,
                 color='cornflowerblue', ls='dashdot', lw=2.)
        plt.plot(phi_grid, daisy_2_RGE[..., 7], label="InertGB, "+daisy_label_2,
                 color='cornflowerblue', ls='dashdot', lw=2.)
        plt.plot(phi_grid, daisy_2_RGE[..., 4], label="Inert, "+daisy_label_2,
                 color='blue', ls='dashdot', lw=2.)

    if '3' in which_to_plot:
        plt.plot(phi_grid, daisy_3_RGE[..., 5], label="InertGB, "+daisy_label_3,
                 color='cornflowerblue', ls='solid', lw=1.5)
        plt.plot(phi_grid, daisy_3_RGE[..., 6], label="InertGB, "+daisy_label_3,
                 color='cornflowerblue', ls='solid', lw=1.5)
        plt.plot(phi_grid, daisy_3_RGE[..., 7], label="InertGB, "+daisy_label_3,
                 color='cornflowerblue', ls='solid', lw=1.5)
        plt.plot(phi_grid, daisy_3_RGE[..., 4], label="Inert, "+daisy_label_3,
                 color='blue', ls='solid', lw=1.5)

    if '4' in which_to_plot:
        plt.plot(phi_grid, daisy_4_RGE[..., 5], label="InertGB, "+daisy_label_4,
                 color='cornflowerblue', ls='dotted', lw=1.)
        plt.plot(phi_grid, daisy_4_RGE[..., 6], label="InertGB, "+daisy_label_4,
                 color='cornflowerblue', ls='dotted', lw=1.)
        plt.plot(phi_grid, daisy_4_RGE[..., 7], label="InertGB, "+daisy_label_4,
                 color='cornflowerblue', ls='dotted', lw=1.)
        plt.plot(phi_grid, daisy_4_RGE[..., 4], label="Inert, "+daisy_label_4,
                 color='blue', ls='dotted', lw=1.)

    plt.plot([0., 500., 500., 0.], [500., 500., -500., -500.], lw=0.5,
             ls='dashed', color='k', marker='None')
    plt.plot([T_scan[0], T_scan[-1]], [0., 0.], lw=0.75, ls='solid', color='k', marker='None')

    plt.title(r"Thermal masses, Inert Doublet, $T = \phi$")
    plt.legend()
    plt.xlabel(r"Inert $\phi$")
    plt.savefig('plots/'+WHICH_BM+'_M_th_Inert_T_phi.png')
    plt.close()

    print("Chi plot:")
    fig = plt.figure(dpi=300, figsize=[8., 8.])
    axis = fig.add_subplot(111)
    plt.xlim([0., x_max])
    plt.xscale('symlog', linthreshx=500., linscalex=1.5)
    plt.yscale('symlog', linthreshy=500., linscaley=2.5)

    plt.xticks(ticks=ticks_x, labels=labels_x, rotation=45)
    ticks_y = np.arange(0., 600., 100.)
    labels_y = ["%.0f" % x for x in ticks_y.copy()]

    new_ticks = np.logspace(3, 12, 10)
    new_label = list(new_ticks.copy())
    for i, val in enumerate(new_label):
        new_label[i] = '{:1.0e}'.format(val)
    ticks_y = np.append(ticks_y, new_ticks)
    labels_y.extend(new_label)
    plt.yticks(ticks=ticks_y, labels=labels_y)

    if '1' in which_to_plot:
        plt.plot(phi_grid, daisy_1_RGE[..., 8], label="Singlets, "+daisy_label_1,
                 lw=3., color='lightgreen', ls='dashed')

    if '2' in which_to_plot:
        plt.plot(phi_grid, daisy_2_RGE[..., 8], label="Singlets, "+daisy_label_2,
                 color='lightgreen', ls='dashdot', lw=2.)

    if '3' in which_to_plot:
        plt.plot(phi_grid, daisy_3_RGE[..., 8], label="Singlets, "+daisy_label_3,
                 color='lightgreen', ls='solid', lw=1.5)
    if '4' in which_to_plot:
        plt.plot(phi_grid, daisy_4_RGE[..., 8], label="Singlets, "+daisy_label_4,
                 color='lightgreen', ls='dotted', lw=1.)

    plt.plot([0., 500., 500.], [500., 500., 0.], lw=0.5, ls='dashed', color='k', marker='None')
    plt.plot([T_scan[0], T_scan[-1]], [0., 0.], lw=0.75, ls='solid', color='k', marker='None')
    plt.title(r"Thermal masses, Chi singlets, $T = \phi$")
    plt.legend()
    plt.xlabel(r"Inert $\phi$")
    plt.savefig('plots/'+WHICH_BM+'_M_th_chi_T_phi.png')
    plt.close()

if FIND_BFB_SCALE:
    print("* * * * * checking BFB * * * * *")

    num_pts = 10000
    scales = np.logspace(np.log10(246.), 10, num_pts)

    is_bounded = np.empty(num_pts, dtype=bool)
    for index, scale in enumerate(scales):
        is_bounded[index] = model.BFB(scale=scale)
    running_bound = True
    running_index = 0
    while running_bound:
        running_bound = running_bound and is_bounded[running_index]
        running_index += 1
    print("Model is bounded up to ren_scale {} and not bounded at {}".format(
        scales[running_index-2],
        scales[running_index-1]))
    print(is_bounded[running_index-2], is_bounded[running_index-1])
    print(scales[running_index-2], scales[running_index-1])

if PLOT_THERMAL_JOINT:
    print("* * * * * plotting thermal masses in joint plot* * * * *")

    plt.rc('text', usetex=True)
    plt.rc('text.latex', preamble=r'\usepackage{amsmath,amssymb}')
    plt.rc('font', family='serif')
    plt.rc('font', size=12)

    model.use_RGE = True
    plot_min = 5e3
    plot_max = 1.5e4
    plot_num_pts = 51
    current_T = 5e3

    daisy_label_1 = helper.get_daisy_label('1')
    daisy_label_2 = helper.get_daisy_label('2')
    #daisy_label_3 = helper.get_daisy_label('3')
    daisy_label_4 = helper.get_daisy_label('4')

    phi_grid = np.linspace(plot_min, plot_max, plot_num_pts)
    field = np.concatenate([[np.zeros_like(phi_grid), phi_grid],
                            model.N * [np.zeros_like(phi_grid)]]).T
    ren_scale = model.current_scale(field, current_T)
    field = helper.convert_to_full(field)
    print("Done computing renormalization scale for field configuration of T = {}".format(current_T))

    model.use_Daisy = 1.
    daisy_1_RGE = model.delta_m_daisy(field, current_T, scale=ren_scale)
    print("Done with Daisy {} at T={}".format(1, current_T))
    model.use_Daisy = 2.
    daisy_2_RGE = model.delta_m_daisy(field, current_T, scale=ren_scale)
    print("Done with Daisy {} at T={}".format(2, current_T))
    #model.use_Daisy = 3.
    #daisy_3_RGE = model.delta_m_daisy(field, current_T, scale=ren_scale)
    #print("Done with Daisy {} at T={}".format(3, current_T))
    model.use_Daisy = 4.
    daisy_4_RGE = model.delta_m_daisy(field, current_T, scale=ren_scale)
    print("Done with Daisy {} at T={}".format(4, current_T))

    #fig = plt.figure(dpi=300, figsize=[8., 8.])
    #axis = fig.add_subplot(111)

    fig, (ax1, ax2) = plt.subplots(2, 1, sharex=True, figsize=[7., 5.25])
    fig.subplots_adjust(hspace=0.15)

    plt.xlim([phi_grid[0], phi_grid[-1]])

    ax1.plot(phi_grid, daisy_1_RGE[..., 0], label="Higgs and GB, "+daisy_label_1,
             lw=3., color='lightcoral', ls='dashed')

    ax1.plot(phi_grid, daisy_2_RGE[..., 1], label="HiggsGB, "+daisy_label_2,
             color='lightcoral', ls='dashdot', lw=2.)
    ax1.plot(phi_grid, daisy_2_RGE[..., 2], label="HiggsGB, "+daisy_label_2,
             color='lightcoral', ls='dashdot', lw=2.)
    ax1.plot(phi_grid, daisy_2_RGE[..., 3], label="HiggsGB, "+daisy_label_2,
             color='lightcoral', ls='dashdot', lw=2.)
    ax1.plot(phi_grid, daisy_2_RGE[..., 0], label="Higgs, "+daisy_label_2,
             color='red', ls='dashdot', lw=2.)

    ax1.plot(phi_grid, daisy_4_RGE[..., 1], label="HiggsGB, "+daisy_label_4,
             color='lightcoral', ls='dotted', lw=2.)
    ax1.plot(phi_grid, daisy_4_RGE[..., 2], label="HiggsGB, "+daisy_label_4,
             color='lightcoral', ls='dotted', lw=2.)
    ax1.plot(phi_grid, daisy_4_RGE[..., 3], label="HiggsGB, "+daisy_label_4,
             color='lightcoral', ls='dotted', lw=2.)
    ax1.plot(phi_grid, daisy_4_RGE[..., 0], label="Higgs, "+daisy_label_4,
             color='red', ls='dotted', lw=2.)

    ax2.plot(phi_grid, daisy_1_RGE[..., 4], label="Inert and GB, "+daisy_label_1,
             lw=3., color='cornflowerblue', ls='dashed')

    ax2.plot(phi_grid, daisy_2_RGE[..., 5], label="InertGB, "+daisy_label_2,
             color='cornflowerblue', ls='dashdot', lw=2.)
    ax2.plot(phi_grid, daisy_2_RGE[..., 6], label="InertGB, "+daisy_label_2,
             color='cornflowerblue', ls='dashdot', lw=2.)
    ax2.plot(phi_grid, daisy_2_RGE[..., 7], label="InertGB, "+daisy_label_2,
             color='cornflowerblue', ls='dashdot', lw=2.)
    ax2.plot(phi_grid, daisy_2_RGE[..., 4], label="Inert, "+daisy_label_2,
             color='blue', ls='dashdot', lw=2.)

    ax2.plot(phi_grid, daisy_4_RGE[..., 5], label="InertGB, "+daisy_label_4,
             color='cornflowerblue', ls='dotted', lw=2.)
    ax2.plot(phi_grid, daisy_4_RGE[..., 6], label="InertGB, "+daisy_label_4,
             color='cornflowerblue', ls='dotted', lw=2.)
    ax2.plot(phi_grid, daisy_4_RGE[..., 7], label="InertGB, "+daisy_label_4,
             color='cornflowerblue', ls='dotted', lw=2.)
    ax2.plot(phi_grid, daisy_4_RGE[..., 4], label="Inert, "+daisy_label_4,
             color='blue', ls='dotted', lw=2.)

    ax1.plot(phi_grid, daisy_1_RGE[..., 8], label="Singlets, "+daisy_label_1,
             lw=3., color='green', ls='dashed')

    ax1.plot(phi_grid, daisy_2_RGE[..., 8], label="Singlets, "+daisy_label_2,
             color='green', ls='dashdot', lw=2.)

    ax1.plot(phi_grid, daisy_4_RGE[..., 8], label="Singlets, "+daisy_label_4,
             color='green', ls='dotted', lw=2.)

    if WHICH_BM == 'heavy':
        ax1.set_ylim(3.5e6, 8.5e6)
        ax2.set_ylim(-2.3e7, -1.4e7)
    elif WHICH_BM == 'light':
        ax1.set_ylim(3.9e6, 8.5e6)
        ax2.set_ylim(-2e7, -9e6)
    # hide the spines between ax and ax2
    ax1.spines['bottom'].set_visible(False)
    ax2.spines['top'].set_visible(False)
    ax1.xaxis.tick_top()
    ax1.tick_params(labeltop=False)  # don't put tick labels at the top
    ax2.xaxis.tick_bottom()

    d = .5  # proportion of vertical to horizontal extent of the slanted line
    kwargs = dict(marker=[(-1, -d), (1, d)], markersize=12,
                  linestyle="none", color='k', mec='k', mew=1, clip_on=False)
    ax1.plot([0, 1], [0, 0], transform=ax1.transAxes, **kwargs)
    ax2.plot([0, 1], [1, 1], transform=ax2.transAxes, **kwargs)



    ax1.text(4800., 8.65e6, r' $\times 10^6 [\text{GeV}^2]$')

    upper_ticks = ax1.get_yticks() / 1e6
    ax1.set_yticklabels(upper_ticks)
    lower_ticks = ax2.get_yticks() / 1e6
    ax2.set_yticklabels(lower_ticks)

    ax1.set_title(r"Th. masses squared, $T = {:d}$ GeV".format(int(current_T)), loc='right')

    legend_elements_line = [Line2D([0], [0], color='gray', lw=3, ls='dashed', label=daisy_label_1),
                            Line2D([0], [0], color='gray', lw=2, ls='dashdot', label=daisy_label_2),
                            Line2D([0], [0], color='gray', lw=2, ls='dotted', label=daisy_label_4)]

    legend_elements_color = [Patch(facecolor='red', label='Higgs'),
                             Patch(facecolor='lightcoral', label='Higgs GB'),
                             Patch(facecolor='blue', label='Inert'),
                             Patch(facecolor='cornflowerblue', label='Inert GB'),
                             Patch(facecolor='green', label='Singlets')]
    ax1.legend(handles=legend_elements_line, bbox_to_anchor=(1.05, 1), loc='upper left')
    ax2.legend(handles=legend_elements_color, bbox_to_anchor=(1.05, 1), loc='upper left')
    plt.xlabel(r"Inert $\phi$ [GeV]", fontsize=14)
    plt.tight_layout()
    #plt.show()

    plt.savefig('plots/'+WHICH_BM+'_M_th_Joint_T_'+str(current_T)+'.png', dpi=300)
    plt.close()

if PLOT_TRACED_MINIMA_CW:
    print("* * * * * plotting traced minima and v/T with combined CW results* * * * *")

    if model.use_CW == 0:
        model_no_CW = model
        model_parameters['use_CW'] = 1.
        model_CW = model_2HDM_N(**model_parameters)
    else:
        model_CW = model
        model_parameters['use_CW'] = 0.
        model_no_CW = model_2HDM_N(**model_parameters)

    # helper.load_delta_dics(model)
    helper.load_minima_dic(model_CW)
    helper.load_minima_dic(model_no_CW)

    plt.rc('text', usetex=True)
    plt.rc('text.latex', preamble=r'\usepackage{amsmath,amssymb}')
    plt.rc('font', family='serif')
    plt.rc('font', size=12)

    fig = plt.figure(dpi=150, figsize=[8., 6.])
    axis = fig.add_subplot(111)
    x_max = 1e5
    plt.xlim([0., 4e4])
    plt.ylim([-50., 1e6])
    plt.xscale('symlog', linthreshx=500., linscalex=1.5)
    plt.yscale('symlog', linthreshy=500., linscaley=1.5)

    ticks_x, labels_x = helper.get_ticks_labels(x_max)
    ticks_y, labels_y = helper.get_ticks_labels(1e7)


    plt.xticks(ticks=ticks_x, labels=labels_x, rotation=45)
    plt.yticks(ticks=ticks_y, labels=labels_y)

    ls_cycler = cycler(ls=['solid', 'dashed', 'dashdot', 'dotted'])
    #ms_cycler = cycler(marker=['o', 'x', '.'])
    ms_cycler = cycler(markersize=[8, 8, 8])
    lw_cycler = cycler(lw=[1., 1.])
    cl_cycler = cycler(color=['lightgreen', 'lightcoral', 'cornflowerblue',
                              'green', 'red', 'blue'])
    my_cycler = (ls_cycler * (2*ms_cycler)) + (2*(cl_cycler)*lw_cycler)+\
        cycler(marker=[None, None, None, None, None, None, None, None, None, None, None, None,
                       None, None, None, None, None, None, '+', '+', '+', '+', '+', '+'])+\
        cycler(alpha=[1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1.,
                      0.5, 0.5, 0.5, 0.5, 0.5, 0.5])

    axis.set_prop_cycle(my_cycler)
    scaled_minima_dict_cw = {}
    scaled_minima_dict_no_cw = {}
    joint_T_dict = {}
    for which in which_to_plot:
        minima_CW = model_CW.__dict__[f'minima_dict_wd{which}']
        minima_no_CW = model_no_CW.__dict__[f'minima_dict_wd{which}']
        T_array_CW = []
        minima_array_CW = []
        T_array_no_CW = []
        minima_array_no_CW = []
        for entry in minima_CW:
            T_array_CW.append(entry)
        for entry in minima_no_CW:
            T_array_no_CW.append(entry)
        T_array = []
        for temp in T_array_CW:
            if temp in T_array_no_CW:
                if which == '4' and WHICH_BM == 'light':
                    if (temp < 400) or (temp in [3e3, 4e3, 1e5]):
                        pass
                    else:
                        T_array.append(temp)
                else:
                    T_array.append(temp)
        for entry in T_array:
            minima_array_CW.append(minima_CW[entry])
            minima_array_no_CW.append(minima_no_CW[entry])
        T_array = np.array(T_array, dtype=np.float64)
        joint_T_dict[which] = T_array
        minima_array_CW = np.array(minima_array_CW)
        minima_array_no_CW = np.array(minima_array_no_CW)

        # wfr:
        if USE_WFR:
            minima_array_ext_CW = np.zeros((len(minima_array_CW), 2+model.N))
            minima_array_ext_CW[..., 0] = minima_array_CW[..., 0]
            minima_array_ext_CW[..., 1] = minima_array_CW[..., 1]
            minima_array_ext_CW[..., 2:] = np.ones((1, model.N)) *\
                minima_array_CW[..., 2, np.newaxis]
            minima_array_ext_no_CW = np.zeros((len(minima_array_no_CW), 2+model.N))
            minima_array_ext_no_CW[..., 0] = minima_array_no_CW[..., 0]
            minima_array_ext_no_CW[..., 1] = minima_array_no_CW[..., 1]
            minima_array_ext_no_CW[..., 2:] = np.ones((1, model.N)) *\
                minima_array_no_CW[..., 2, np.newaxis]

            if model.use_CW == 0:
                if which in computed_scales:
                    scales_no_CW = computed_scales[which]
                    print("recycled renormalization scales")
                else:
                    scales_no_CW = model_no_CW.current_scale(minima_array_ext_no_CW, T_array)
                    computed_scales[which] = scales_no_CW
                scales_CW = model_CW.current_scale(minima_array_ext_CW, T_array)
            else:
                if which in computed_scales:
                    scales_CW = computed_scales[which]
                    print("recycled renormalization scales")
                else:
                    scales_CW = model_CW.current_scale(minima_array_ext_CW, T_array)
                    computed_scales[which] = scales_CW
                scales_no_CW = model_no_CW.current_scale(minima_array_ext_no_CW, T_array)

            scaled_minima_dict_cw[which] = scales_CW
            scaled_minima_dict_no_cw[which] = scales_no_CW

            print("Applying wave function renormalization")
            wfr_higgs_CW = model_CW.wfr_higgs(scales_CW)
            wfr_phi_CW = model_CW.wfr_phi(scales_CW)
            wfr_higgs_no_CW = model_no_CW.wfr_higgs(scales_no_CW)
            wfr_phi_no_CW = model_no_CW.wfr_phi(scales_no_CW)
            summary = "{} correction is between {} and {} for Daisy = {}"
            print("CW on:")
            print(summary.format('Higgs', wfr_higgs_CW.min(), wfr_higgs_CW.max(), which))
            print(summary.format('Inert', wfr_phi_CW.min(), wfr_phi_CW.max(), which))
            print("CW off:")
            print(summary.format('Higgs', wfr_higgs_no_CW.min(), wfr_higgs_no_CW.max(), which))
            print(summary.format('Inert', wfr_phi_no_CW.min(), wfr_phi_no_CW.max(), which))

            minima_array_CW[..., 0] *= wfr_higgs_CW
            minima_array_CW[..., 1] *= wfr_phi_CW
            minima_array_no_CW[..., 0] *= wfr_higgs_no_CW
            minima_array_no_CW[..., 1] *= wfr_phi_no_CW

            scaled_minima_dict_cw[which] = minima_array_CW
            scaled_minima_dict_no_cw[which] = minima_array_no_CW


        #print(T_array, minima_array)
        T_array = T_array[T_array <= x_max]
        sorted_indices = np.argsort(T_array)

        daisy_label = helper.get_daisy_label(which)

        plt.plot(T_array[sorted_indices],
                 np.abs(minima_array_CW[..., 2])[sorted_indices],
                 #np.abs(minima_array_no_CW[..., 2])[sorted_indices],
                 label='Singlet, '+daisy_label,
                 lw=1.25)
        plt.plot(T_array[sorted_indices],
                 #np.abs(minima_array_CW[..., 2])[sorted_indices],
                 np.abs(minima_array_no_CW[..., 2])[sorted_indices],
                 #label='Singlet, '+daisy_label,
                 lw=1.25)
        plt.plot(T_array[sorted_indices],
                 np.abs(minima_array_CW[..., 0])[sorted_indices],
                 #np.abs(minima_array_no_CW[..., 0])[sorted_indices],
                 label='Higgs, '+daisy_label,
                 lw=1.25)
        plt.plot(T_array[sorted_indices],
                 #np.abs(minima_array_CW[..., 0])[sorted_indices],
                 np.abs(minima_array_no_CW[..., 0])[sorted_indices],
                 #label='Higgs, '+daisy_label,
                 lw=1.25)
        plt.plot(T_array[sorted_indices],
                 np.abs(minima_array_CW[..., 1])[sorted_indices],
                 #np.abs(minima_array_no_CW[..., 1])[sorted_indices],
                 label='Inert, '+daisy_label,
                 lw=1.25)
        plt.plot(T_array[sorted_indices],
                 #np.abs(minima_array_CW[..., 1])[sorted_indices],
                 np.abs(minima_array_no_CW[..., 1])[sorted_indices],
                 #label='Inert, '+daisy_label,
                 lw=1.25)

    plt.plot([0., 500., 500.], [500., 500., 0.], lw=0.5, ls='dashed', color='k', marker='None')
    plt.xlim([0., x_max if WHICH_BM=='light' else 4e4])
    if WHICH_BM == 'heavy':
        plt.ylim([-50., 2e5])
    plt.title('Phase Structure of BM ' + ('A' if WHICH_BM=='light' else 'B'))
    plt.legend(loc='upper left')
    plt.xlabel(r"$T$ [GeV]")
    plt.ylabel(r"Field value at Minimum")
    plt.tight_layout()
    plt.savefig('plots/'+WHICH_BM+'_BM_phase_structure_BOTH.png')
    plt.close()

    print("plotting 1 done")
    fig = plt.figure(dpi=150, figsize=[6., 6.])
    axis = fig.add_subplot(111)
    x_max = 1e5
    plt.xscale('symlog', linthreshx=500., linscalex=1.5)

    ticks, labels = helper.get_ticks_labels(x_max)

    plt.xticks(ticks=ticks, labels=labels, rotation=45)
    ls_cycler = cycler(ls=['solid', 'dashed', 'dashdot', 'dotted'])
    #cl_cycler = cycler(color=['lightgreen', 'lightcoral', 'cornflowerblue',
    #                          'green'])

    #cl_cycler = cycler(lw=[1.5, 1.5, 1.5, 1.5])
    #my_cycler = ((ls_cycler + cl_cycler)*cycler(color=['k', 'b']))+\

    cl_cycler = cycler(color=['k', 'k', 'k', 'k'])
    my_cycler = ((ls_cycler + cl_cycler)*cycler(lw=[1.5, 1.5]))+\
        cycler(marker=[None, None, None, None, None, None, '+', '+'])+\
        cycler(alpha=[1., 1., 1., 1., 1., 1., 0.5, 0.5])
    axis.set_prop_cycle(my_cycler)
    for which in which_to_plot:
        T_array = joint_T_dict[which]
        minima_array_CW = scaled_minima_dict_cw[which]
        minima_array_no_CW = scaled_minima_dict_no_cw[which]
        print("recycled wavefunction renormalization from previous plot")

        v_eff_CW = np.sqrt(minima_array_CW[..., 0]**2 + minima_array_CW[..., 1]**2)
        v_eff_no_CW = np.sqrt(minima_array_no_CW[..., 0]**2 + minima_array_no_CW[..., 1]**2)
        sorted_indices = np.argsort(T_array)

        daisy_label = helper.get_daisy_label(which)

        if T_array[sorted_indices[0]] == 0.:
            plt.plot(T_array[sorted_indices][1:],
                     v_eff_CW[sorted_indices][1:]/T_array[sorted_indices][1:],
                     label=daisy_label, lw=1.5)
            plt.plot(T_array[sorted_indices][1:],
                     v_eff_no_CW[sorted_indices][1:]/T_array[sorted_indices][1:], lw=1.5)
        else:
            plt.plot(T_array[sorted_indices], v_eff_CW[sorted_indices]/T_array[sorted_indices],
                     label=daisy_label, lw=1.5)
            plt.plot(T_array[sorted_indices], v_eff_no_CW[sorted_indices]/T_array[sorted_indices],
                     lw=1.5)

    plt.plot([0., x_max], [1., 1.], lw=0.5, ls='dashed', color='r', marker='None')
    plt.plot([500., 500.], [0., 5.], lw=0.5, ls='dashed', color='k', marker='None')

    plt.xlim([0., x_max if WHICH_BM=='light' else 4e4])
    plt.ylim([0., 5.])
    plt.title(r'$\xi$ of BM ' + ('A' if WHICH_BM=='light' else 'B'))
    plt.legend(loc='upper right')
    plt.xlabel(r"$T$ [GeV]")
    #plt.ylabel(r"$v_T / T$")
    plt.ylabel(r"$\xi = \sqrt{v^2 + w^2} / T$")
    plt.tight_layout()
    plt.savefig('plots/'+WHICH_BM+'_BM_v_over_T_BOTH.png')
    plt.close()

    print("all plotting done")
