""" Solving the result of [1] https://arxiv.org/pdf/hep-ph/9210228.pdf
    numerically and check how \bar{s}(t) = 0 needs to be
    implemented numerically (if at all)"""

import numpy as np
from scipy.integrate import solve_ivp
import matplotlib.pyplot as plt

def rge(t, c):
    lmbda, msq, phit, omega = c
    
    beta_lmbda = 3.* lmbda**2

    gamma_msq = -lmbda

    # eq to be solved:
    beta_msq = - gamma_msq * msq

    gamma_phi = 0.*phit

    # eq to be solved:
    beta_phi = - gamma_phi

    beta_omega = 2.*omega*gamma_msq + 0.5

    pre = 1./(16.*np.pi**2)

    ret = pre * np.array([beta_lmbda, beta_msq, beta_phi, beta_omega])
    return ret

def eff_pot_rge(phi, ren_sc, lmbda, msq, omega):
    """LO part of [1], eq. 33"""
    logs = 3.*lmbda * np.log((0.5*lmbda*phi**2 + msq)/(ren_sc**2))/(32.*np.pi**2)
    lmbda_t = lmbda / (1.-logs)
    msq_t = msq/(1.-logs)**(1./3.)
    omegam4_t = omega*msq**2 + 0.5*((msq**2)/lmbda)*(1.-(1.-logs)**(1/3))

    potential = 0.5*msq_t*phi**2 + (1./24.)*lmbda_t*phi**4 + omegam4_t
    return potential

def eff_pot_plain(phi, ren_sc, lmbda, msq, omega):
    """LO part of [1], eq. 2"""
    potential = 0.5*msq*phi**2 + (1./24.)*lmbda*phi**4 + omega*msq**2
    return potential

def eff_pot_beta_fix(phi, ren_sc, ivp_sol):
    """LO potential with running couplings, scale fix"""
    lmbda, msq, phit, omega = ivp_sol.sol(np.log(ren_sc))
    phi = phi*phit
    potential = 0.5*msq*phi**2 + (1./24.)*lmbda*phi**4 + omega*msq**2
    return potential

def eff_pot_beta_phi(phi, ren_sc, ivp_sol):
    """LO potential with running couplings, scale as phi"""
    lmbda, msq, phit, omega = ivp_sol.sol(np.log(np.maximum(phi, ren_sc)))
    phi = phi*phit
    potential = 0.5*msq*phi**2 + (1./24.)*lmbda*phi**4 + omega*msq**2
    return potential

def phi_from_t(ren_sc, ivp_sol, t):
    """ eq. 35 of [1]"""
    t_ivp = t+np.log(ren_sc)
    lmbda, msq, phit, omega = ivp_sol.sol(t_ivp)
    del omega
    ret = (2./lmbda) * (1./phit)**2 *(np.exp(2.*t)*ren_sc**2 - msq)
    if ret.any() <0:
        print("Running should start at the mass scale of the field!")
    return np.sqrt(ret)

def eff_pot_num(ren_sc, ivp_sol, t):
    """Numerical implementation as suggested in [1] """
    phi = phi_from_t(ren_sc, ivp_sol, t)
    t_ivp = t+np.log(ren_sc)
    lmbda, msq, phit, omega = ivp_sol.sol(t_ivp)
    del phit
    potential = 0.5*msq*phi**2 + (1./24.)*lmbda*phi**4 + omega*msq**2
    return potential

start = 50.
end = 5000.
delta = .1

couplings = ['lambda', 'm^2', 'phi_tilde', 'omega']
c_0 = np.array([0.1, 50.**2, 1., 0.])

span = [np.log(start), np.log(end)]
t_eval = np.arange(np.log(start)+delta, np.log(end), delta)
sol_DOP = solve_ivp(rge, span, c_0, t_eval = t_eval, dense_output=True, method = 'DOP853')
#sol_RK = solve_ivp(rge, span, c_0, t_eval = t_eval, dense_output=True, method = 'RK45')
#np.set_printoptions(threshold = np.inf)
print(sol_DOP.message)
#for i in range(len(couplings)):
#    print("The running of {} between {} GeV and {} GeV:".format(couplings[i], start, end))
#    print(sol_DOP.y[i])
#print("*** other method***")
#print(sol_RK.message)
#for i in range(len(couplings)):
#    print("The running of {} between {} GeV and {} GeV:".format(couplings[i], start, end))
#    print(sol_RK.y[i])
#np.set_printoptions(threshold = 1000)

# plots:
x_min = 0.
x_max = 100.
increments = 1000
t_test = np.linspace(0., 1.5, 10001)
field_values = phi_from_t(50., sol_DOP, t_test)


x_pts = np.linspace(x_min, x_max, increments+1)
fig = plt.figure(dpi=150, figsize=[6., 6.])
axis = fig.add_subplot(111)
plt.xlim([x_min, x_max])
plt.ylim([0., 1e6])

#plt.xscale('log')
plt.plot(x_pts, eff_pot_rge(x_pts, start, c_0[0], c_0[1], c_0[3]), label = "Eq. 33 of hep-ph/9210228", color = 'k')
plt.plot(x_pts, eff_pot_plain(x_pts, start, c_0[0], c_0[1], c_0[3]), label = "Eq. 2 of hep-ph/9210228", color = 'b')
plt.plot(x_pts, eff_pot_beta_fix(x_pts, 50., sol_DOP), label = r"tree-level potential with running to $\mu = 50$ GeV", color = 'r', ls='--')
plt.plot(x_pts, eff_pot_beta_fix(x_pts, 500., sol_DOP), label = r"tree-level potential with running to $\mu = 500$ GeV", color = 'r', ls='-.')
plt.plot(x_pts, eff_pot_beta_fix(x_pts, 5000., sol_DOP), label = r"tree-level potential with running to $\mu = 5000$ GeV", color = 'r', ls='dotted')
plt.plot(x_pts, eff_pot_beta_phi(x_pts, 50., sol_DOP), label = r"tree-level potential with running to $\mu = \phi$", color = 'g', ls='dotted')
plt.plot(field_values, eff_pot_num(50., sol_DOP, t_test), label = r"Eq. 20 and 35 of hep-ph/9210228", color = 'orange', ls='dotted')

plt.title("RGE-improved Effective Potential")
plt.legend()
plt.xlabel(r"$\phi$")
plt.ylabel(r"$V(\phi)$")
plt.savefig('EP_ref.png')
plt.show()
plt.close()

x_min = 940.
x_max = 960.
increments = 1000

x_pts = np.linspace(x_min, x_max, increments+1)
fig = plt.figure(dpi=150, figsize=[6., 6.])
axis = fig.add_subplot(111)
plt.xlim([x_min, x_max])
plt.ylim([4.2e9, 4.7e9])

#plt.xscale('log')
plt.plot(x_pts, eff_pot_rge(x_pts, start, c_0[0], c_0[1], c_0[3]), label = "Eq. 33 of hep-ph/9210228", color = 'k')
plt.plot(x_pts, eff_pot_plain(x_pts, start, c_0[0], c_0[1], c_0[3]), label = "Eq. 2 of hep-ph/9210228", color = 'b')
plt.plot(x_pts, eff_pot_beta_fix(x_pts, 50., sol_DOP), label = r"tree-level potential with running to $\mu = 50$ GeV", color = 'r', ls='--')
plt.plot(x_pts, eff_pot_beta_fix(x_pts, 500., sol_DOP), label = r"tree-level potential with running to $\mu = 500$ GeV", color = 'r', ls='-.')
plt.plot(x_pts, eff_pot_beta_fix(x_pts, 5000., sol_DOP), label = r"tree-level potential with running to $\mu = 5000$ GeV", color = 'r', ls='dotted')
plt.plot(x_pts, eff_pot_beta_phi(x_pts, 50., sol_DOP), label = r"tree-level potential with running to $\mu = \phi$", color = 'g', ls='dotted')
plt.plot(field_values, eff_pot_num(50., sol_DOP, t_test), label = r"Eq. 20 and 35 of hep-ph/9210228", color = 'orange', ls='dotted')

plt.title("RGE-improved Effective Potential")
plt.legend()
plt.xlabel(r"$\phi$")
plt.ylabel(r"$V(\phi)$")
plt.savefig('EP_ref_large.png')
plt.show()
plt.close()

eval_pt = 950.
print("The value of the effective potential at {} is: \n Eq. 33 of hep-ph/9210228: {} \n Eq. 2 of hep-ph/9210228: {}\n tree-level potential with running to {}: {}\n tree-level potential with running to {}: {}\n tree-level potential with running to {}: {}\n tree-level potential with running to phi: {}".format(eval_pt, eff_pot_rge(eval_pt, start, c_0[0], c_0[1], c_0[3]), eff_pot_plain(eval_pt, start, c_0[0], c_0[1], c_0[3]), 50, eff_pot_beta_fix(eval_pt, 50., sol_DOP), 500, eff_pot_beta_fix(eval_pt, 500., sol_DOP), 5000, eff_pot_beta_fix(eval_pt, 5000., sol_DOP),eff_pot_beta_phi(eval_pt, 50., sol_DOP)))

