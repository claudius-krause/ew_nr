""" Compute sphaleron energy of our model. 
    Based on Newton-Kantorovich method of 1708.03061"""

import numpy as np
import scipy.optimize as opt
import scipy.interpolate as interp
from scipy.misc import derivative as derv
import scipy.integrate as integrate
import matplotlib.pyplot as plt
from matplotlib.ticker import (MultipleLocator, FormatStrFormatter,
                               AutoMinorLocator)

# set physics constants
Gf = 1.16637e-5
mW = 80.385
v = np.sqrt(1./(np.sqrt(2)*Gf))
g = 2.*mW / v

#v = 246. 
#g = 0.65

# SM:
mh=125.
lbda= mh**2 / (2.*v**2)
mu2 = -(mh**2)/2.

#lbda = 0.3 * g**2
#mu2 = - lbda*v**2

print("lambda/g^2 = {}".format(lbda/g**2))

# set numerical parameters
N = 2000 # ref: 10000
L = 30. # ref: 30
maxiter = 50

# define discrete coordinate
xi = np.linspace(0.,L,N+1)
deltax = L/N

# some flags

plotB = False
plotRest = False

# first guess of functions
h_guess = np.tanh(10.*xi)
f_guess = np.tanh(3.*xi)


P_guess = np.exp(-0.5*xi)
P_guess[1] = P_guess[0] # needed for boundary cond.
#P_guess[0] = (4./3.)*P_guess[1] - P_guess[2]/3.
Q_guess = -np.exp(-0.5*xi)
Q_guess[1] = Q_guess[0] # needed for boundary cond.
#Q_guess[0] = (4./3.)*Q_guess[1] - Q_guess[2]/3.


Yrun = np.array([h_guess,f_guess])
PQrun = np.array([P_guess,Q_guess])

def first_der(func, upper=1.):
    """ First derivative, defined as
        h_i+1 - h_i / delta
        Derivative at i=N computed with h_N+1 = upper
    """
    assert len(func) == N+1
    der = (func[1:] - func[:-1]) / deltax
    der = np.append(der, (upper-func[-1]) / deltax)
    return der

def first_der_left(func, lower=0.):
    """ First derivative, defined as
        h_i - h_i-1 / delta
        Derivative at i=0 computed with h_1- = lower
    """
    assert len(func) == N+1
    der = (func[1:] - func[:-1]) / deltax
    der = np.insert(der, 0, (func[0]-lower) / deltax)
    return der

def second_der(func, upper=1., lower=0.):
    """ Second derivative, defined as
        h_i+1 - 2 h_i  + h_i-1 / delta**2
        Derivative at i=N computed with h_N+1 = upper
        and at i=0 computed with h_-1 = lower
    """
    assert len(func) == N+1
    der = (func[2:] - 2.*func[1:-1] + func[:-2]) / deltax**2
    der = np.insert(der, 0, (func[1] - 2.*func[0] + lower) / deltax**2)
    der = np.append(der, (upper - 2.*func[-1] + func[-2]) / deltax**2)
    return der

def delete(arr, row, column):
    """ delete row row and column column
        of the 2d array arr"""
    ret = np.delete(arr, row, axis=0)
    ret = np.delete(ret, column, axis=1)
    return ret

def Jacobian_no_d(Y):
    current_h, current_f = Y
    dF1dh = -(mu2 * xi**2)/(g**2 * v**2) - (3.*lbda*current_h**2 * xi**2)/(g**2)\
            -2.*(1-current_f)**2
    dF1df = 4.*current_h*(1.-current_f)
    dF2dh = 0.5 * current_h * xi**2 * (1.-current_f)
    dF2df = -12.*current_f**2 + 12.*current_f - 0.25*current_h**2 * xi**2 -2.
    
    return np.array([[dF1dh, dF1df],[dF2dh, dF2df]])

#def Jacobian_times_delta(Y, delta_fh):
#    delta_h, delta_f = delta_fh
#    JacobianNOd = Jacobian_no_d(Y)
#    dF1dh, dF1df = JacobianNOd[0]
#    dF2dh, dF2df = JacobianNOd[1]
#
#    F1 = xi**2 * second_der(delta_h, 0., 0.)  + 2.*xi*first_der(delta_h, 0.)\
#         +dF1dh*delta_h + dF1df*delta_f
#    F2 = xi**2*second_der(delta_f, 0., 0.) + dF2dh*delta_h + dF2df*delta_f
#
#    return np.array([F1, F2])

def RHS_F(Y):
    current_h, current_f = Y
    F1 = 2.*xi*first_der(current_h) + xi**2*second_der(current_h)-2.*(1-current_f)**2*current_h\
         -mu2*xi**2*current_h/(g**2 * v**2) - lbda*xi**2*current_h**3 / g**2
    F2 = xi**2 * second_der(current_f) - 2*current_f*(1-current_f)*(1-2.*current_f)\
         + 0.25*xi**2*(1-current_f)*current_h**2

    return np.array([F1, F2])

#def full_eq(delta, Y):
#    delta = np.array([delta[:N+1], delta[N+1:]])
#    delta[0, 0] = delta[0, -1] = delta[1, 0] = delta[1, -1] = 0.
#    equation = Jacobian_times_delta(Y, delta) + RHS_F(Y)
#    # res = np.linalg.norm(equation.flatten())
#    res = equation.flatten()
#    return res

def build_matrix(Y):
    """ 2N-2 x 2N-2 matrix of Jacobian.
        0 and N component of both dh and df 
        left out due to boundary conditions."""
    ret = np.zeros((2*N-2, 2*N-2))
    JacobianNOd = Jacobian_no_d(Y)
    dF1dh, dF1df = JacobianNOd[0]
    dF2dh, dF2df = JacobianNOd[1]
    for i in range(N-1):
        if i !=0:
            ret[i,i-1] += (xi[i+1]**2)*(1./deltax**2)
            ret[N-1+i,N-1+i-1] += (xi[i+1]**2)*(1./deltax**2)
        if i != N-2:
            ret[i,i+1] += (xi[i+1]**2)*(1./deltax**2)
            ret[N-1+i,N-1+i+1] += (xi[i+1]**2)*(1./deltax**2)
            ret[i,i+1] += (xi[i+1])*(2./deltax)
        ret[i,i] += dF1dh[i+1] - (xi[i+1]**2)*(2./deltax**2)- (xi[i+1])*(2./deltax)
        ret[i,N-1+i] += dF1df[i+1]
        ret[N-1+i,i] += dF2dh[i+1]
        ret[N-1+i,N-1+i] += dF2df[i+1]- (xi[i+1]**2)*(2./deltax**2)

    return ret


contd = 0
Yrun_prog = np.array([Yrun.flatten()])

def lastbutone(Y):
    F1, F2 = RHS_F(Yrun)
    ret = np.concatenate([F1[:-1], F2[:-1]])
    return np.linalg.norm(ret)

delta_fh = 1.

while np.linalg.norm(delta_fh) > 1e-8 and contd < maxiter: #RHS_F(Yrun)
    #delta_fh = opt.fsolve(full_eq, np.zeros(2*N+2), args=(Yrun,))
    delta_fh = np.linalg.solve(build_matrix(Yrun),  - np.concatenate([RHS_F(Yrun)[0,1:-1], RHS_F(Yrun)[1,1:-1]]))
    #delta_h, delta_f = delta_fh[:N+1], delta_fh[N+1:]
    delta_h, delta_f = delta_fh[:N-1], delta_fh[N-1:]
    #delta_h[0] = delta_h[-1] = 0.
    #delta_f[0] = delta_f[-1] = 0.
    delta_h = np.insert(delta_h, 0, 0.)
    delta_h = np.append(delta_h, 0.)
    delta_f = np.insert(delta_f, 0, 0.)
    delta_f = np.append(delta_f, 0.)
    Yrun += np.array([delta_h, delta_f])
    contd += 1
    Yrun_prog = np.append(Yrun_prog, [Yrun.flatten()], axis = 0)
    print("After iteration {}, the norm of deltaY is {}.".format(contd, np.linalg.norm(delta_fh)))
    #print("After iteration {}, the norm of the RHS is {}, its mean is {} and its max is {}".format(contd, np.linalg.norm(RHS_F(Yrun)), np.mean(RHS_F(Yrun)), np.max(np.abs(RHS_F(Yrun)))))
    #print("The maximum is at position {}".format(np.unravel_index(np.argmax(np.abs(RHS_F(Yrun)), axis=None), np.abs(RHS_F(Yrun)).shape)))

if contd == maxiter:
    print("Optimization terminated after maximal number of iterations ({}).".format(maxiter))
else:
    print("profile functions converged after {} iterations".format(contd))
print("The norm of the full RHS of the solution is {}, and excluding the last bin it is {}.".format(np.linalg.norm(RHS_F(Yrun)), lastbutone(Yrun)))

if plotRest:
    print("Plotting ... ")

    fig = plt.figure(dpi=150, figsize=[6., 6.])
    axis = fig.add_subplot(111)
    plt.xlim([0., L/3])
    plt.ylim([-1., 1.5])

    for i in range(contd):
        plt.plot(xi, Yrun_prog[i, :N+1], label = "iteration "+str(i+1))
    plt.title("Profile of h")
    plt.legend()
    plt.xlabel(r"$\xi$")
    plt.savefig('h_profile.png')
    plt.show()
    plt.close()

    fig = plt.figure(dpi=150, figsize=[6., 6.])
    axis = fig.add_subplot(111)
    plt.xlim([0., L/3])
    plt.ylim([-1., 1.5])

    for i in range(contd):
        plt.plot(xi, Yrun_prog[i, N+1:], label = "iteration "+str(i+1))
    plt.title("Profile of f")
    plt.legend()
    plt.xlabel(r"$\xi$")
    plt.savefig('f_profile.png')
    plt.show()
    plt.close()


# interpolated results below give wrong final integral by 5%
h_final = interp.interp1d(xi, Yrun[0], kind='cubic', fill_value=(0., 1.), bounds_error=False)
f_final = interp.interp1d(xi, Yrun[1], kind='cubic', fill_value=(0., 1.), bounds_error=False)

def EOM_der(x_coord, profile_h, profile_f):
    F1 = 2.*x_coord*derv(profile_h, x_coord) + x_coord**2*derv(profile_h, x_coord, n=2)-2.*(1-profile_f(x_coord))**2*profile_h(x_coord)\
         -mu2*x_coord**2*profile_h(x_coord)/(g**2 * v**2) - lbda*x_coord**2*profile_h(x_coord)**3 / g**2
    F2 = x_coord**2 * derv(profile_f, x_coord, n=2) - 2*profile_f(x_coord)*(1-profile_f(x_coord))*(1-2.*profile_f(x_coord))\
         + 0.25*x_coord**2*(1-profile_f(x_coord))*profile_h(x_coord)**2
    return np.array([F1, F2])

def E_sph(x_coord, profile_h, profile_f):
    ret = 4.*derv(profile_f, x_coord)**2 + (8./x_coord**2)*profile_f(x_coord)**2 * (1.-profile_f(x_coord))**2
    ret += 0.5*x_coord**2 *derv(profile_h, x_coord)**2 + (1.-profile_f(x_coord))**2*profile_h(x_coord)**2
    ret += (x_coord**2 / g**2) * (lbda/4.)*(profile_h(x_coord)**2-1.)**2
    return ret

def E_sph_disc(Y):
    current_h, current_f = Y
    ret = 4.*first_der(current_f)**2
    ret2 = (8./xi[1:]**2)*current_f[1:]**2 * (1.-current_f[1:])**2
    ret2 = np.insert(ret2, 0, 0.)
    ret += ret2
    ret += 0.5*xi**2 *first_der(current_h)**2 + (1.-current_f)**2*current_h**2
    ret += (xi**2 / g**2) * (lbda/4.)*(current_h**2-1.)**2
    return ret

energy_factor = integrate.quad(E_sph, 0., 100., args=(h_final, f_final,))
prefactor = 4.*np.pi*v / g

print("Continous sphaleron energy is {}, with prefactor {} and B-function {}".format(prefactor*energy_factor[0], prefactor, energy_factor))

E_disc = E_sph_disc(Yrun)
energy_disc = (E_disc[:-1] + E_disc[1:])*deltax / 2.

print("Discrete sphaleron energy is {}, with prefactor {} and B-function {}".format(prefactor*np.sum(energy_disc), prefactor, np.sum(energy_disc)))


def N2_tr_disc(Y):
    current_h, current_f = Y
    ret = 16.*first_der(current_f)**2
    ret2 = (32./xi[1:]**2)*current_f[1:]**2 * (1.-current_f[1:])**2
    ret2 = np.insert(ret2, 0, 0.)
    ret += ret2
    ret += xi**2 *first_der(current_h)**2 + 2.*(1.-current_f)**2*current_h**2
    return (2./3.) * ret

N_tr_disc = N2_tr_disc(Yrun)
N_tr = (N_tr_disc[:-1] + N_tr_disc[1:])*deltax /2.
N_tr = np.sum(N_tr)**(3./2.)

#def PQ_equations(Y, PQ):
#    current_h, current_f = Y
#    current_P, current_Q = PQ
#
#    eq1 = -xi * first_der(current_f) - xi**2 * second_der(current_P) - 2.*xi*first_der(current_P) + 2.*current_P + 4.*current_f*(current_f-1.)*current_P + xi**2 * current_h**2 * current_P / 4. + 2.*(2.*current_f-1.)*current_Q
#
#    eq2 = -2.*current_f*(1-current_f)- xi**2 * second_der(current_Q) - 2.*xi*first_der(current_Q) + 4.*current_Q + 8.*current_f*(current_f-1)*current_Q + xi**2 * current_h**2 * current_Q / 4. + 4.*(2.*current_f-1.)*current_P
#    return np.array([eq1, eq2])

#def build_PQ_matrix(Y):
#    """ 2N+1 x 2N+1 matrix of Jacobian.
#        the Q_0 is deleted, because it is the same
#        as P_0, yielding the matrix singular
#        """
#    current_h, current_f = Y
#
#    ret = np.zeros((2*N+2, 2*N+2))
#    for i in range(N+1):
#        ret[i,i] += 2. + 4.*current_f[i]*(current_f[i]-1) + xi[i]**2 * current_h[i]**2 / 4.
#        ret[i,i] += 2.*xi[i] / deltax
#        ret[i,i] += 2.*xi[i]**2 / deltax**2
#        if i != N:
#            ret[i,i+1] += -2.*xi[i] / deltax
#            ret[i,i+1] += -xi[i]**2 / deltax**2
#            ret[N+1+i, N+1+i+1] += -2.*xi[i] / deltax
#            ret[N+1+i, N+1+i+1] += -xi[i]**2 / deltax**2
#        if i != 0:
#            ret[i,i-1] += -xi[i]**2 / deltax**2
#            ret[N+1+i,N+1+i-1] += -xi[i]**2 / deltax**2
#
#        ret[i,i+N+1] += 2.*(2.*current_f[i] -1.)
#        ret[N+1+i,N+1+i] += 4. + 8.*current_f[i]*(current_f[i]-1) + xi[i]**2 * current_h[i]**2 / 4.
#        ret[N+1+i,N+1+i] += 2.*xi[i] / deltax
#        ret[N+1+i,N+1+i] += 2.*xi[i]**2 / deltax**2
#
#        ret[N+1+i,i] += 4.*(2.*current_f[i] -1.)
#    ret = delete(ret, N+1, N+1)
#    return ret

def build_PQ_matrix_2N(Y):
    """ 2N x 2N matrix of Jacobian.
        the Q_0 and P_0 not included, so the division by 
        xi_0 does not cause errors
        """
    current_h, current_f = Y

    ret = np.zeros((2*N, 2*N))
    for i in range(N):
        ret[i,i] += (2. + 4.*current_f[i+1]*(current_f[i+1]-1))/xi[i+1]**2 +  current_h[i+1]**2 / 4.
        ret[i,i] += 2./xi[i+1] / deltax
        #ret[i,i] += - 2./xi[i+1] / deltax
        ret[i,i] += 2. / deltax**2
        if i != N-1:
            ret[i,i+1] += -2./xi[i+1] / deltax
            ret[i,i+1] += -1. / deltax**2
            ret[N+i, N+i+1] += -2./xi[i+1] / deltax
            ret[N+i, N+i+1] += -1. / deltax**2
        if i != 0:
            #ret[i,i-1] += 2./xi[i+1] / deltax
            #ret[N+i, N+i-1] += 2./xi[i+1] / deltax
            
            ret[i,i-1] += -1. / deltax**2
            ret[N+i,N+i-1] += -1. / deltax**2

        ret[i,i+N] += 2.*(2.*current_f[i+1] -1.) / xi[i+1]**2
        ret[N+i,N+i] += (4. + 8.*current_f[i+1]*(current_f[i+1]-1))/ xi[i+1]**2 + current_h[i+1]**2 / 4.

        ret[N+i,N+i] += 2./xi[i+1] / deltax
        #ret[N+i,N+i] += - 2./xi[i+1] / deltax

        ret[N+i,N+i] += 2. / deltax**2

        ret[N+i,i] += 4.*(2.*current_f[i+1] -1.)/ xi[i+1]**2
    return ret

                     
def PQ_equations_2N(Y, PQ):
    current_h, current_f = Y
    current_P, current_Q = PQ
    #P0 = (4./3.)*current_P[1] - current_P[2]/3.
    #Q0 = (4./3.)*current_Q[1] - current_Q[2]/3.
    P0 = (1./3.)*current_P[1] + (2./3.)*current_P[2]
    Q0 = (1./3.)*current_Q[1] + (2./3.)*current_Q[2]
    #eq1 = (-first_der(current_f)/xi - second_der(current_P, upper = 0., lower=P0) - 2.*first_der_left(current_P, lower=P0)/xi + (2.*current_P + 4.*current_f*(current_f-1.)*current_P)/xi**2 + current_h**2 * current_P / 4. + 2.*(2.*current_f-1.)*current_Q/xi**2)[1:]

    #eq2 = (-2.*current_f*(1-current_f)/xi**2- second_der(current_Q, upper = 0., lower=Q0) - 2.*first_der_left(current_Q, lower=Q0)/xi + (4.*current_Q + 8.*current_f*(current_f-1)*current_Q)/xi**2 + current_h**2 * current_Q / 4. + 4.*(2.*current_f-1.)*current_P/xi**2)[1:]
    eq1 = (-first_der(current_f)/xi - second_der(current_P, upper = 0., lower=P0) - 2.*first_der(current_P, upper=0.)/xi + (2.*current_P + 4.*current_f*(current_f-1.)*current_P)/xi**2 + current_h**2 * current_P / 4. + 2.*(2.*current_f-1.)*current_Q/xi**2)[1:]

    eq2 = (-2.*current_f*(1-current_f)/xi**2- second_der(current_Q, upper = 0., lower=Q0) - 2.*first_der(current_Q, upper=0.)/xi + (4.*current_Q + 8.*current_f*(current_f-1)*current_Q)/xi**2 + current_h**2 * current_Q / 4. + 4.*(2.*current_f-1.)*current_P/xi**2)[1:]

#    eq1, eq2 = PQ_equations(Y, PQ)
#    eq1 = eq1[1:]/xi[1:]**2
#    eq2 = eq2[1:]/xi[1:]**2
    return np.array([eq1, eq2])

def PQ_equations_2N1(Y, PQ):
    """ Only add P0, not Q0"""
    current_h, current_f = Y
    current_P, current_Q = PQ
    #P0 = (4./3.)*current_P[1] - current_P[2]/3.
    #Q0 = (4./3.)*current_Q[1] - current_Q[2]/3.
    P0 = (1./3.)*current_P[1] + (2./3.)*current_P[2]
    Q0 = (1./3.)*current_Q[1] + (2./3.)*current_Q[2]

    eq1 = (-first_der(current_f)/xi - second_der(current_P, upper = 0., lower=P0) - 2.*first_der(current_P, upper=0.)/xi + (2.*current_P + 4.*current_f*(current_f-1.)*current_P)/xi**2 + current_h**2 * current_P / 4. + 2.*(2.*current_f-1.)*current_Q/xi**2)[1:]

    eq2 = (-2.*current_f*(1-current_f)/xi**2- second_der(current_Q, upper = 0., lower=Q0) - 2.*first_der(current_Q, upper=0.)/xi + (4.*current_Q + 8.*current_f*(current_f-1)*current_Q)/xi**2 + current_h**2 * current_Q / 4. + 4.*(2.*current_f-1.)*current_P/xi**2)[1:]

    eq1 = np.insert(eq1, 0,(2.*current_P + 4.*current_f*(current_f-1.)*current_P+ 2.*(2.*current_f-1.)*current_Q)[0])
    return np.array([eq1, eq2])

def build_PQ_matrix_2N1(Y):
    """ 2N+1 x 2N+1 matrix of Jacobian. P0 is included
        (multiplied by xi**2), Q_0 is not included
        """
    current_h, current_f = Y
    ret = np.zeros((2*N, 2*N))
    for i in range(N):
        ret[i,i] += (2. + 4.*current_f[i+1]*(current_f[i+1]-1))/xi[i+1]**2 +  current_h[i+1]**2 / 4.
        ret[i,i] += 2./xi[i+1] / deltax
        ret[i,i] += 2. / deltax**2
        if i != N-1:
            ret[i,i+1] += -2./xi[i+1] / deltax
            ret[i,i+1] += -1. / deltax**2
            ret[N+i, N+i+1] += -2./xi[i+1] / deltax
            ret[N+i, N+i+1] += -1. / deltax**2
        if i != 0:
            ret[i,i-1] += -1. / deltax**2
            ret[N+i,N+i-1] += -1. / deltax**2

        ret[i,i+N] += 2.*(2.*current_f[i+1] -1.) / xi[i+1]**2
        ret[N+i,N+i] += (4. + 8.*current_f[i+1]*(current_f[i+1]-1))/ xi[i+1]**2 + current_h[i+1]**2 / 4.

        ret[N+i,N+i] += 2./xi[i+1] / deltax

        ret[N+i,N+i] += 2. / deltax**2

        ret[N+i,i] += 4.*(2.*current_f[i+1] -1.)/ xi[i+1]**2

    ret = np.insert(ret, 0, np.zeros(2*N), axis=1)
    ret = np.insert(ret, 0, np.zeros(2*N+1), axis=0)
    ret[0,0] += (2. + 4.*current_f[0]*(current_f[0]-1))
    ret[0,N+1] += 2.*(2.*current_f[0] -1.)
    ret[1,0] += -1. / deltax**2
    return ret

#np.set_printoptions(threshold=np.inf)
#print(build_PQ_matrix(Yrun))
#np.set_printoptions(threshold=1000)

contd = 0
PQrun_prog = np.array([PQrun.flatten()])

delta_PQ = 1.

while np.linalg.norm(delta_PQ) > 1e-8 and contd < maxiter:
    #delta_PQ = np.linalg.solve(build_PQ_matrix(Yrun),  - np.concatenate([PQ_equations(Yrun, PQrun)[0], PQ_equations(Yrun, PQrun)[1,1:]]))
    #delta_P, delta_Q = delta_PQ[:N+1], delta_PQ[N+1:]

    #delta_PQ = np.linalg.solve(build_PQ_matrix_2N(Yrun),  - np.concatenate([PQ_equations_2N(Yrun, PQrun)[0], PQ_equations_2N(Yrun, PQrun)[1]]))
    #delta_P, delta_Q = delta_PQ[:N], delta_PQ[N:]
    #delta_P = np.insert(delta_P, 0, 0.) #delta_P[0])
    #delta_Q = np.insert(delta_Q, 0, 0.) #delta_Q[0])
    
    delta_PQ = np.linalg.solve(build_PQ_matrix_2N1(Yrun),  - np.concatenate([PQ_equations_2N1(Yrun, PQrun)[0], PQ_equations_2N1(Yrun, PQrun)[1]]))
    delta_P, delta_Q = delta_PQ[:N+1], delta_PQ[N+1:]
    delta_Q = np.insert(delta_Q, 0, 0.)
    
    PQrun += np.array([delta_P, delta_Q])
    PQrun[1,0] = (4./3.)*PQrun[1,1] - PQrun[1,2]/3.
    #PQrun[:,0] = (4./3.)*PQrun[:,1] - PQrun[:,2]/3.
    #PQrun[:,0] = PQrun[:,1]
    
    contd += 1
    PQrun_prog = np.append(PQrun_prog, [PQrun.flatten()], axis = 0)
    print("After iteration {}, the norm of deltaPQ is {}.".format(contd, np.linalg.norm(delta_PQ)))
    #np.set_printoptions(threshold=np.inf)
    #print(PQ_equations_2N(Yrun, PQrun))
    #np.set_printoptions(threshold=1000)

if contd == maxiter:
    print("Optimization terminated after maximal number of iterations ({}).".format(maxiter))
else:
    print("profile functions converged after {} iterations".format(contd))
print("The norm of the full RHS of the solution is {}.".format(np.linalg.norm(PQ_equations_2N(Yrun, PQrun))))

np.set_printoptions(threshold=np.inf)
print(PQ_equations_2N1(Yrun, PQrun)[0])
print("***")
print(PQ_equations_2N1(Yrun, PQrun)[1])
print("***")
print("***")
print(PQrun[0,:3])
print("***")
print(PQrun[1,:3])
np.set_printoptions(threshold=1000)
# check term by term of first entry

def N2_rot_disc(Y, PQ):
    current_h, current_f = Y
    current_P, current_Q = PQ
    ret = 0.5*xi**2 * first_der(current_f)**2
    ret += 2.*current_f**2 * (1.-current_f)**2
    ret += xi**2 *current_h**2 *(1.-current_f)**2/8.
    ret += -2.*xi*first_der(current_f)*current_P
    ret += -2.*current_f*(1.-current_f)*current_Q
    return (32./3.) * ret

N_rot_disc = N2_rot_disc(Yrun, PQrun)
N_rot = (N_rot_disc[:-1] + N_rot_disc[1:])*deltax /2.
N_rot = np.sum(N_rot)**(3./2.)

if plotRest:
    print("Plotting ... ")

    fig = plt.figure(dpi=150, figsize=[6., 6.])
    axis = fig.add_subplot(111)
    plt.xlim([0., L])
    plt.ylim([-0.5, 1.5])

    for i in range(contd):
        plt.plot(xi, PQrun_prog[i, :N+1], label = "iteration "+str(i+1))
    plt.title("P function")
    plt.legend()
    plt.xlabel(r"$\xi$")
    plt.savefig('P_solution.png')
    plt.show()
    plt.close()

    fig = plt.figure(dpi=150, figsize=[6., 6.])
    axis = fig.add_subplot(111)
    plt.xlim([0., L])
    plt.ylim([-0.5, 1.5])

    for i in range(contd):
        plt.plot(xi, PQrun_prog[i, N+1:], label = "iteration "+str(i+1))
    plt.title("Q function")
    plt.legend()
    plt.xlabel(r"$\xi$")
    plt.savefig('Q_solution.png')
    plt.show()
    plt.close()


print("Prefactors: \n{} is {}\n{} is {}".format("N_tr", N_tr, "N_rot", N_rot))


if plotB:
    print("Fitting B(mh/mW) for mh in [25, 250] GeV and plotting result.")

    # polynomial fit between mh = 25 and mh = 250 GeV:

    mh_scan = 25*np.linspace(1., 10., 10)
    x_scan = mh_scan/mW

    # obtained with code above
    B_old = np.array([1.6086009936278491, 1.6861008568157216, 1.746769376192876, 1.7969078072411062, 1.8396908546712738, 1.876994058544048, 1.910034797104835, 1.9396542566538713, 1.9664606010930532, 1.9909090350163061])
    B = np.array([1.6475149170026702, 1.7362198282796213, 1.806539396742029, 1.8650183393645792, 1.915023905352053, 1.9585913082279054, 1.9970723182404506, 2.031421831475817, 2.062344598521245, 2.0903781629255094])

    my_poly = np.polyfit(x_scan, B, 2)
    my_old_poly = np.polyfit(x_scan, B_old, 2)

    # from hep-ph/9901312:
    quiros_poly = np.array([-0.05, 0.32, 1.58])

    fig = plt.figure(dpi=150, figsize=[6., 6.])
    axis = fig.add_subplot(111)
    plt.xlim([0., 3.5])
    plt.ylim([1.4, 2.5])

    #axis.set_yticks(np.linspace(1., 2.5, 16))
    #axis.set_yticklabels(np.linspace(1., 2.5, 16))

    axis.yaxis.set_major_locator(MultipleLocator(0.2))
    axis.yaxis.set_minor_locator(MultipleLocator(0.1))
    axis.yaxis.set_major_formatter(FormatStrFormatter('%.1f'))
    plt.plot(x_scan, my_poly[0]*x_scan**2 + my_poly[1]*x_scan + my_poly[2], label = "my fit")
    plt.plot(x_scan, my_old_poly[0]*x_scan**2 + my_old_poly[1]*x_scan + my_old_poly[2], label = "my old fit")
    plt.plot(x_scan, quiros_poly[0]*x_scan**2 + quiros_poly[1]*x_scan + quiros_poly[2], label = "fit of Quiros [hep-ph/9901312]")
    plt.plot(x_scan, B, marker = 'x', color = 'k', label = "my results", linestyle="None")
    plt.plot(x_scan, B_old, marker = 'x', color = 'r', label = "my old results", linestyle="None")
    plt.title("Evaluation of B")
    plt.legend()
    plt.xlabel(r"$\frac{m_h}{m_W}$")
    plt.ylabel(r"$B\left(\frac{m_h}{m_W}\right)$")
    plt.savefig('B_fit.png')
    plt.show()
    plt.close()
    print((my_poly[0]*x_scan**2+ my_poly[1]*x_scan + my_poly[2])/ (quiros_poly[0]*x_scan**2 + quiros_poly[1]*x_scan + quiros_poly[2]))
