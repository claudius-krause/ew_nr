""" Digitized data of figure 5 (N_tr and N_rot) and 6 (omega_minus) 
    of https://journals.aps.org/prd/pdf/10.1103/PhysRevD.41.647
    using g3data [http://manpages.ubuntu.com/manpages/xenial/man1/g3data.1.html]
    and table 1 (kappa) of hep-th/9402078
"""

import numpy as np
import scipy.interpolate as interp
import matplotlib.pyplot as plt

interpolation_level = 'linear'

class SphaleronParameters:
    """ returns interpolated values for the prefactor of the sphaleron rate. 
        N_tr: normalization of translations
        N_rot: normalization of rotations
        omega_minus: frequency of unstable mode
        note that omega_minus of the decay rate actually is
        np.sqrt(returned_result) * g * v
    """

    def __init__(self):
        self.xi_grid = np.sort(np.array([10**j * i for i in np.arange(1, 10) for j in [-2, -1, 0]]))
        self.xi_grid = np.append(self.xi_grid, 10.)
    
    def N_tr_disc(self):
        ret = np.array([6.1212997737, 6.2485245748, 6.34292845646, 6.4667941869, 6.5293568779, 6.5914996850, 6.6838041475, 6.7152254542, 6.7769483776, 6.8080897618, 7.23847068978, 7.48452261541, 7.69915323427, 7.85262077492, 7.97536681518, 8.09797289417, 8.18999743404, 8.28202197392, 8.37376659124, 8.9253539854, 9.26231076069, 9.5072429961, 9.72131376986, 9.87436142667, 9.99696750566, 10.1192936621, 10.2416198185, 10.2727612027])
        return ret

    def N_tr_interp(self, xi):
        ret = interp.interp1d(self.xi_grid, self.N_tr_disc(), kind=interpolation_level)
        return ret(xi)
    
    def N_rot_disc(self):
        ret = np.array([15.1203217622, 14.686854751, 14.3051596535, 14.0109436565, 13.7455599855, 13.5085175788, 13.3315951315, 13.1536905611, 13.0056004396, 12.8575103181, 11.9391551404, 11.4064235522, 11.0819020449, 10.8160273124, 10.6395959265, 10.4929789896, 10.3453799296, 10.2280863801, 10.1401162179, 9.58591822657, 9.35575068106, 9.2130622362, 9.09871505571, 9.04399677302, 9.01811081621, 9.02253036982, 8.99566228998, 8.93848869974])
        return ret
    
    def N_rot_interp(self, xi):
        ret = interp.interp1d(self.xi_grid, self.N_rot_disc(), kind=interpolation_level)
        return ret(xi)
    
    def omega_minus_disc(self):
        ret = np.array([0.445119580012, 0.462424654871, 0.479545012639, 0.4923779895, 0.505172078553, 0.517946723702, 0.526472875753, 0.534999027805, 0.543505735952, 0.552012444099, 0.619949445849, 0.666320125996, 0.699816902392, 0.733352470654, 0.762698517375, 0.787855042555, 0.804613128719, 0.829789049832, 0.842357614455, 0.989243015525, 1.09842272272, 1.19925248074, 1.29171289364, 1.379983785, 1.45564731987, 1.53973838765, 1.61122209895, 1.66587014035])
        return ret

    def omega_minus_interp(self, xi):
        ret = interp.interp1d(self.xi_grid, self.omega_minus_disc(), kind=interpolation_level)
        return ret(xi)
        
    def ln_kappa_disc_line(self):
        # from table:
        # ret = np.exp(np.array([-46.80,-31.00,-22.30,-13.64,-9.64,-5.96,-5.14]))
        # from figure:
        ret = np.array([-45.1177158018, -29.9543286693, -22.3143931019, -18.0941852753, -15.128265058, -12.7323836897, -11.3628754619, -9.99311455734, -8.96573070971, -6.11023025173, -4.62398534475, -3.1357190234, -2.33321752314, -1.98780834465, -1.64290451976, -1.4124632829, -1.18227472285, -0.950822778813, -0.602128801996, -0.36941347399, -0.248886642873, -0.244085783772, -0.126338397397, -0.12330627586, -0.0045481823063, 0.112946527273])
        return ret

    def ln_kappa_interp_line(self, xi):
        # from table:
        # self.kappa_grid = np.array([.02,.031,.045,.08,.125,.281,.5])
        # from figure:
        self.kappa_grid_line = np.array([0.0200849631877, 0.0301326482141, 0.0398419983979, 0.0498094963753, 0.0595884201245, 0.0702649740359, 0.0792579776458, 0.0907818021332, 0.100897077916, 0.149598812114, 0.199675748628, 0.301256364417, 0.402520705549, 0.498522597117, 0.598794826199, 0.697659639765, 0.800492823744, 0.991584819881, 1.49864513868, 2.00414813551, 2.98399882767, 3.99191480469, 5.02205205888, 6.03534095553, 8.07251057462, 10.0013091578])
        ret = interp.interp1d(self.kappa_grid_line, self.ln_kappa_disc_line(), kind=interpolation_level)
        return ret(xi)

    def ln_kappa_disc(self):
        # from table:
        ret = np.array([-46.80,-31.00,-22.30,-13.64,-9.64,-5.96,-5.14])
        return ret

    def ln_kappa_interp(self, xi):
        # from table:
        self.kappa_grid = np.array([.02,.031,.045,.08,.125,.281,.5])
        fill_value = (self.ln_kappa_disc()[0], self.ln_kappa_disc()[-1])
        ret = interp.interp1d(self.kappa_grid, self.ln_kappa_disc(), kind=interpolation_level, bounds_error=False, fill_value=fill_value)
        return ret(xi)

    def prefactor_1(self, xi):
        """ square braket of 1708.03061, eq. 36 """
        ret = 16.*np.pi**2 * self.N_tr_interp(xi) * self.N_rot_interp(xi) * np.sqrt(self.omega_minus_interp(xi))
        return ret

param = SphaleronParameters()

print("The SM values (lambda/g^2 = 0.3) are:\n{}: {}\n{}: {}\n{}: {}\n{}: {}\n{}: {}".format("N_tr", param.N_tr_interp(0.3), "N_rot", param.N_rot_interp(0.3),"omega_minus^2", param.omega_minus_interp(0.3),"ln kappa",param.ln_kappa_interp(0.3),"kappa",np.exp(param.ln_kappa_interp(0.3)) ))

print("Prefactor in SM, as defined in 1708.03061 eq. 36 is {}".format(param.prefactor_1(0.3)))

#print("***")
#lg2 = 1.
#print("The values for lambda/g^2 = {} are:\n{}: {}\n{}: {}\n{}: {}\n{}: {}\n{}: {}".format(lg2, "N_tr", param.N_tr_interp(lg2), "N_rot", param.N_rot_interp(lg2),"omega_minus^2", param.omega_minus_interp(lg2),"ln kappa",param.ln_kappa_interp(lg2),"kappa",np.exp(param.ln_kappa_interp(lg2))))

# control plots

xi_pts = np.logspace(-2., 1., 201)
xi_pts2 = np.logspace(-1.65, 1., 201)

# rotations and translations
fig = plt.figure(dpi=150, figsize=[6., 6.])
axis = fig.add_subplot(111)
plt.xlim([1e-2, 1e1])
plt.ylim([6., 16.])

plt.xscale('log')

plt.plot(xi_pts, param.N_rot_interp(xi_pts), label = "N_rot interpolation ", color = 'k')
plt.plot(param.xi_grid, param.N_rot_disc(), label = "N_rot source", color ='k', marker='x', linestyle="None")

plt.plot(xi_pts, param.N_tr_interp(xi_pts), label = "N_tr interpolation ", color = 'b')
plt.plot(param.xi_grid, param.N_tr_disc(), label = "N_tr source", color ='b', marker='x', linestyle="None")

plt.title("Normalizations")
plt.legend()
plt.xlabel(r"$\lambda / g^2$")
plt.savefig('N_digit.png')
plt.show()
plt.close()


# omega minus
fig = plt.figure(dpi=150, figsize=[6., 6.])
axis = fig.add_subplot(111)
plt.xlim([1e-2, 1e1])
plt.ylim([.4, 1.8])

plt.xscale('log')

plt.plot(xi_pts, param.omega_minus_interp(xi_pts), label = r"$\omega_-$ interpolation ", color = 'k')
plt.plot(param.xi_grid, param.omega_minus_disc(), label = r"$\omega_-$ source", color ='k', marker='x', linestyle="None")

plt.title(r"$\omega_-$")
plt.legend()
plt.xlabel(r"$\lambda / g^2$")
plt.ylabel(r"$\omega_-^2 / (gv)^2$")
plt.savefig('omega_digit.png')
plt.show()
plt.close()


# kappa
fig = plt.figure(dpi=150, figsize=[6., 6.])
axis = fig.add_subplot(111)
plt.xlim([1e-2, 1e1])
plt.ylim([-50., 5])

plt.xscale('log')

plt.plot(xi_pts2, param.ln_kappa_interp_line(xi_pts2), label = r"$\kappa$ interpolation of hep-th/9402078", color = 'k')
plt.plot(param.kappa_grid_line, param.ln_kappa_disc_line(), label = r"$\kappa$ source for digitization of hep-th/9402078", color ='k', marker='x', linestyle="None")

plt.plot(param.kappa_grid, param.ln_kappa_disc(), label = r"$\kappa$ points of hep-th/9402078", color ='b', marker='s', linestyle="None", markersize=4)
plt.plot(xi_pts, param.ln_kappa_interp(xi_pts), label = r"suggested $\kappa$ interpolation", color = 'b')

axis.fill_between(xi_pts, param.ln_kappa_interp(xi_pts)-np.log(100.), param.ln_kappa_interp(xi_pts)+np.log(100.), alpha=0.2, color='b', label=r'uncertainty of suggested $\kappa: [0.01\kappa, 100 \kappa]$')

axis.fill_between([1e-2, 1e1], [np.log(1e-1),np.log(1e-1)], [np.log(1e-4),np.log(1e-4)], alpha=0.2, color='green', label=r'$10^{-4} < \kappa < 10^{-1}$')

plt.title(r"$\kappa$")
plt.legend(loc='lower right')
plt.xlabel(r"$\lambda / g^2$")
plt.ylabel(r"$\ln{\kappa}$")
plt.savefig('kappa_digit.png')
plt.show()
plt.close()

# prefactor of 1708.03061
fig = plt.figure(dpi=150, figsize=[6., 6.])
axis = fig.add_subplot(111)
plt.xlim([1e-2, 1e1])
plt.xscale('log')

plt.plot(xi_pts, param.prefactor_1(xi_pts), label = "prefactor of 1708.03061", color = 'k')

plt.title("Prefactor")
plt.legend()
plt.xlabel(r"$\lambda / g^2$")
plt.savefig('prefactor_digit.png')
plt.show()
plt.close()
