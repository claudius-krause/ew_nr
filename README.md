## A New Approach to Electroweak Symmetry Non-Restoration

by Marcela Carena, Claudius Krause, Zhen Liu, Yikun Wang </br>

This is the code used to obtain the results of our paper.  </br>

"A New Approach to Electroweak Symmetry Non-Restoration", </br>
[arXiv:2104.00638](https://arxiv.org/abs/2104.00638)