""" This script evaluates the scalar potential at a given field configuration 
    and temperature and writes all contributions to a file. It will serve as 
    a cross check to Yikun's implementation"""

import numpy as np
import pandas as pd

import helper.helper as helper
from model.Inert2HDM_N import model_2HDM_N

WHICH_DAISY = 0.

num_chi = 100
model_parameters = {'b2': 100**2, 'b4': 0.126, 'c2': 10**2, 'c4': 1.,
                    'a2': -0.02, 'ct': -0.06, 'dt': 0., 'at': 0., 'c5': 0.,
                    'N': num_chi, 'use_Daisy': WHICH_DAISY,
                    'use_CW_in_delta_m':False,
                    'use_CW': 1., 'use_RGE': False}

model = model_2HDM_N(**model_parameters)

temp = 200.

# start with all 8 combinations of Higgs, Inert and Singlet at value 100. 
field = np.zeros((8, 108))
field_content = helper.guess_plain(100.)
field[..., 0] = field_content[..., 0]
field[..., 4] = field_content[..., 1]
field[..., 8:] = np.ones((1, model.N)) * field_content[..., 2, np.newaxis]

# add 3 random points with non-zero, but equal GBs to the list:
for _ in range(3):
    higgs = 1e3 * np.random.rand()
    higgsGB = 1e3 * np.random.rand()
    inert = 1e3 * np.random.rand()
    inertGB = 1e3 * np.random.rand()
    chi = 1e3 * np.random.rand()

    new = np.concatenate([[higgs], 3*[higgsGB], [inert], 3*[inertGB], model.N*[chi]])
    field = np.append(field, new[np.newaxis, ...], axis=0)

for _ in range(3):
    higgs = 1e3 * np.random.rand()
    higgsGB = 0.
    inert = 1e3 * np.random.rand()
    inertGB = 0.
    chi = 1e3 * np.random.rand()

    new = np.concatenate([[higgs], 3*[higgsGB], [inert], 3*[inertGB], model.N*[chi]])
    field = np.append(field, new[np.newaxis, ...], axis=0)

# add points by hand for minima comparison:
def add_specific(higgs, inert, chi, field):
    higgsGB = 0.
    inertGB = 0.
    new = np.concatenate([[higgs], 3*[higgsGB], [inert], 3*[inertGB], model.N*[chi]])
    field_add = np.append(field, new[np.newaxis, ...], axis=0)
    return field_add

# Claudius minima:
field = add_specific(1.67901904e-03, 2.95500000e+02, 2.95000000e+01,field)

# Yikun's points:
"""
field = add_specific(0.014196,0.0023624, -0.33594, field)
field = add_specific(-177.6, 0.14208,16.89, field)
field = add_specific(-0.75303,  -18.606, -18.831, field)
field = add_specific(-0.00024118, 0.0003635, 18.953, field)
field = add_specific(-1.498, -15.938, 18.961, field)
field = add_specific(202.48, 0.000050378, 21.089, field)
field = add_specific(0.084048, 21.428, 18.96, field)
field = add_specific(204., 198.57, 19.66, field)
"""

field = add_specific(0.0023624, 0.014196,  -0.33594, field)
field = add_specific(0.14208,-177.6, 16.89, field)
field = add_specific(-18.606,-0.75303, -18.831, field)
field = add_specific(0.0003635,-0.00024118,  18.953, field)
field = add_specific(-15.938,-1.498,  18.961, field)
field = add_specific( 0.000050378,202.48, 21.089, field)
field = add_specific(21.428,0.084048,  18.96, field)
field = add_specific(198.57, 204., 19.66, field)
    
short_field = helper.convert_to_short(field)
    
# create pandas dataframe with field entries
output = pd.DataFrame(field[...,[0,1,4,5,8]])
output.columns = ['    Higgs   ', '    HiggsGB   ', '    Inert   ', '   InertGB   ', '   Chi   ']
print("DataFrame created")

# add temperature entry
output[' temperature'] = temp
print("Temperature added")

# add ren scale entry
ren_scale = model.current_scale(short_field)
output['   ren scale   '] = ren_scale
print("Renormalization scale added")

# add V0 entry
V0 = []
for index, single_field in enumerate(short_field):
    V0.append(model.V0(single_field, scale=ren_scale[index]))
output['   V0   '] = V0
print("V0 added")

# add thermal masses:
deltaM = model.delta_m_daisy(field, temp, scale=ren_scale)
output[' dM-Higgs '] = deltaM[..., 0]
output['dM-HiggsGB1'] = deltaM[..., 1]
output['dM-HiggsGB2'] = deltaM[..., 2]
output['dM-HiggsGB3'] = deltaM[..., 3]
output[' dM-Inert '] = deltaM[..., 4]
output['dM-InertGB1'] = deltaM[..., 5]
output['dM-InertGB2'] = deltaM[..., 6]
output['dM-InertGB3'] = deltaM[..., 7]
output['dM-singlet'] = deltaM[..., 8]
print("deltaM added")

# add VCW entry
VCW = []
for index, single_field in enumerate(short_field):
    bosons = model.boson_massSq(single_field, temp, scale=ren_scale[index], deltaM=deltaM[index])
    fermions = model.fermion_massSq(single_field, scale=ren_scale[index])
    VCW.append(model.V1(bosons, fermions, scale=ren_scale[index]))
output['   V_CW   '] = VCW
print("V_CW added")

# add Vth entry
Vth = []
for index, single_field in enumerate(short_field):
    bosons = model.boson_massSq(single_field, temp, scale=ren_scale[index], deltaM=deltaM[index])
    fermions = model.fermion_massSq(single_field, scale=ren_scale[index])
    Vth.append(model.V1T(bosons, fermions, np.array([temp]))[0])
output['   V_th   '] = Vth
print("V_th added")

# add bosonic mass eigenvalues
eig_vals = []
for index, single_field in enumerate(short_field):
    masses, _, _ = model.boson_massSq(single_field, temp, scale=ren_scale[index], deltaM=deltaM[index])
    eig_vals.append(masses)
eig_vals = np.array(eig_vals)
for i in range(8+num_chi+5):
    output['  eig-'+str(i)+'  '] = eig_vals[..., i]
print("Mass eigenvalues added")

# add bosonic diagonal elements
diag_vals = []
for index, single_field in enumerate(field):
    FULL_MATRIX = model.Full_Matrix(single_field, scale=ren_scale[index])
    for i in range(num_chi+8):
        FULL_MATRIX[i, i] += deltaM[index, i]
    diag_vals.append(FULL_MATRIX.diagonal())
diag_vals = np.array(diag_vals)
for i in range(8+1):
    output['  diag-'+str(i)+'  '] = diag_vals[..., i]
print("Mass diagonal values added")

for i in range(num_chi):
    if (diag_vals[..., 8] != diag_vals[..., 8+i]).any():
        print("Diagonal entries of singlets not equal")

file_name = 'Cross_check_Yikun/potential_daisy_'+str(WHICH_DAISY)+'.txt'
output.to_csv(file_name, sep='\t', float_format='%.8e')
print("Values saved to {}".format(file_name))
