""" Contains the high-T expansion of Jb and Jf. """

import numpy as np

gammaE = 0.5772156649
af = np.pi**2 * np.exp(1.5-(2*gammaE))
ab = 16. * af

def Jb_high(x, subleading=1.):
    ret = np.real(-np.pi**4./45. + np.pi**2*(x/12.) -\
                subleading*(np.pi/6.)*(np.sqrt(x+0j))**3  -\
                subleading*(1./32.)* x**2 * np.log((x+0j)/ab))
    return ret

def Jf_high(x, subleading=1.):
    ret = np.real(-7.*np.pi**4/360. + np.pi**2*x/24. +\
                subleading*(1./32.)* x**2 * np.log((x+0j)/af))
    return ret
