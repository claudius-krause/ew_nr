""" Contains helper functions. """

import numpy as np
import pickle
import os
from scipy.misc import central_diff_weights
from scipy import optimize

def partial_derivative(func, x0, var, dx=1e-3, n=1, args=(), order=3, **kwargs):
    """
    Modified from scipy.misc.derivative().
    Find the n-th partial derivative of a function at a point along the var
    direction.
    Given a function, use a central difference formula with spacing `dx` to
    compute the `n`-th derivative at `x0`.
    Parameters
    ----------
    func : function
        Input function.
    x0 : float
        The point at which `n`-th derivative is found.
    var : int
        The variable to take the `n`-th derivative with respect to
    dx : float, optional
        Spacing. Default is 1e-3
    n : int, optional
        Order of the derivative. Default is 1.
    args : tuple, optional
        Arguments
    order : int, optional
        Number of points to use, must be odd.
    Notes
    -----
    Decreasing the step size too small can result in round-off error.
    Example
    -------
    >>> def f(x):
    ...    return x[...,0]**3 + x[...,0]**2*x[...,1]
    >>> partial_derivative(f, var=0, x0=np.array([2.,1.]), n=1)
    16.0
    """
    if order < n + 1:
        raise ValueError("'order' (the number of points used to compute the derivative), "
                         "must be at least the derivative order 'n' + 1.")
    if order % 2 == 0:
        raise ValueError("'order' (the number of points used to compute the derivative) "
                         "must be odd.")
    # pre-computed for n=1 and 2 and low-order for speed.
    if n == 1:
        if order == 3:
            weights = np.array([-1, 0, 1])/2.0
        elif order == 5:
            weights = np.array([1, -8, 0, 8, -1])/12.0
        elif order == 7:
            weights = np.array([-1, 9, -45, 0, 45, -9, 1])/60.0
        elif order == 9:
            weights = np.array([3, -32, 168, -672, 0, 672, -168, 32, -3])/840.0
        else:
            weights = central_diff_weights(order, 1)
    elif n == 2:
        if order == 3:
            weights = np.array([1, -2.0, 1])
        elif order == 5:
            weights = np.array([-1, 16, -30, 16, -1])/12.0
        elif order == 7:
            weights = np.array([2, -27, 270, -490, 270, -27, 2])/180.0
        elif order == 9:
            weights = np.array([-9, 128, -1008, 8064, -14350, 8064, -1008, 128, -9])/5040.0
        else:
            weights = central_diff_weights(order, 2)
    else:
        weights = central_diff_weights(order, n)
    val = 0.0
    ho = order >> 1
    for k in range(order):
        x_shift = np.copy(x0)
        x_shift[..., var] = x0[..., var]+(k-ho)*dx
        val += weights[k]*func(x_shift, *args, **kwargs)
    return val / np.prod((dx,)*n, axis=0)

def convert_to_full(short_field):
    """ converts short (2 + num_chi) field to full (8 + num_chi) field using 0s. """
    field_shape = list(short_field.shape)
    field_shape[-1] += 6
    field_shape = tuple(field_shape)
    full_field = np.zeros(field_shape)
    full_field[..., 0] = short_field[..., 0]
    full_field[..., 4] = short_field[..., 1]
    full_field[..., 8:] = short_field[..., 2:]
    return full_field

def convert_to_short(full_field):
    """ converts full (8 + num_chi) field to short (2 + num_chi) field ignoring GBs. """
    field_shape = list(full_field.shape)
    field_shape[-1] -= 6
    field_shape = tuple(field_shape)
    short_field = np.zeros(field_shape)
    short_field[..., 0] = full_field[..., 0]
    short_field[..., 1] = full_field[..., 4]
    short_field[..., 2:] = full_field[..., 8:]
    return short_field

def jacobian(func, point, dim, dx):
    """ Jacobian for Newton's method.
        func: callable function
        point: point at which Jacobian is evaluated
        dim: (int) dimensionality of point
        dx: delta x for numerical derivative
    """
    # TODO: there is a more clever version that reduces the
    # number of function calls of func(point)
    ret = np.zeros((dim, dim))
    for i in range(9):
        ret[i] = partial_derivative(func, point, var=i, dx=dx)[:dim]
    return ret.T

def make_tuple(field, temp):
    """ transforms field and temperature to (integer rounded) tuple
        used in store_delta_m dictionaries
    """
    ret = np.around(field, 0).astype(int)
    ret = np.insert(ret, 0, np.around(temp, 0).astype(int))
    ret = tuple(ret)
    return ret

def make_param_string(dictionary):
    """ creates a string that contains all parameters of a given dictionary."""
    ret = ""
    length = 1
    for param, value in dictionary.items():
        ret += str(param) + '=' + str(value) + ' '
        if length % 5 == 0:
            ret += '\n'
        length += 1
    return ret

def make_save_string(model):
    """ extracts couplings and their value from the model instance,
        adds CW flag to it, returns string"""
    ret = "saved_dicts/"
    for param in model.couplings_str:
        value = np.around(eval('model.'+param), 3)
        ret += str(param) + str(value) + '_'
    ret += 'N' + str(model.N) + 'RGE' + str(model.use_RGE)
    return ret+'/'

def save_delta_dics(model):
    """ saves all deltaM dictionaries to a file"""
    folder = make_save_string(model)
    if not os.path.exists(folder):
        os.mkdir(folder)
    with open(folder +  'deltaM2.pkl', 'wb') as fil:
        pickle.dump(model.store_delta_m_2, fil, pickle.HIGHEST_PROTOCOL)
    with open(folder +  'deltaM3.pkl', 'wb') as fil:
        pickle.dump(model.store_delta_m_3, fil, pickle.HIGHEST_PROTOCOL)
    with open(folder +  'deltaM4.pkl', 'wb') as fil:
        pickle.dump(model.store_delta_m_4, fil, pickle.HIGHEST_PROTOCOL)
    print("Dictionaries saved to {}".format(folder))

def load_delta_dics(model):
    """ loads previously saved deltaM dictionaries from file """
    folder = make_save_string(model)
    if not os.path.exists(folder):
        print("Folder {} does not exist! Nothing loaded".format(folder))
        return
    if os.path.exists(folder +  'deltaM2.pkl'):
        with open(folder +  'deltaM2.pkl', 'rb') as fil:
            model.store_delta_m_2 = pickle.load(fil)
        print('deltaM2.pkl loaded!')
    if os.path.exists(folder +  'deltaM3.pkl'):
        with open(folder +  'deltaM3.pkl', 'rb') as fil:
            model.store_delta_m_3 = pickle.load(fil)
        print('deltaM3.pkl loaded!')
    if os.path.exists(folder +  'deltaM4.pkl'):
        with open(folder +  'deltaM4.pkl', 'rb') as fil:
            model.store_delta_m_4 = pickle.load(fil)
        print('deltaM4.pkl loaded!')

def clean_delta_dics(model):
    """ deletes previously saved deltaM and minima dictionaries and their folder """
    folder = make_save_string(model)
    if os.path.exists(folder):
        os.remove(folder +  'deltaM2.pkl')
        os.remove(folder +  'deltaM3.pkl')
        os.remove(folder +  'deltaM4.pkl')
        if os.path.exists(folder +  'minima_wd0.pkl'):
            os.remove(folder +  'minima_wd0.pkl')
            os.remove(folder +  'minima_wd1.pkl')
            os.remove(folder +  'minima_wd2.pkl')
            os.remove(folder +  'minima_wd3.pkl')
            os.remove(folder +  'minima_wd4.pkl')
        os.rmdir(folder)

def save_minima_dic(model):
    """ saves the minima_dict to a file"""
    folder = make_save_string(model)
    if not os.path.exists(folder):
        os.mkdir(folder)
    for i in range(5):
        with open(folder +  'minima_wd'+ str(i) +'.pkl', 'wb') as fil:
            current_dic = eval('model.minima_dict_wd'+str(i))
            pickle.dump(current_dic, fil, pickle.HIGHEST_PROTOCOL)
    print("Minima dictionary saved to {}".format(folder))

def load_minima_dic(model):
    """ loads previously saved minima_dict dictionaries from file """
    folder = make_save_string(model)
    if not os.path.exists(folder):
        print("Folder {} does not exist! Nothing loaded".format(folder))
        return
    file_names = ['minima_wd'+str(i)+'.pkl' for i in range(5)]
    dict_array = []
    for file_name in file_names:
        if os.path.exists(folder + file_name):
            with open(folder +  file_name, 'rb') as fil:
                dict_array.append(pickle.load(fil))
            print(file_name + ' loaded!')
        else:
            print(file_name + ' does not exist')
    for i, dic in enumerate(dict_array):
        model.__dict__[f'minima_dict_wd{i}'] = dic

def guess_plain(value):
    """ Returns all 8 possible combinations of zero and non-zero field
        directions as guess for minima finder """
    ret = []
    for i in range(8):
        ret.append([float(k) for k in np.binary_repr(i, 3)])
    ret = np.array(ret)
    return value*ret

def trace_global_min(model, T_start, T_end, T_interval, rounded_digits=0):
    """ Finds global minimum for [T_start, T_start+T_interval, ..., T_end]
        and returns a dictionary of field configurations, for T rounded to
        rounded_digits digits """
    load_delta_dics(model)
    load_minima_dic(model)
    if T_interval=='log':
        T_scan = [0.]
        current_T = 100.
        while current_T<T_end:
            for i in range(1,6):
                T_scan.append(current_T*i)
            current_T *= 10
        T_scan.append(current_T)
        T_scan = np.array(T_scan)
    elif T_interval=='log_final':
        T_scan = np.concatenate([np.arange(0., 500., 5.), np.arange(500., 1e3, 50.), 
                                 np.arange(1e3, 5e3, 5e2), np.arange(5e3, 1e4, 1e3), 
                                 np.arange(1e4, 5e4, 5e3), np.arange(5e4, 1e5, 1e4), 
                                 np.arange(1e5, 6e5, 1e5)])
    else:
        T_scan = np.arange(T_start, T_end+T_interval, T_interval)
    print("Scanning the temperatures: {}".format(T_scan))
    LIST_OF_APPROX_MINIMA = model.approxZeroTMin()
    def short_Vtot(x, T, run_scale):
        """ wrapper for Vtot"""
        if (np.abs(x) > model.scale_end_upper).any():
            return -1e99
        x_long = np.concatenate([[x[0]], [x[1]], model.N*[x[2]]])
        ret = model.Vtot(np.abs(x_long), T, run_scale=run_scale)
        return ret
    for TEMP in T_scan:
        current_dic = eval('model.minima_dict_wd'+str(int(model.use_Daisy)))
        if TEMP in current_dic:
            print("Minima for T = {} was computed previously, moving on ... ".format(TEMP))
        else:
            print("scanning T = {}...".format(TEMP))
            Vmin = 1e99
            if TEMP == 0:
                guesses = LIST_OF_APPROX_MINIMA[..., :3]
                #guesses = guess_plain(500.)
                #guesses = np.append(guesses, [[246., 0., 0.]], axis=0)
            else:
                guesses = guess_plain(1.5* TEMP)
            for guess in guesses:
                current_sol = optimize.fmin(short_Vtot, guess,
                                            args=(TEMP, model.use_RGE,),
                                            ftol=1e0, xtol=1e0, disp=False)
                current_val = short_Vtot(current_sol, TEMP, model.use_RGE)
                if current_val < Vmin:
                    Vmin = current_val
                    xmin = current_sol
            current_dic[np.around(TEMP, rounded_digits).astype(int)] = xmin
            save_delta_dics(model)
            save_minima_dic(model)

def trace_local_min(model, T_start, T_end, T_interval, field_value,
                    field_direction, rounded_digits=0):
    """ Finds local minimum for [T_start, T_start+T_interval, ..., T_end]
        and returns a dictionary of field configurations, for T rounded to
        rounded_digits digits. It only scans along the field_direction and
        keeps all other field values as in field_value """
    load_delta_dics(model)
    load_minima_dic(model)
    if T_interval=='log':
        T_scan = [0.]
        current_T = 100.
        while current_T<T_end:
            for i in range(1,6):
                T_scan.append(current_T*i)
            current_T *= 10
        T_scan.append(current_T)
        T_scan = np.array(T_scan)
    else:
        T_scan = np.arange(T_start, T_end+T_interval, T_interval)
    print("Scanning the temperatures: {}".format(T_scan))
    def short_Vtot(x, T, run_scale):
        """ wrapper for Vtot"""
        if np.abs(x) > model.scale_end_upper:
            return -1e99
        x_long = np.concatenate([[field_value[0]], [field_value[1]], model.N*[field_value[2]]])
        x_long[field_direction] = x
        ret = model.Vtot(np.abs(x_long), T, run_scale=run_scale)
        return ret
    for TEMP in T_scan:
        if TEMP in model.minima_dict:
            print("Minima for T = {} was computed previously, moving on ... ".format(TEMP))
        else:
            print("scanning T = {}...".format(TEMP))
            Vmin = 1e99
            current_sol = optimize.fmin(short_Vtot, TEMP,
                                        args=(TEMP, model.use_RGE,), #field_value[field_direction]
                                        ftol=1e0, xtol=1e0, disp=False)
            current_val = short_Vtot(current_sol, TEMP, model.use_RGE)
            if current_val < Vmin:
                Vmin = current_val
                xmin = current_sol
            xmin_large = field_value.copy()
            xmin_large[field_direction] = xmin
            model.minima_dict[np.around(TEMP, rounded_digits).astype(int)] = xmin_large
        save_delta_dics(model)
        save_minima_dic(model)

def denoise_local_min(model, T_start, T_end, T_interval,
                      field_direction, overwrite, resolution, start=None):
    """ Looks at previously found minima in either
        [T_start, T_start+T_interval, ..., T_end] or
        in log space if T_interval == 'log'.
        Then it looks at 50 values to the left and right
        in field_direction with spacing resolution to check if
        the potential is deeper there.
        If overwrite is true, the minima dictionary is
        overwritten with the new value"""
    num_pts = 101
    load_delta_dics(model)
    load_minima_dic(model)
    if T_interval=='log':
        T_scan = [0.]
        current_T = 100.
        while current_T<T_end:
            for i in range(1,6):
                T_scan.append(current_T*i)
            current_T *= 10
        T_scan.append(current_T)
        T_scan = np.array(T_scan)
    else:
        T_scan = np.arange(T_start, T_end+T_interval, T_interval)
    print("Scanning the temperatures: {}".format(T_scan))

    def short_Vtot(x, T, run_scale):
        """ wrapper for Vtot"""
        if (np.abs(x) > 1e8).any():
            return -1e99
        x_long = np.zeros((len(x), model.N+2))
        x_long[..., :3] = x
        x_long[..., 2:] = np.ones((1, model.N)) * x[..., 2, np.newaxis]
        ret = []
        for x_loop in x_long:
            ret.append(model.Vtot(np.abs(x_loop), T, run_scale=run_scale))
        ret = np.array(ret)
        return ret
    new_dic = {}
    for TEMP in T_scan:
        current_dic = eval('model.minima_dict_wd'+str(int(model.use_Daisy)))
        if TEMP in current_dic:
            if start is not None:
                current_minimum = np.zeros(3)
                current_minimum[field_direction] = start
            else:
                current_minimum = current_dic[TEMP].copy()
            old_minimum = current_dic[TEMP].copy()
            step = num_pts >> 1
            steps = resolution*(np.arange(num_pts)-step)
            field_scan = np.ones((num_pts, 3))*np.around(current_minimum, 0)
            field_scan[..., field_direction] += steps
            V_values = short_Vtot(field_scan, TEMP, model.use_RGE)
            best_index = np.argmin(V_values)
            if best_index == 0 or best_index == num_pts-1:
                print("BEST VALUE FOUND AT BOUNDARY, INCREASE num_pts!")
            new_value = short_Vtot(field_scan[best_index][np.newaxis, ...],
                                   TEMP, model.use_RGE)
            old_value = short_Vtot(old_minimum[np.newaxis, ...], TEMP, model.use_RGE)
            print("old value: ", old_value, " at ", old_minimum)
            print("new value: ", new_value, " at ", field_scan[best_index])
            if new_value < old_value:
                new_dic[int(TEMP)] = field_scan[best_index]
            else:
                new_dic[int(TEMP)] = old_minimum
        else:
            print("Skipping T = {}, was not in dictionary".format(TEMP))
    print("old: ", current_dic)
    print("new: ", new_dic)
    if overwrite:
        save_delta_dics(model)
        save_minima_dic(model)

def get_ticks_labels(max_x):
    "creates ticks and labels for lin-log plots"
    if max_x == 1e4:
        ticks = np.concatenate([np.arange(0., 1000., 100),
                                np.arange(1000., 1.1e4, 1e3)])
        labels = list(ticks.copy())
        for i in range(1, 15):
            if i not in [1, 6, 9, 10]:
                labels[-i] = ''
            else:
                labels[-i] = '{:.0e}'.format(labels[-i])
    elif max_x == 1e5:
        ticks = np.concatenate([np.arange(0., 1000., 100),
                                np.arange(1000., 1e4, 1e3),
                                np.arange(1e4, 1.1e5, 1e4)])
        labels = list(ticks.copy())
        for i in range(1, 24):
            if i not in [1, 6, 9, 10, 15, 18, 19]:
                labels[-i] = ''
            else:
                labels[-i] = '{:.0e}'.format(labels[-i])
    elif max_x == 5e5:
        ticks = np.concatenate([np.arange(0., 1000., 100),
                                np.arange(1000., 1e4, 1e3),
                                np.arange(1e4, 1.1e5, 1e4),
                                np.arange(1e5, 6e5, 1e5)])
        labels = list(ticks.copy())
        for i in range(1, 29):
            if i not in [1, 4, 5, 11, 14, 15, 20, 23, 24]:
                labels[-i] = ''
            else:
                labels[-i] = '{:.0e}'.format(labels[-i])
    elif max_x == 1e6:
        ticks = np.concatenate([np.arange(0., 1000., 100),
                                np.arange(1000., 1e4, 1e3),
                                np.arange(1e4, 1.1e5, 1e4),
                                np.arange(1e5, 1.1e6, 1e5)])
        labels = list(ticks.copy())
        for i in range(1, 34):
            if i not in [1, 6, 9, 10, 16, 19, 20, 25, 28, 29]:
                labels[-i] = ''
            else:
                labels[-i] = '{:.0e}'.format(labels[-i])
    elif max_x == 1e7:
        ticks = np.concatenate([np.arange(0., 1000., 100),
                                np.arange(1000., 1e4, 1e3),
                                np.arange(1e4, 1.1e5, 1e4),
                                np.arange(1e5, 1.1e6, 1e5),
                                np.arange(1e6, 1.1e7, 1e6)])
        labels = list(ticks.copy())
        for i in range(1, 44):
            if i not in [1, 6, 9, 10, 16, 19, 20, 26, 29, 30, 35, 38, 39]:
                labels[-i] = ''
            else:
                labels[-i] = '{:.0e}'.format(labels[-i])
    else:
        raise ValueError('max_x value does not match label creation')
    return ticks, labels

def get_daisy_label(daisy_num):
    """ create label for daisy approx. in plotting"""
    if daisy_num == '0':
        ret_label = 'no th. mass'
    elif daisy_num == '1':
        ret_label = r'high-$T$ th. mass'
    elif daisy_num == '2':
        ret_label = 'truncated th. mass'
    elif daisy_num == '4':
        ret_label = 'gap th. mass'
    else:
        ret_label = 'Daisy = '+daisy_num
    return ret_label
